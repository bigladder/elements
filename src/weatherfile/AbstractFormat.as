/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile {
import flash.net.FileFilter;

public class AbstractFormat implements IFileFormat {
    private var _rawDescription:String;
    private var _fileDescription:String;
    private var _fileExtension:String;
    private var _fileRawExtension:String;
    private var _fileFilter:FileFilter;

    public function
    AbstractFormat(description:String, extension:String,
                   rawExtension:String=null) {
        _rawDescription = description;
        _fileDescription = description + " (" + extension + ")";
        _fileExtension = extension;
        if (rawExtension == null) {
          _fileRawExtension = extension;
        } else {
          _fileRawExtension = rawExtension;
        }
        _fileFilter = new FileFilter(_fileDescription, extension);
    }

    public function get rawDescription():String {
      return _rawDescription;
    }

    public function get
    fileDescription():String {
        return _fileDescription;
    }

    public function get
    fileDescriptionNoWildcard():String {
        return _rawDescription + " (." + _fileRawExtension + ")";
    }

    public function get
    fileExtension():String {
        return _fileExtension;
    }

    public function get
    fileRawExtension():String {
      return _fileRawExtension;
    }

    public function get
    fileFilter():FileFilter {
        return _fileFilter;
    }
}
}