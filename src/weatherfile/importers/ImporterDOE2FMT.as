/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.importers {
import flash.filesystem.File;

import dse.DataSet;
import dse.DataSetBuilder;

import pdt.PersistentVector;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DOE2Specific;
import plugins.DateAndTime;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.PlugInfo;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.AngleUnits;
import units.DensityUnits;
import units.IrradianceUnits;
import units.PressureUnits;
import units.SpecificEnergyUnits;
import units.SpeedUnits;
import units.TemperatureUnits;
import units.UnitConversions;

import utils.MapUtils;
import utils.ParseUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.DOE2FmtFormat;
import weatherfile.importers.IImporter;

/* References
[1] F. Buhl. (1999). "DOE-2 Weather Processor",
    available at: http://gundog.lbl.gov/dirun/2001weath.pdf */
public class
ImporterDOE2FMT extends AbstractHasFileFormat implements IImporter {
    public function
    ImporterDOE2FMT() {
        super(DOE2FmtFormat.FORMAT);
    }

    public function
    read(file:File):dse.DataSet {
        var lines:Array = FileReader.readLines(file);
        var hdr:Object = new Object();
        var pts:Array = new Array();
        var length:uint = lines.length - 1;
        var year:int;
        // unpack unit conversion functions
        var R_to_C:Function = UnitConversions.get_(
            TemperatureUnits.R, TemperatureUnits.C);
        var F_to_C:Function = UnitConversions.get_(
            TemperatureUnits.F, TemperatureUnits.C);
        var inHg_to_kPa:Function = UnitConversions.get_(
            PressureUnits.IN_HG, PressureUnits.KPA);
        var compDir_to_deg:Function = UnitConversions.get_(
            AngleUnits.COMPASS_DIRECTION, AngleUnits.DEGREES);
        var lbm__ft3_to_kg__m3:Function = UnitConversions.get_(
            DensityUnits.LBM__FT3, DensityUnits.KG__M3);
        var BTU__lbm_to_kJ__kg:Function = UnitConversions.get_(
            SpecificEnergyUnits.BTU__LBM, SpecificEnergyUnits.KJ__KG);
        var BTU__hr_ft2_to_W__m2:Function = UnitConversions.get_(
            IrradianceUnits.BTU__HR_FT2, IrradianceUnits.W__M2);
        var knots_to_m__s:Function = UnitConversions.get_(
            SpeedUnits.KNOTS, SpeedUnits.M__S);
        for (var index:uint = 0; index < length; index++) {
            var line:String = lines[index];
            if (index == 0) {
                hdr[Location.CITY] = line.substr(0, 20);
                year = Number(line.substr(20, 5));
                hdr[Location.LATITUDE_deg] = Number(line.substr(25, 8));
                // multiply by -1 as DOE2 sign convention is + for West, - for
                // East but we want to use the EPW convention internally and
                // throughout: + for East and - for West
                hdr[Location.LONGITUDE_deg] = Number(line.substr(33, 8)) * -1.0;
                // The DOE2 time-zone doesn't come in with the right sign
                // so we'll multiply it by negative one.
                hdr[DateAndTime.TIME_ZONE] = -1 * Number(line.substr(41, 5));
                hdr[DOE2Specific.SOLAR_FLAG] = Number(line.substr(46, 5));
            }
            if (index == 1) {
                hdr[Conditions.MONTHLY_CLEARNESS_INDEXES] =
                    ParseUtils.parseToks(line, " ", /\d+/,
                    function(x:String):Number {
                        return Number(x);
                });
            }
            if (index == 2) {
                hdr[GroundTemperatures.GROUND_TEMPERATURES_C] =
                    ParseUtils.parseToks(line, " ", /\d+/,
                    function(x:String):Number {
                        return R_to_C(Number(x));
                });
            }
            if (index > 2) {
                var pt:Object = new Object();
                var noStore:Number;
                var month:Number = Number(line.substr(0, 2));
                var day:Number = Number(line.substr(2, 2));
                var hr:Number = Number(line.substr(4, 2)) - 1;
                if (index == 3) {
                    hdr[DateAndTime.REFERENCE_YEAR] = year;
                    hdr[DateAndTime.REFERENCE_MONTH] = month;
                    hdr[DateAndTime.REFERENCE_DAY] = day;
                    hdr[DateAndTime.REFERENCE_HOUR] = hr;
                    hdr[DateAndTime.REFERENCE_MINUTE] = 0;
                    hdr[DateAndTime.REFERENCE_SECOND] = 0;
                }
                var hourOfYear:Number = DateAndTime.hourOfYearFromDate(
                    year, month, day, hr);
                pt[DateAndTime.ELAPSED_TIME_hrs] = hourOfYear;
                pt[Psych.WET_BULB_TEMP_C] = F_to_C(
                    Number(line.substr(6, 5)));
                pt[Psych.DRY_BULB_TEMP_C] = F_to_C(
                    Number(line.substr(11, 5)));
                pt[Psych.PRESSURE_kPa] = inHg_to_kPa(
                    Number(line.substr(16, 6)));
                var CC:Number = Number(line.substr(22, 5));
                pt[Conditions.OPAQUE_SKY_COVER] = CC;
                pt[Clouds.CLOUD_COVER] = CC;
                pt[Conditions.TOTAL_SKY_COVER] = CC;
                pt[Solar.IR_FROM_SKY_Wh__m2] = Solar.skyIRFromCloudCover_W__m2(
                  pt[Psych.DRY_BULB_TEMP_C], pt[Psych.WET_BULB_TEMP_C],
                  pt[Psych.PRESSURE_kPa], CC / 10.0);
                pt[Conditions.SNOW_FLAG] = Number(
                    line.substr(27, 3));
                pt[Conditions.RAIN_FLAG] = Number(
                    line.substr(30, 3));
                pt[Wind.DIRECTION_deg] = compDir_to_deg(
                    Number(line.substr(33, 4)));
                noStore = Number( line.substr(37, 7));
                noStore = lbm__ft3_to_kg__m3(
                    Number(line.substr(44, 6)));
                noStore = BTU__lbm_to_kJ__kg(
                    Number(line.substr(50, 6)));
                var solarGlobalHoriz_W__m2:Number = BTU__hr_ft2_to_W__m2(
                    Number(line.substr(56, 7)));
                var solarBeamNormal_W__m2:Number = BTU__hr_ft2_to_W__m2(
                    Number(line.substr(63, 7)));
                if ((solarBeamNormal_W__m2 < 0.0) ||
                    (solarGlobalHoriz_W__m2 < 0.0)) {
                  solarGlobalHoriz_W__m2 = 0.0;
                  solarBeamNormal_W__m2 = 0.0;
                }
                pt = MapUtils.merge(pt,
                    Solar.makePointFromGlobalAndBeam(
                        solarGlobalHoriz_W__m2, solarBeamNormal_W__m2,
                        hourOfYear, year, hdr));
                pt[Conditions.CLOUD_TYPE] = Number(
                    line.substr(70, 3));
                pt[Wind.SPEED_m__s] = knots_to_m__s(
                    Number(line.substr(73, 5)));
                pts.push(pt);
            }
        }
        var seriesMetaMap:Object = PlugInfo.knownSeriesMetaData();
        var headerMetaMap:Object = PlugInfo.knownHeaderMetaData();
        return (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(PersistentVector.create(pts))
          .withSeriesMetaData(seriesMetaMap)
          .withHeaderMetaData(headerMetaMap)
          .build();
    }
}
}