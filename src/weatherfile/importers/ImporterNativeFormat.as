/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.importers {
import flash.filesystem.File;

import dse.DataSet;
import dse.DataSetBuilder;

import pdt.IPVec;
import pdt.PersistentVector;

import plugins.PlugInfo;

import utils.FP;
import utils.MapUtils;
import utils.ParseUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.ElementsNativeFormat;

public class
ImporterNativeFormat extends AbstractHasFileFormat implements IImporter {
    public function ImporterNativeFormat() {
        super(ElementsNativeFormat.FORMAT);
    }

    public static function
    parseHeader(lines:Array):Object {
        var keyVals:Array = new Array();
        var toks:Array;
        var valAsNum:Number;
        for each (var line:String in lines) {
            if (ParseUtils.trim(line) == "") continue;
            toks = FP.map(ParseUtils.tryToNum, ParseUtils.split(line));
            if (toks.length > 2) {
              trace("ERROR parsing line = ", line);
              throw new Error("Parsing header: encountered line w/ more " +
                "than two elements! Ill formed header: " + line);
            }
            valAsNum = Number(toks[1]);
            keyVals.push(toks[0]);
            keyVals.push(toks[1]);

        }
        return MapUtils.makeObject(keyVals);
    }

    public static function
    parseTimeSeries(lines:Array):IPVec {
        var pts:Array = new Array();
        var first:Boolean = true;
        var keys:Array;
        var vals:Array;
        for each (var line:String in lines) {
            if (first) {
                keys = FP.map(ParseUtils.trim, line.split(","))
                first = false;
                continue;
            }
            if (ParseUtils.trim(line) == "") continue;
            vals = FP.map(ParseUtils.tryToNum,
                ParseUtils.split(line));
            pts.push(MapUtils.makeFromKeysAndVals(keys, vals));
        }
        return PersistentVector.create(pts);
    }

    public function
    read(file:File):dse.DataSet {
        var headerLines:Array = FileReader.linesFromZip(
            file, ElementsNativeFormat.HEADER_FILE);
        var timeSeriesLines:Array = FileReader.linesFromZip(
            file, ElementsNativeFormat.TIME_SERIES_FILE);
        var header:Object = parseHeader(headerLines);
        var points:IPVec = parseTimeSeries(timeSeriesLines);
        var seriesMetaData:Object = PlugInfo.knownSeriesMetaData();
        var headerMetaData:Object = PlugInfo.knownHeaderMetaData();
        return (new DataSetBuilder())
          .withHeader(header)
          .withPoints(points)
          .withSeriesMetaData(seriesMetaData)
          .withHeaderMetaData(headerMetaData)
          .build();
    }
}
}
