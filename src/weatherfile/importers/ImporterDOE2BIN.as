/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.importers {
import flash.filesystem.File;
import flash.utils.ByteArray;

import dse.DataSet;
import dse.DataSetBuilder;

import pdt.PersistentVector;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DOE2Specific;
import plugins.DateAndTime;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.PlugInfo;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.AngleUnits;
import units.DensityUnits;
import units.IrradianceUnits;
import units.PressureUnits;
import units.SpecificEnergyUnits;
import units.SpeedUnits;
import units.TemperatureUnits;
import units.UnitConversions;

import utils.MapUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.DOE2BinFormat;

public class ImporterDOE2BIN extends AbstractHasFileFormat
  implements IImporter {
  public function ImporterDOE2BIN() {
    super(DOE2BinFormat.FORMAT);
  }
  public static function decodeN01(n:int):Array {
    var Twb_F:Number;
    var Tdb_F:Number;
    var p_inHg:Number;
    Twb_F = Number(Math.floor((n % 65536) / 256)) - 99.0;
    Tdb_F = Number(n % 256) - 99.0;
    p_inHg = Number(Math.floor(n / 65536)) * 0.1 + 15.0;
    return [Twb_F, Tdb_F, p_inHg];
  }
  public static function decodeN02(n:int):Array {
    /*
    DATA XMASK /
    1   1: -99., 2: -99., 3: 15., 4: 0., 5: 0., 6: 0., 7: 0., 8: 0.,
    9: .02, 10: -30., 11: 0., 12: .0, 13: .0, 14: .0, 15: .0, 16: 10.,

    2   1: 1., 2: 1., 3: .1, 4: 1., 5: 1., 6: 1., 7: 1., 8: .0001,
    9: .001, 10: .5,
    3  11: 1., 12: 1., 13: 1., 14: 1., 15: 0., 16: 0. /

    IWTH(11) = IDAT30(IP1+1)/1048576
    IWTH(12) = MOD(IDAT30(IP1+1),1048576)/1024
    IWTH(4) = MOD(IDAT30(IP1+1),1024)/64
    IWTH(5) = MOD(IDAT30(IP1+1),64)/32
    IWTH(6) = MOD(IDAT30(IP1+1),32)/16
    IWTH(7) = MOD(IDAT30(IP1+1),16)

    CALC(I) = FLOAT(IWTH(I))*XMASK(I,2) + XMASK(I,1) */
    var total_horizontal_solar_btu__hr_ft2:Number =
      Number(Math.floor(n / 1048576));
    var direct_normal_solar_btu__hr_ft2:Number =
      Number(Math.floor((n % 1048576) / 1024));
    if ((direct_normal_solar_btu__hr_ft2 < 0.0) ||
        (total_horizontal_solar_btu__hr_ft2 < 0.0)) {
      trace('WARNING!!! Read in negative Igh or Ibn');
      trace('Igh: ', total_horizontal_solar_btu__hr_ft2);
      trace('Ibn: ', direct_normal_solar_btu__hr_ft2);
      total_horizontal_solar_btu__hr_ft2 = 0.0;
      direct_normal_solar_btu__hr_ft2 = 0.0;
    }
    var cloud_amount:Number =
      Number(Math.floor((n % 1024) / 64)); // 0 - 10
    var snow_flag:int = int(Number((n % 64) / 32) + 0.01);
    var rain_flag:int = int(Number((n % 32) / 16) + 0.01);
    var wind_direction:int = int(Number((n % 16)) + 0.01);
    return [
      total_horizontal_solar_btu__hr_ft2,
      direct_normal_solar_btu__hr_ft2,
      cloud_amount,
      snow_flag,
      rain_flag,
      wind_direction];
  }
  public static function decodeN03(n:int):Array {
    /* DATA XMASK /
    1   1: -99., 2: -99., 3: 15., 4: 0., 5: 0., 6: 0., 7: 0., 8: 0.,
    9: .02, 10: -30., 11: 0., 12: .0, 13: .0, 14: .0, 15: .0, 16: 10.,

    2   1: 1., 2: 1., 3: .1, 4: 1., 5: 1., 6: 1., 7: 1., 8: .0001,
    9: .001, 10: .5,
    3  11: 1., 12: 1., 13: 1., 14: 1., 15: 0., 16: 0. /

    CALC(8)        HUMIDITY RATIO     (LB H2O/LB AIR)
    CALC(9)        DENSITY OF AIR     (LB/CU FT)
    CALC(I) = FLOAT(IWTH(I))*XMASK(I,2) + XMASK(I,1)

    IWTH(8) = IDAT30(IP1+2)/128
    IWTH(9) = MOD(IDAT30(IP1+2),128) */
    var humidityRatio_lb_H2O__lb_air:Number =
      Number(Math.floor(n/128)) * 0.0001;
    var airDensity_lb__ft3:Number = Number(n % 128) * 0.001 + 0.02;
    return [humidityRatio_lb_H2O__lb_air, airDensity_lb__ft3];
  }
  public static function decodeN04(n:int):Array {
    /* DATA XMASK / 1: -99., 2: -99., 3: 15., 4: 0., 5: 0., 6: 0., 7: 0.,
    1               8: 0., 9: .02, 10: -30., 11: 0.,
    12: .0, 13: .0, 14: .0, 15: .0, 16: 10.,

    2              1: 1., 2: 1., 3: .1, 4: 1., 5: 1., 6: 1., 7: 1.,
    8: .0001, 9: .001, 10: .5,
    3             11: 1., 12: 1., 13: 1., 14: 1., 15: 0., 16: 0. /

    IWTH(10) = IDAT30(IP1+3)/2048
    IWTH(13) = MOD(IDAT30(IP1+3),2048)/128
    IWTH(14) = MOD(IDAT30(IP1+3),128)
    CALC(I) = FLOAT(IWTH(I))*XMASK(I,2) + XMASK(I,1)
    ICLDTY = INT(CALC(13) + .01)

    CALC(10)       SPECIFIC ENTHALPY  (BTU/LB)
    ICLDTY         CLOUD TYPE         (0 - 2)
    CALC(14)       WIND SPEED         KNOTS */
    var specific_enthalpy_BTU__lb:Number =
      Number(Math.floor(n / 2048)) * 0.5 - 30.0;
    var cloud_type:int = int(Math.floor((n % 2048) / 128) + 0.01);
    var wind_speed_knots:Number = n % 128;
    return [specific_enthalpy_BTU__lb, cloud_type, wind_speed_knots];
  }
  public function read(file:File):DataSet {
    var bytes:ByteArray = FileReader.readBytes(file);
    var pts:Array = new Array();
    var hdr:Object = new Object();
    // DOE-2 converters say 6200, but has 4-byte header and tail
    var recordLength:uint = 6200 + 8;
    hdr[GroundTemperatures.GROUND_TEMPERATURES_C] = [];
    hdr[Conditions.MONTHLY_CLEARNESS_INDEXES] = [];
    // read year -- year of last record used in case year's not consistent
    bytes.position = recordLength * 23 + 24;
    var year:int = bytes.readInt();
    // DOE2 is a 1-year data format so reference start date/time is
    // January 1, YEAR at 00:00:00
    hdr[DateAndTime.REFERENCE_YEAR] = year;
    hdr[DateAndTime.REFERENCE_MONTH] = 1;
    hdr[DateAndTime.REFERENCE_DAY] = 1;
    hdr[DateAndTime.REFERENCE_HOUR] = 0;
    hdr[DateAndTime.REFERENCE_MINUTE] = 0;
    hdr[DateAndTime.REFERENCE_SECOND] = 0;
    bytes.position = 0;
    // unpack unit conversion functions
    var R_to_C:Function = UnitConversions.get_(
      TemperatureUnits.R, TemperatureUnits.C);
    var F_to_C:Function = UnitConversions.get_(
      TemperatureUnits.F, TemperatureUnits.C);
    var inHg_to_kPa:Function = UnitConversions.get_(
      PressureUnits.IN_HG, PressureUnits.KPA);
    var BTU__hr_ft2_to_W__m2:Function = UnitConversions.get_(
      IrradianceUnits.BTU__HR_FT2, IrradianceUnits.W__M2);
    var compDir_to_deg:Function = UnitConversions.get_(
      AngleUnits.COMPASS_DIRECTION, AngleUnits.DEGREES);
    var lbm__ft3_to_kg__m3:Function = UnitConversions.get_(
      DensityUnits.LBM__FT3, DensityUnits.KG__M3);
    var BTU__lbm_to_kJ__kg:Function = UnitConversions.get_(
      SpecificEnergyUnits.BTU__LBM, SpecificEnergyUnits.KJ__KG);
    var knots_to_m__s:Function = UnitConversions.get_(
      SpeedUnits.KNOTS, SpeedUnits.M__S);
    for (var recordIndex:uint = 0; recordIndex < 24; recordIndex++) {
      var recordStart:uint = recordIndex * recordLength;
      // FORTRAN starts every binary record with a 4-byte header
      bytes.position = recordStart + 4;
      // IWDID 5*4H
      hdr[Location.CITY] = bytes.readUTFBytes(20);
      // read year -- the last record is used as definitive year
      //              in-case inconsistent with this value...
      bytes.readInt();
      hdr[Location.LATITUDE_deg] = bytes.readFloat();
      // need to round precision to 2 decimal places
      // The DOE2 sign convention for longitude is opposite of EPW and
      // opposite of internal convention so multiply by -1 to get
      // negative for west of GMT/UTC and "+" for east.
      hdr[Location.LONGITUDE_deg] = -1.0 * bytes.readFloat();
      // Multiply by negative one to get time zone in consistent convention.
      hdr[DateAndTime.TIME_ZONE] = -1 * bytes.readInt();
      bytes.readInt();  // record number...  same as index + 1
      var numDays:uint = bytes.readInt();
      if (recordIndex % 2 == 0) {
        var startDate:uint = 1;
        var endDate:uint = 16;
        /* from
        http://docs.autodesk.com/RVTMPJ/2010/ENU/
        Revit%20MEP%202010%20Users%20Guide/RME/
        index.html?url=WS1a9193826455f5ff-6507591011913e8f4b8-7fb1.htm,
        topicNumber=d0e127138

        The clearness number ranges from 0 to 2, with 1 indicating an
        average clearness. 0 and 2 are extremes with 0 indicating
        very high haziness and 2 indicating crystal clear conditions.
        These conditions would rarely if ever occur in the US
        (probably a range of 0.6 to 1.4 is more realistic in the US).
        Clearness is specified according to the 2007 ASHRAE Handbook -
        HVAC Applications, Section 33.4: Clear and Dry - greater
        than 1.2; Average - 1.0; Hazy, humid - less than 0.8 */
        (hdr[Conditions.MONTHLY_CLEARNESS_INDEXES] as Array).push(
          bytes.readFloat());
        (hdr[GroundTemperatures.GROUND_TEMPERATURES_C] as Array).push(
          R_to_C(bytes.readFloat()));
      } else {
        startDate = 17;
        endDate = numDays;
        // read a dummy clearness number -- second 1/2 of month
        // same as 1st
        bytes.readFloat();
        // read a dummy ground temperature -- same comment as above
        bytes.readFloat();
      }
      hdr[DOE2Specific.SOLAR_FLAG] = bytes.readInt();
      var month:uint = recordIndex / 2 + 1;
      var pt:Object;
      for (var date:uint = startDate; date < endDate + 1; date++) {
        for (var hour:uint = 1; hour < 25; hour++) {
          pt = new Object();
          var n1:int = bytes.readInt();
          var n2:int = bytes.readInt();
          var n3:int = bytes.readInt();
          var n4:int = bytes.readInt();
          var hourOfYear:int = DateAndTime.hourOfYearFromDate(
            year, month, date, hour - 1);
          pt[DateAndTime.ELAPSED_TIME_hrs] = hourOfYear;
          var decodedN01:Array = decodeN01(n1);
          pt[Psych.WET_BULB_TEMP_C] = F_to_C(decodedN01[0]);
          pt[Psych.DRY_BULB_TEMP_C] = F_to_C(decodedN01[1]);
          pt[Psych.PRESSURE_kPa] = inHg_to_kPa(decodedN01[2]);
          var decodedN02:Array = decodeN02(n2);
          var solarGlobalHoriz_W__m2:Number =
            BTU__hr_ft2_to_W__m2(decodedN02[0]);
          var solarBeamNormal_W__m2:Number =
            BTU__hr_ft2_to_W__m2(decodedN02[1]);
          pt = MapUtils.merge(pt,
            Solar.makePointFromGlobalAndBeam(
              solarGlobalHoriz_W__m2, solarBeamNormal_W__m2,
              hourOfYear, year, hdr));
          var CC:Number = decodedN02[2];
          pt[Conditions.OPAQUE_SKY_COVER] = CC;
          pt[Conditions.TOTAL_SKY_COVER] = CC;
          pt[Clouds.CLOUD_COVER] = CC;
          pt[Solar.IR_FROM_SKY_Wh__m2] = Solar.skyIRFromCloudCover_W__m2(
            pt[Psych.DRY_BULB_TEMP_C], pt[Psych.WET_BULB_TEMP_C],
            pt[Psych.PRESSURE_kPa], CC / 10.0);
          pt[Conditions.SNOW_FLAG] = decodedN02[3];
          pt[Conditions.RAIN_FLAG] = decodedN02[4];
          pt[Wind.DIRECTION_deg] = compDir_to_deg(decodedN02[5]);
          var decodedN03:Array = decodeN03(n3);
          //pt[Psych.HUMIDITY_RATIO] = decodedN03[0];
          //pt[Psych.DENSITY_kg__m3] =
          //    lbm__ft3_to_kg__m3(decodedN03[1]);
          var decodedN04:Array = decodeN04(n4);
          //pt[Psych.ENTHALPY_kJ__kg] =
          //    BTU__lbm_to_kJ__kg(decodedN04[0]);
          pt[Conditions.CLOUD_TYPE] = decodedN04[1];
          pt[Wind.SPEED_m__s] = knots_to_m__s(decodedN04[2]);
          pts.push(pt);
        }
      }
    }
    var seriesMetaMap:Object = PlugInfo.knownSeriesMetaData();
    var headerMetaMap:Object = PlugInfo.knownHeaderMetaData();
    return (new DataSetBuilder())
    .withHeader(hdr)
      .withPoints(PersistentVector.create(pts))
      .withSeriesMetaData(seriesMetaMap)
      .withHeaderMetaData(headerMetaMap)
      .build();
  }
}
}