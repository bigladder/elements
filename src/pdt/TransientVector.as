/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
import utils.ArrayUtils;
import utils.MapUtils;

public class TransientVector implements ITransientVector {
    public static const ERROR_CANT_POP_EMPTY_VECTOR:String =
        "Can't pop empty vector.";
    public static const ERROR_KEY_MUST_BE_AN_INTEGER:String =
        "Key must be integer";
    public static const ERROR_INDEX_OUT_OF_BOUNDS:String =
        'Index out of Bounds';
    public var _cnt:int;
    public var _shift:int;
    public var _root:Node;
    public var _tail:Array;

    public static function
    fromPersistentVector(pv:PersistentVector):TransientVector {
        return new TransientVector(
            pv._cnt, pv._shift, editableRoot(pv._root),
            ArrayUtils.copyArray(pv._tail));
    }

    public static function editableRoot(n:Node):Node {
        var xs:Array = ArrayUtils.copyArray(n.array);
        return new Node(xs);
    }

    public function
    TransientVector(cnt:int, shift:int, root:Node, tail:Array) {
        _cnt = cnt;
        _shift = shift;
        _root = root;
        _tail = tail;
    }

    public function assocN(i:int, val:Object):ITransientVector {
        if (i >= 0 && i < _cnt) {
            if (i >= _tailoff()) {
                _tail[i & 0x01f] = val;
                return this;
            }
            _root = _doAssoc(_shift, _root, i, val);
            return this;
        }
        if (i == _cnt) return conj(val) as ITransientVector;
        throw new Error(ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public function _doAssoc(level:int, node:Node, i:int, val:Object):Node {
        var ret:Node = node;
        if (level == 0) {
            ret.array[i & 0x01f] = val;
        } else {
            var subidx:int = (i >>> level) & 0x01f;
            ret.array[subidx] = _doAssoc(
                level - 5, node.array[subidx] as Node, i, val);
        }
        return ret;
    }

    public function pop():ITransientVector {
        if (_cnt == 0)
            throw new Error(ERROR_CANT_POP_EMPTY_VECTOR);
        if (_cnt == 1) {
            _cnt = 0;
            return this;
        }
        var i:int = _cnt - 1;
        // pop in tail?
        if ((i & 0x01f) > 0) {
            _cnt--;
            return this;
        }
        var newTail:Array = _editableArrayFor(_cnt - 2);
        var newRoot:Node = _popTail(_shift, _root);
        var newShift:int = _shift;
        if (newRoot == null) {
            newRoot = new Node();
        }
        if (_shift > 5 && newRoot.array[1] == null) {
            newRoot = _ensureEditable(newRoot.array[0] as Node);
            newShift -= 5;
        }
        _root = newRoot;
        _shift = newShift;
        _cnt--;
        _tail = newTail;
        return this;
    }

    public function _editableArrayFor(i:int):Array {
        if (i >= 0 && i < _cnt) {
            if (i >= _tailoff()) return _tail;
            var node:Node = _root;
            for (var level:int = _shift; level > 0; level -= 5) {
                node = _ensureEditable(
                    node.array[(i >>> level) & 0x01f] as Node);
            }
            return node.array;
        }
        throw new Error(ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public function _popTail(level:int, node:Node):Node {
        node = _ensureEditable(node);
        var subidx:int = ((_cnt - 2) >>> level) & 0x01f;
        var ret:Node;
        if (level > 5) {
            var newChild:Node = _popTail(
                level - 5, node.array[subidx] as Node);
            if (newChild == null && subidx == 0) {
                return null;
            } else {
                ret = node;
                ret.array[subidx] = newChild;
                return ret;
            }
        } else if (subidx == 0) {
            return null;
        }
        ret = node;
        ret.array[subidx] = null;
        return ret;
    }

    public function _ensureEditable(node:Node):Node {
        return node;
    }

    public function assoc(key:Object, val:Object):ITransientAssociative {
        if (key is int) {
            var i:int = int(key);
            return assocN(i, val);
        }
        throw new Error(ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public function nth(i:int, notFound:Object=null):Object {
        if (notFound == null) {
            var node:Array = _arrayFor(i);
            return node[i & 0x01f];
        }
        if (i >= 0 && i < this.count) return nth(i);
        return notFound;
    }

    public function _arrayFor(i:int):Array {
        if (i >= 0 && i < _cnt) {
            if (i >= _tailoff()) return _tail;
            var node:Node = _root;
            for (var level:int=_shift; level > 0; level -= 5) {
                node = node.array[(i >>> level) & 0x01f] as Node;
            }
            return node.array;
        }
        throw new Error(ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public function get count():int {
        return _cnt;
    }

    public function valAt(key:Object, notFound:Object=null):Object {
        if (key is int) {
            return nth(int(key));
        }
        throw new Error(ERROR_KEY_MUST_BE_AN_INTEGER);
    }

    public function conj(val:Object):ITransientCollection {
        var i:int = _cnt;
        // room in tail?
        if ((i - _tailoff()) < 32) {
            _tail[i & 0x01f] = val;
            _cnt++;
            return this;
        }
        // full tail; push into tree
        var newRoot:Node;
        var tailNode:Node = new Node(_tail);
        _tail = new Array(32);
        _tail[0] = val;
        var newShift:int = _shift;
        // overflow root?
        if ((_cnt >>> 5) > (1 << _shift)) {
            newRoot = new Node();
            newRoot.array[0] = _root;
            newRoot.array[1] = _newPath(_shift, tailNode);
            newShift += 5;
        } else {
            newRoot = _pushTail(_shift, _root, tailNode);
        }
        _root = newRoot;
        _shift = newShift;
        _cnt++;
        return this;
    }

    public function conjAll(vals:Array):TransientVector {
        var tailOff:int;
        for each (var val:Object in vals) {
            if (_cnt < 32) {
                tailOff = 0;
            } else {
                tailOff = ((_cnt - 1) >>> 5) << 5;
            }
            // room in tail?
            if ((_cnt - tailOff) < 32) {
                _tail[_cnt & 0x01f] = val;
                _cnt++;
            } else {
                // full tail; push into tree
                var newRoot:Node;
                var tailNode:Node = new Node(_tail);
                _tail = new Array(32);
                _tail[0] = val;
                var newShift:int = _shift;
                // overflow root?
                if ((_cnt >>> 5) > (1 << _shift)) {
                    newRoot = new Node();
                    newRoot.array[0] = _root;
                    newRoot.array[1] = _newPath(_shift, tailNode);
                    newShift += 5;
                } else {
                    newRoot = _pushTail(_shift, _root, tailNode);
                }
                _root = newRoot;
                _shift = newShift;
                _cnt++;
            }
        }
        return this;
    }

    public function _pushTail(level:int, parent:Node, tailNode:Node):Node {
        /* if parent is leaf, insert node,
        else does it map to an existing child? ->
            nodeToInsert = pushNode one more level
        else alloc new path
        return nodeToInsert placed in parent */
        var subidx:int = ((_cnt - 1) >>> level) & 0x01f;
        var ret:Node = parent;
        var nodeToInsert:Node;
        if (level == 5) {
            nodeToInsert = tailNode;
        } else {
            var child:Node = parent.array[subidx] as Node;
            if (child != null) {
                nodeToInsert = _pushTail(level - 5, child, tailNode);
            } else {
                nodeToInsert = _newPath(level - 5, tailNode);
            }
        }
        ret.array[subidx] = nodeToInsert;
        return ret;
    }

    public function persistent():IPersistentCollection {
        var trimmedTail:Array = new Array(_cnt - _tailoff());
        ArrayUtils.copyIntoArray(_tail, 0, trimmedTail, 0, trimmedTail.length);
        return new PersistentVector(_cnt, _shift, _root, trimmedTail);
    }

    public function _tailoff():int {
        if (_cnt < 32) return 0;
        return ((_cnt - 1) >>> 5) << 5;
    }

    public static function editableTail(tl:Array):Array {
        var ret:Array = new Array(32);
        ArrayUtils.copyIntoArray(tl, 0, ret, 0, tl.length);
        return ret;
    }

    public static function _newPath(level:int, node:Node):Node {
        if ((level % 5) != 0) {
            trace('level =', level);
            MapUtils.traceObject(node, 'node');
            throw new Error('level is not a multiple of 5');
        }
        if (level < 0) {
            trace('level =', level);
            MapUtils.traceObject(node, 'node');
            throw new Error('level is less than 0');
        }
        if (level == 0) {
            return node;
        }
        var ret:Node = new Node();
        ret.array[0] = _newPath(level - 5, node);
        return ret;
    }
}
}