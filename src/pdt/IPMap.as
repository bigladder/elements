/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
public interface IPMap extends ICounted, IAssociative, ILookup {
    function assoc(key:Object, val:Object):IPMap;
    function assocEx(key:Object, val:Object):IPMap;
    function without(key:Object):IPMap;
    function getIn(keys:Array):Object;
    function assocIn(keys:Array, val:Object):IPMap;
    function get keys():Array;
    function get vals():Array;
}
}