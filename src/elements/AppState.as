/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import elements.views.BaseWindow;
import elements.views.MenuBar;

import gui.IManagedRef;

import pdt.IPMap;
import pdt.IPVec;

public class AppState implements IHasOpenWindows, IHasActiveWindowIndex,
  IHasApplicationName, IHasMenuBar, IHasPreferences, IHasNewDocMaker,
  IHasNewDocFromViewMaker, IHasLoadFileIntoDocFun {
  internal var _openWindows:IPVec;
  internal var _activeWindowIdx:int;
  internal var _applicationName:String;
  internal var _menuBar:MenuBar;
  internal var _preferences:IPMap;
  internal var _newDocMaker:Function;
  internal var _newDocFromViewMaker:Function;
  internal var _loadFileIntoDocFun:Function;
  internal var _numOpenWins:int;
  internal var _activeWindow:Object;
  internal var _startupMode:Boolean;
  internal var _modal:Boolean;
  // AppData -- application data (state)
  // openWindows :: Vector BaseWindow
  // activeWindowIdx :: n of Int where n >= -1 && n < openWindows.count
  // appName :: String
  // mb :: MenuBar
  // prefs :: IPMap -- keyValue store
  // newDocMaker :: Function():BaseWindow
  // newDocFromViewMaker :: Function(v:Group, stateRef:IManagedRef,
  //                                 checkClose:Boolean, opts:Object=null, parentWindow:BaseWindow):BaseWindow
  // loadFileIntoDocFun :: Function(f:File, win:Object):void
  // -- loads the data from the given file into the given window
  public function AppState(b:AppStateBuilder) {
    _openWindows = b.openWindows;
    _activeWindowIdx = b.activeWindowIdx;
    _applicationName = b.applicationName;
    _menuBar = b.menuBar;
    _preferences = b.preferences;
    _newDocMaker = b.newDocMaker;
    _newDocFromViewMaker = b.newDocFromViewMaker;
    _loadFileIntoDocFun = b.loadFileIntoDocFun;
    _startupMode = b.startupMode;
    _modal = b.modal;
    _numOpenWins = _openWindows.length;
    _activeWindow = null;
    if (_activeWindowIdx >= _numOpenWins) {
      throw new Error("Active window index is greater than the " +
        "number of windows");
    }
    if (_activeWindowIdx < -1) {
      throw new Error("Active window index is less than -1");
    }
  }
  public function get openWindows():IPVec {
    return _openWindows;
  }
  public function withOpenWindows(ws:IPVec):AppState {
    return AppStateBuilder.fromAppState(this)
      .withOpenWindows(ws)
      .build();
  }
  public function get activeWindowIndex():int {
    return _activeWindowIdx;
  }
  public function withActiveWindowIndex(idx:int):AppState {
    return AppStateBuilder.fromAppState(this)
      .withActiveWindowIndex(idx)
      .build();
  }
  public function get applicationName():String {
    return _applicationName;
  }
  public function withApplicationName(n:String):AppState {
    return AppStateBuilder.fromAppState(this)
      .withApplicationName(n)
      .build();
  }
  public function get menuBar():MenuBar {
    return _menuBar;
  }
  public function withMenuBar(mb:MenuBar):AppState {
    return AppStateBuilder.fromAppState(this)
      .withMenuBar(mb)
      .build();
  }
  public function get preferences():IPMap {
    return _preferences;
  }
  public function withPreferences(ps:IPMap):AppState {
    return AppStateBuilder.fromAppState(this)
      .withPreferences(ps)
      .build();
  }
  public function get newDocMaker():Function {
    return _newDocMaker;
  }
  public function withNewDocMaker(f:Function):AppState {
    return AppStateBuilder.fromAppState(this)
      .withNewDocMaker(f)
      .build();
  }
  public function get newDocFromViewMaker():Function {
    return _newDocFromViewMaker;
  }
  public function withNewDocFromViewMaker(f:Function):AppState {
    return AppStateBuilder.fromAppState(this)
      .withNewDocFromViewMaker(f)
      .build();
  }
  public function get loadFileIntoDocFun():Function {
    return _loadFileIntoDocFun;
  }
  public function withLoadFileIntoDocFun(f:Function):AppState {
    return AppStateBuilder.fromAppState(this)
      .withLoadFileIntoDocFun(f)
      .build();
  }
  public function get numberOfOpenWindows():int {
    if (_numOpenWins == -1) {
      _numOpenWins = openWindows.length;
    }
    return _numOpenWins;
  }
  public function get activeWindow():Object {
    if (_activeWindow == null) {
      if (this.activeWindowIndex < 0) return null
      var ws:IPVec = this.openWindows;
      _activeWindow = ws.valAt(this.activeWindowIndex) as Object;
    }
    return _activeWindow;
  }
  public function get startupMode():Boolean {
    return _startupMode;
  }
  public function withStartupMode(flag:Boolean):AppState {
    return AppStateBuilder.fromAppState(this)
      .withStartupMode(flag)
      .build();
  }
  public function get modal():Boolean {
    return _modal;
  }
  public function withModal(flag:Boolean):AppState {
    return AppStateBuilder.fromAppState(this)
      .withModal(flag)
      .build();
  }
  public function get activeDocState():IManagedRef {
    var w:BaseWindow = this.activeWindow as BaseWindow;
    if (w == null) return null;
    return w.docStateCtrlr;
  }
}
}