/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import pdt.IPMap;
import pdt.PersistentArrayMap;

public class NormWizData{
  public static const _METRICS:String = 'metrics';
  public static const _METRIC_NAMES:String = 'metricNames';
  public static const _SETTING_SETS:String = 'settingSets';
  public static const _SELECTIONS:String = 'selections';
  public static const _DOCREF:String = 'docRef';
  public var _map:IPMap;
  
  public static function create(map:IPMap):NormWizData {
    return new NormWizData(
      map.valAt(_METRICS) as Array,
      map.valAt(_METRIC_NAMES) as Array,
      map.valAt(_SETTING_SETS) as Object,
      map.valAt(_SELECTIONS) as Array);
  }
  // let Name = String
  //     FullName = String
  // in  metrics      :: [Name] -- e.g., ['Tdb']
  //     metricNames  :: [FullName] -- e.g., ['Dry-Bulb Temperature']
  //     settingSets  :: Object
  //     selections   :: [SeriesSelection]
  //     mods         :: [ScalingWizMod]
  public function NormWizData(metrics:Array, metricNames:Array,
                              settingSets:Object, selections:Array){
    var keyVals:Array = [
      _METRICS, metrics,
      _METRIC_NAMES, metricNames,
      _SETTING_SETS, settingSets,
      _SELECTIONS, selections];
    _map = new PersistentArrayMap(keyVals);
  }
  public function get metrics():Array {
    return _map.valAt(_METRICS) as Array;
  }
  public function get metricNames():Array {
    return _map.valAt(_METRIC_NAMES) as Array;
  }
  public function get settingSets():Object {
    return _map.valAt(_SETTING_SETS) as Object;
  }
  public function get selections():Array {
    return _map.valAt(_SELECTIONS) as Array;
  }
}
}