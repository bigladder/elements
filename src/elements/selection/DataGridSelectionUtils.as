/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.selection {
import mx.collections.ArrayCollection;

import spark.components.DataGrid;
import spark.components.gridClasses.CellPosition;
import spark.components.gridClasses.GridColumn;

import dse.SeriesSelection;

import utils.AssociativeArray;

public class DataGridSelectionUtils {
    public static function
    setDataGridSelection(selections:ArrayCollection, dataGrid:DataGrid):void {
        //trace('setDataGridSelection');
        if (selections.length == 0) {
            return;
        }
        if (sameSelection(selections, dataGrid)) return;
        dataGrid.clearSelection();
        var cases:Vector.<SelectionCase> =
            makeTabularSelectionCases(selections, dataGrid);
        var bestCase:SelectionCase = chooseBestCase(cases);
        dataGrid.selectCellRegion(
            bestCase.tableRegion.rowStart, bestCase.tableRegion.colStart,
            bestCase.tableRegion.rowEnd - bestCase.tableRegion.rowStart + 1,
            bestCase.tableRegion.colEnd - bestCase.tableRegion.colStart + 1);
        //trace('number of additional cells', bestCase.additionalCells.length);
        for each (var cp:CellPosition in bestCase.additionalCells) {
            dataGrid.addSelectedCell(cp.rowIndex, cp.columnIndex);
        }
    }

    // Determine if the current dataGrid selection is the same as that
    // of the selections coming in.
    public static function
    sameSelection(selections:ArrayCollection, dataGrid:DataGrid):Boolean {
      //trace('DataGridSelectionUtils.sameSelection');
      var selCells:Vector.<CellPosition> = dataGrid.selectedCells;
      if ((selections.length == 0) && (selCells.length == 0)) return true;
      // for performance reasons, we're not going to try and figure this out
      // if over 400 cells are already selected
      if (selCells.length > 400) return false;
      var expSells:Vector.<CellPosition> = Vector.<CellPosition>([]);
      for each (var selection:SeriesSelection in selections) {
        for each (var cell:CellPosition in selection.toCellPositions(dataGrid.columns)) {
          expSells.push(cell);
        }
      }
      if (selCells.length != expSells.length) return false;
      for (var expSellIdx:int=0; expSellIdx<expSells.length; expSellIdx++) {
        var flag:Boolean = false;
        for (var selCellIdx:int=0; selCellIdx<selCells.length; selCellIdx++) {
          var selCell:CellPosition = selCells[selCellIdx];
          var expCell:CellPosition = expSells[expSellIdx];
          if ((selCell.columnIndex == expCell.columnIndex) &&
              (selCell.rowIndex == expCell.rowIndex)) {
            flag = true;
            break;
          }
        }
        if (!flag) return false;
      }
      return true;
    }

    public static function
    makeTabularSelectionCases(selections:ArrayCollection,
                              dataGrid:DataGrid):Vector.<SelectionCase> {
        //trace('makeTabularSelectionCases');
        var seriesSelections:Vector.<TableRegion> =
            makeSeriesSelections(selections, dataGrid);
        var cases:Vector.<SelectionCase> = new Vector.<SelectionCase>;
        for (var idx01:int = 0; idx01 < seriesSelections.length; idx01++) {
            var rowStarts:Vector.<int> = new Vector.<int>;
            var rowEnds:Vector.<int> = new Vector.<int>;
            for (var idx02:int = idx01;
                idx02 < seriesSelections.length; idx02++) {
                rowStarts.push(seriesSelections[idx02].rowStart);
                rowEnds.push(seriesSelections[idx02].rowEnd);
                var colStart:int = seriesSelections[idx01].colStart;
                var colEnd:int = seriesSelections[idx02].colEnd;
                var tr:TableRegion =
                    makeTableRegion(
                        colStart, colEnd, rowStarts, rowEnds,
                        seriesSelections);
                if (tr == null) continue;
                var remCells:Vector.<CellPosition> =
                    addRemainingCells(seriesSelections, tr);
                cases.push(new SelectionCase(tr, remCells));
                if(remCells.length == 0) break;
            }
            if(remCells.length == 0) break;
        }
        return cases;
    }

    public static function
    chooseBestCase(cases:Vector.<SelectionCase>):SelectionCase {
        //trace('chooseBestCase');
        var numCells:int = -1;
        var bestCase:SelectionCase = null;
        for each (var c:SelectionCase in cases) {
            if(c.tableRegion.numCells > numCells) {
                numCells = c.tableRegion.numCells;
                bestCase = c;
            }
        }
        return bestCase;
    }

    public static function
    addRemainingCells(seriesSelections:Vector.<TableRegion>,
                      tr:TableRegion):Vector.<CellPosition> {
        var remCells:Vector.<CellPosition> = new Vector.<CellPosition>;
        for each (var seriesSelection:TableRegion in seriesSelections) {
            remCells = remCells.concat(
                seriesSelection.cellsOutsideOfRegion(tr));
        }
        return remCells;
    }

    public static function
    makeTableRegion(colStart:int, colEnd:int,
                    rowStarts:Vector.<int>, rowEnds:Vector.<int>,
                    seriesSelections:Vector.<TableRegion>):TableRegion {
        var remCells:Vector.<CellPosition> = new Vector.<CellPosition>;
        var rowStart:int = determineTabularRowStart(rowStarts);
        var rowEnd:int = determineTabularRowEnd(rowEnds);
        if (rowStart > rowEnd) return null;
        return (new TableRegion(rowStart, rowEnd, colStart, colEnd));
    }

    public static function
    makeSeriesSelections(selections:ArrayCollection,
                         dataGrid:DataGrid):Vector.<TableRegion> {
        var colIdxByName:AssociativeArray = makeColumnIdxByName(dataGrid);
        var seriesSelections:Vector.<TableRegion> =
            new Vector.<TableRegion>;
        var colIdx:int;
        for each (var s:SeriesSelection in selections) {
            colIdx = colIdxByName.getValByKey(s.selectedSeries);
            seriesSelections.push(
                new TableRegion(
                    s.startIndex, s.startIndex + s.numberOfValues - 1,
                    colIdx, colIdx));
        }
        seriesSelections.sort(sortTableRegion);
        return seriesSelections;
    }

    public static function
    determineTabularRowStart(rowStarts:Vector.<int>):int {
        var rowStart:int = -1;
        for each (var idx:int in rowStarts) {
            if((rowStart == -1) || (idx > rowStart)) rowStart = idx;
        }
        return rowStart;
    }

    public static function
    determineTabularRowEnd(rowEnds:Vector.<int>):int {
        var rowEnd:int = -1;
        for each (var idx:int in rowEnds) {
            if((rowEnd == -1) || (idx < rowEnd)) rowEnd = idx;
        }
        return rowEnd;
    }

    public static function
    makeColumnIdxByName(dataGrid:DataGrid):AssociativeArray {
        var colIdxByName:AssociativeArray = new AssociativeArray([], []);
        dataGrid.columns.toArray().forEach(
            function (item:GridColumn, idx:int, arr:Array):void {
                colIdxByName.setKeyVal(item.dataField, idx);
        });
        return colIdxByName;
    }

    public static function
    sortTableRegion(x:TableRegion, y:TableRegion):Number {
        var retVal:int;
        if(x.colStart < y.colStart) {
            retVal = -1;
        } else if (x.colStart == y.colStart) {
            retVal = 0;
        } else {
            retVal = 1;
        }
        return retVal;
    }
}
}