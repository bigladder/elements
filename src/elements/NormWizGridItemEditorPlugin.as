/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import spark.components.gridClasses.GridItemEditor;

import dse.MetaData;

import gui.IManagedRef;

import units.UnitConversions;

import utils.MapUtils;

public class NormWizGridItemEditorPlugin implements IDataGridRowToNum,
  IDataGridValSaver, IDataGridValToStr, IDataGridValToObj {
  public var _metricName:String;
  
  public function NormWizGridItemEditorPlugin(metricName:String) {
    _metricName = metricName;
  }
  public function dataToNumber(sc:IManagedRef, field:String,
                               data:Object):Number {
    trace('NormWizGridItemEditor.dataToValue');
    if (data.hasOwnProperty(field)) {
      return data[field] as Number;
    }
    trace('NormWizGridItemEditor.dataToValue :: field not found');
    trace('NormWizGridItemEditor.dataToValue :: field', field);
    MapUtils.traceObject(data,'NormWizGridItemEditor.dataToValue :: data');
    return NaN;
  }
  public function saveValue(sc:IManagedRef, gie:GridItemEditor,
                            val:Number):void {
    var row:Object = gie.dataGrid.dataProvider.getItemAt(gie.rowIndex);
    row[gie.column.dataField] = val;
    gie.dataGrid.dataProvider.setItemAt(row, gie.rowIndex);
  }
  public function valueToString(sc:IManagedRef, field:String,
                                val:Object):String {
    var ucf:Function = getBaseToDisplayConverter(sc);
    var convertedVal:Number = ucf(Number(val));
    if(isNaN(convertedVal)) {
      return "";
    }
    return convertedVal.toString();
  }
  public function valToObj(sc:IManagedRef, field:String,
                           valAsTxt:String):Object {
    var ucf:Function = getDisplayToBaseConverter(sc);
    var convertedVal:Number;
    if(valAsTxt == "") {
      convertedVal = NaN;
    } else {
      convertedVal = ucf(Number(valAsTxt));
    }
    return convertedVal;
  }
  private function getSeriesMeta(sc:IManagedRef):MetaData {
    var ds:DocState = DocHandlers.readDocState(sc);
    return ds.dataSet.metaDataFor(this._metricName);
  }
  private function getDisplayUnits(sc:IManagedRef):String {
    var ds:DocState = DocHandlers.readDocState(sc);
    return ds.displayUnitsFor(this._metricName);
  }
  private function getDataDisplayOptions(sc:IManagedRef):DataDisplayOptions {
    var ds:DocState = DocHandlers.readDocState(sc);
    return ds.displayOptionsFor(this._metricName);
  }
  private function getDisplayToBaseConverter(sc:IManagedRef):Function {
    var sm:MetaData = getSeriesMeta(sc);
    var displayUnits:String = getDisplayUnits(sc);
    var ddo:DataDisplayOptions = getDataDisplayOptions(sc)
    return function (displayVal:Number):Number {
      return UnitConversions.displayToBase(displayVal, displayUnits,
        ddo.scale, sm.baseUnits);
    };
  }
  private function getBaseToDisplayConverter(sc:IManagedRef):Function {
    var sm:MetaData = getSeriesMeta(sc);
    var displayUnits:String = getDisplayUnits(sc);
    var ddo:DataDisplayOptions = getDataDisplayOptions(sc)
    return function (baseVal:Number):Number {
      return UnitConversions.baseToDisplay(baseVal, sm.baseUnits,
        ddo.scale, displayUnits);
    };
  }
}
}