/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import spark.components.TextInput;
import spark.components.gridClasses.GridItemEditor;

import dse.MetaData;

import elements.DocHandlers;

import gui.IManagedRef;

public class GridStringItemEditor extends GridItemEditor {
    private var _textInput:TextInput;
    private var _dataPoint:Object;
    private var _docSC:IManagedRef;

    public function GridStringItemEditor(docSC:IManagedRef) {
        super();
        _docSC = docSC;
        _textInput = new TextInput();
        _textInput.visible = false;
        _dataPoint = null;
        addElement(_textInput);
    }

    override public function get data():Object {
        return _dataPoint;
    }

    override public function set data(dataPoint:Object):void {
        var sm:MetaData = getSeriesMeta();
        var hdr:Object = getDataSetHeader();
        _dataPoint = dataPoint;
        this.value = sm.getter(hdr, dataPoint);
    }

    private function getDataPointForThisRow():Object {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.dataSet.pointN(this.rowIndex);
    }

    private function getDataSetHeader():Object {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.dataSet.header;
    }

    // ref: http://help.adobe.com/en_US/flex/using/
    // WS0ab2a460655f2dc3-427f401412c60d04dca-7ff3.html
    override public function set value(val:Object):void {
        var valAsStr:String = val.toString();
        _textInput.text = "";
    }

    override public function get value():Object {
        return _textInput.text;
;
    }

    override public function save():Boolean {
        if (!validate()) {
            return false;
        }

        // TODO: derive settings correctly
        // thought, you know what cell/column has been selected for edit
        // so just grab the first setting set and those are the hardcoded
        // values that you need to update with. In the future we'll open this
        // up to run through the options from the GUI. If the setting set
        // list is empty, awesome!, we've found a value that's independent and
        // just can go about setting it.
        // WHAT"S WRITTEN BELOW WILL NOT WORK ON ACCOUNT OF EMPTY SETTINGS!!
        /* skip this for now
        var settings:Object = {};
        var stateNames:Object = sm.stateNames;
        var newDp:Object = DataPoint.update(
            hdr, dp, sm.constraintFunc, dfs, settings, mins, maxs, stateNames);
        this.dataGrid.dataProvider.setItemAt(newDp, this.rowIndex);
        this.dataGrid.dataProvider.itemUpdated(newDp, null, null, null);
        dataGrid.validateNow();
        */
        return true;
    }

    override protected function validate():Boolean {
        return true;
    }

    private function getSeriesMeta():MetaData {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.dataSet.metaDataFor(this.column.dataField);
    }

    override public function setFocus():void {
        _textInput.visible = true;
        _textInput.setFocus();
    }
}
}