/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.FocusManager;

    import spark.components.Button;
    import spark.components.Group;
    import spark.components.Label;
    import spark.components.TextInput;
    import spark.components.TitleWindow;
    import spark.layouts.HorizontalLayout;
    import spark.layouts.VerticalLayout;

    public class ModalDialogBox extends TitleWindow {
        private var _label:Label;
        private var _textEdit:TextInput;
        private var _okButton:Button;
        private var _cancelButton:Button;
        private var _focusManager:FocusManager;

        public function ModalDialogBox(label:Label, title:String,
                                         onOK:Function, onCancel:Function) {
            super();
            this.width=225;
            this.height=125;
            this.title = title;
            var vlayout:VerticalLayout = new VerticalLayout();
            vlayout.paddingBottom=10;
            vlayout.paddingTop=10;
            vlayout.paddingLeft=10;
            vlayout.paddingRight=10;
            vlayout.horizontalAlign="center";
            vlayout.verticalAlign="middle";
            this.layout = vlayout;
            var inputGroup:Group = new Group();
            var hlayout:HorizontalLayout = new HorizontalLayout();
            hlayout.verticalAlign = "middle";
            inputGroup.layout = hlayout;
            var buttonGroup:Group = new Group();
            buttonGroup.setStyle('horizontalAlign', 'center');
            buttonGroup.layout = new HorizontalLayout();
            buttonGroup.horizontalCenter=0;
            this._label = label;
            inputGroup.addElement(this._label);
            this._textEdit = new TextInput();
            this._textEdit.addEventListener(FlexEvent.ENTER, onOK);
            inputGroup.addElement(this._textEdit);
            this._okButton = new Button();
            this._okButton.label = "OK";
            this._okButton.addEventListener(MouseEvent.CLICK, onOK);
            this._cancelButton = new Button();
            this._cancelButton.label = "CANCEL";
            this._cancelButton.addEventListener(MouseEvent.CLICK, onCancel);
            buttonGroup.addElement(this._okButton);
            buttonGroup.addElement(this._cancelButton);
            this.addElement(inputGroup);
            this.addElement(buttonGroup);
            this.addEventListener(CloseEvent.CLOSE, onCancel);
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
        }

        private function onCreationComplete(e:Event):void
        {
            this.focusManager.activate();
            this.focusManager.setFocus(this._textEdit);
        }

        public function get value():String
        {
            return this._textEdit.text;
        }
    }
}