/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import flash.events.MouseEvent;
import mx.charts.series.LineSeries;

public class InteractiveChart extends MixedSeriesChart {
    public static const SELECT_MODE:uint = 0;
    public static const PAN_MODE:uint = 1;
    public static const ZOOM_MODE:uint = 2;

    public function InteractiveChart() {
        super();
        this.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
        this.percentHeight=100;
        this.percentWidth=100;
    }

    // Needed to refresh the series list and draw any new ones that were added
    override public function updateTimeSeries():void {
        var line:LineSeries;
        for (var index:uint = 0; index < this.series.length; index++) {
            //trace('addFilterFunction');
            line = this.series[index];
            //line.filterFunction = myFilterFunction;
        }
        //super();  dont know why super wont work anywhere besides constructor
        var tmp:Array = this.series;
        this.series = tmp;
        this.invalidateSeriesStyles();
    }

    public function myFilterFunction(cache:Array):Array {
        //This works!!!!
        // improvement would be to only return items within the visible time
        // range and need to filter NaN, null, and undefined.
        //trace('myFilterFunction');
        var newCache:Array = new Array;
        for (var index:uint = 0; index < cache.length; index++) {
            if (index % 10 == 0) {
                newCache.push(cache[index]);
            }
        }
        return(newCache);
    }

    private function onMouseWheel(event:MouseEvent):void {
        //trace("Mouse wheel delta: " + event.delta);
        if (event.delta > 0) {
            //xAxis.maximum.setTime(xAxis.maximum.time + 86400000);
            //dataProvider.refresh;
            //this.invalidateProperties();
            //xAxis.update();
        } else {
            //xAxis.maximum.setTime(xAxis.maximum.time - 86400000);
            //dataProvider.refresh;
        }
    }
}
}