/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import flash.events.MouseEvent;

import spark.components.Group;
import spark.components.HGroup;
import spark.components.Label;
import spark.components.RadioButton;
import spark.layouts.HorizontalLayout;

import elements.DocEvents;
import elements.DocHandlers;
import elements.commands.DocSetDisplayUnitSys;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import units.UnitSystems;

public class UnitSelectionGroup extends Group {
    public var siRadio:RadioButton;
    public var ipRadio:RadioButton;
    private var _docState:IManagedRef;

    public function UnitSelectionGroup() {
        super();
    }

    public function init(docState:IManagedRef):void {
        _docState = docState;
        _docState.registerSubscriberFunction(update);
        layout = new HorizontalLayout();
        var instructionsLabel:Label = new Label();
        instructionsLabel.text = "Units:";
        instructionsLabel.styleName = "heading";
        instructionsLabel.height = 10;
        var hg:HGroup = ViewUtils.makeHGroup();
        hg.addElement(instructionsLabel);
        var radioButtonGroup:Group = new Group;
        siRadio = new RadioButton();
        var us:String = DocHandlers.readDocState(_docState).displayUnitSystem;
        if (us == UnitSystems.SI) {
            siRadio.selected = true;
        } else {
            siRadio.selected = false;
        }
        siRadio.id = UnitSystems.SI;
        siRadio.groupName = "unitSystem";
        siRadio.label = "SI";
        siRadio.addEventListener(MouseEvent.CLICK,
          function (event:MouseEvent):void {
            _docState.handleCommand(new DocSetDisplayUnitSys(UnitSystems.SI));
          });
        ipRadio = new RadioButton();
        if (us == UnitSystems.IP) {
            ipRadio.selected = true;
        } else {
            ipRadio.selected = false;
        }
        ipRadio.id = UnitSystems.IP;
        ipRadio.groupName = "unitSystem";
        ipRadio.label = "IP";
        ipRadio.addEventListener(MouseEvent.CLICK,
          function (event:MouseEvent):void {
            _docState.handleCommand(new DocSetDisplayUnitSys(UnitSystems.IP));
          });
        hg.addElement(siRadio);
        hg.addElement(ipRadio);
        this.addElement(hg);
    }

    public function update(event:String):void {
      switch (event) {
        case DocEvents.EVENT_DISPLAY_UNITS_CHANGED:
        case DocEvents.EVENT_ALL_CHANGED:
        case AcmeManagedRef.EVENT_STATE_CHANGED:
          /* ref: http://www.jeffryhouser.com/index.cfm/2011/1/25/
          How-do-you-force-rendereres-to-refresh-in-a-spark-list */
          var us:String = DocHandlers.readDocState(_docState).displayUnitSystem;
          //trace('UnitSelectionGroup.update displayUnitSystem:', us);
          if (us == UnitSystems.IP) {
            ipRadio.selected = true;
            siRadio.selected = false;
          } else if (us == UnitSystems.SI) {
            ipRadio.selected = false;
            siRadio.selected = true;
          }
          break;
        default:
      }
    }
}
}