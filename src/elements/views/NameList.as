/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.IList;
import mx.events.CloseEvent;
import mx.events.FlexEvent;

import spark.components.Button;
import spark.components.DropDownList;
import spark.components.HGroup;
import spark.components.TitleWindow;
import spark.layouts.VerticalLayout;

public class NameList extends TitleWindow {
  public var list:DropDownList;
  public var okButton:Button;
  public var cancelButton:Button;

  public function NameList(data:IList, onOK:Function, onCancel:Function) {
    super();
    var vlayout:VerticalLayout = new VerticalLayout();
    vlayout.paddingBottom=10;
    vlayout.paddingTop=10;
    vlayout.paddingLeft=10;
    vlayout.paddingRight=10;
    vlayout.horizontalAlign="center";
    vlayout.verticalAlign="middle";
    this.layout = vlayout;
    list = new DropDownList();
    list.dataProvider = data;
    list.selectedIndex = 0;
    list.width = 275;
    var buttonGroup:HGroup = new HGroup();
    okButton = new Button();
    okButton.label = "OK";
    okButton.addEventListener(MouseEvent.CLICK, onOK);
    cancelButton = new Button();
    cancelButton.label = 'Cancel';
    cancelButton.addEventListener(MouseEvent.CLICK, onCancel);
    this.addElement(list);
    buttonGroup.addElement(okButton);
    buttonGroup.addElement(cancelButton);
    this.addElement(buttonGroup);
    this.addEventListener(CloseEvent.CLOSE, onCancel);
    this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
  }

  private function onCreationComplete(e:Event):void {
    this.focusManager.activate();
    this.focusManager.setFocus(this.list);
  }

  public function get value():int {
    return this.list.selectedIndex;
  }
}
}