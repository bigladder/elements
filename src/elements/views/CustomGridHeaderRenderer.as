/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import spark.components.Label;
import spark.skins.spark.DefaultGridHeaderRenderer;

public class CustomGridHeaderRenderer extends DefaultGridHeaderRenderer {
    public function CustomGridHeaderRenderer() {
        //trace('CustomGridHeaderRenderer');
        super();
        var lab:Label = new Label();
        lab.verticalCenter="1";
        lab.left="3";
        lab.right="3";
        lab.top="3";
        lab.bottom="3";
        lab.styleName="gridHeader";
        lab.maxDisplayedLines=3;
        lab.showTruncationTip=true;
        this.labelDisplay = lab;
        this.autoLayout = true;
    }
}
}