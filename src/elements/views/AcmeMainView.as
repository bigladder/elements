/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views{
import flash.events.Event;
import flash.events.EventPhase;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import spark.components.Button;
import spark.components.HGroup;
import spark.components.VGroup;

import elements.AppHandlers;
import elements.AppState;
import elements.DocEvents;
import elements.WindowOptionsBuilder;
import elements.commands.AppNewCustomDoc;
import elements.commands.DocSetSelections;
import elements.components.AcmeChartGroup;
import elements.components.HeaderEditButton;
import elements.helpers.DataProviderForDocState;
import elements.helpers.IGetMetaData;
import elements.helpers.IProvideData;
import elements.helpers.MetaDataGetterForDocState;

import gui.IManagedRef;

import pdt.PersistentVector;

public class AcmeMainView extends VGroup{
    private var _docSC:IManagedRef;

    public function AcmeMainView(docSC:IManagedRef) {
      //trace('AcmeMainView.AcmeMainView');
        super();
        _docSC = docSC;
        _docSC.registerSubscriberFunction(update);
        percentWidth = 100;
        percentHeight = 100;

        // Groups and Components
        var plotButton:Button = new Button();
        var browseHeaderButton:HeaderEditButton = new HeaderEditButton();
        var usg:UnitSelectionGroup = new UnitSelectionGroup();
        var dhg:DataHeaderGroup = new DataHeaderGroup(docSC);
        var arcbg:AddRemoveColumnButtonGroup =
          new AddRemoveColumnButtonGroup(docSC);
        var tb:ToolBar = new ToolBar(docSC);
        var asspd:ActiveSettingSetPulldown =
          new ActiveSettingSetPulldown(docSC);

        // Config
        plotButton.label="Chart";
        plotButton.addEventListener(
          MouseEvent.CLICK,
          onLaunchPlotWin);
        browseHeaderButton.init(docSC, "Header");
        usg.init(docSC);

        var headerLeft:HGroup = new HGroup();
        headerLeft.addElement(dhg);

        var winButtonGr:HGroup = new HGroup();
        winButtonGr.addElement(browseHeaderButton);
        winButtonGr.addElement(plotButton);

        var headerRight:HGroup = new HGroup();
        headerRight.percentWidth = 100;
        headerRight.horizontalAlign = "right";
        headerRight.addElement(winButtonGr);

        var headerGroup:HGroup = new HGroup();
        headerGroup.percentWidth = 100;
        headerGroup.paddingTop = 10;
        headerGroup.paddingLeft = 10;
        headerGroup.paddingRight = 10;
        headerGroup.addElement(headerLeft);
        headerGroup.addElement(headerRight);
        addElement(headerGroup);

        
        var editLeft:HGroup = new HGroup();
        editLeft.addElement(tb);

        var editRight:HGroup = new HGroup();
        editRight.percentWidth = 100;
        editRight.horizontalAlign = "right";
        editRight.addElement(asspd);

        var editGroup:HGroup = new HGroup();
        editGroup.height = 30;
        editGroup.percentWidth = 100;
        editGroup.paddingBottom = 0;
        editGroup.paddingTop = 10;
        editGroup.addElement(editLeft);
        editGroup.addElement(editRight);
        addElement(editGroup);

        var dgg:DataGridGroup = new DataGridGroup(docSC);
        addElement(dgg);

        var footerLeft:HGroup = new HGroup();
        footerLeft.addElement(arcbg);

        var footerRight:HGroup = new HGroup();
        footerRight.percentWidth = 100;
        footerRight.horizontalAlign = "right";
        footerRight.addElement(usg);

        var footerGroup:HGroup = new HGroup();
        footerGroup.percentWidth = 100;
        footerGroup.paddingBottom = 0;
        footerGroup.paddingTop = 0;
        footerGroup.addElement(footerLeft);
        footerGroup.addElement(footerRight);
        addElement(footerGroup);

        this.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void {
          // need to only respond if clicked in "white-space"
          //trace('AcmeMainView :: mouse clicked');
          //trace('AcmeMainView :: event phase:', e.eventPhase);
          if (e.eventPhase == EventPhase.BUBBLING_PHASE) return;
          docSC.handleCommand(new DocSetSelections(PersistentVector.EMPTY));
          dgg.dataGrid.clearSelection();
          focusManager.getFocus();
        });
        this.addEventListener(KeyboardEvent.KEY_DOWN,
          function(e:KeyboardEvent):void {
            if (e.keyCode == Keyboard.ESCAPE) {
              docSC.handleCommand(new DocSetSelections(PersistentVector.EMPTY));
              dgg.dataGrid.clearSelection();
              focusManager.getFocus();
            }});
    }
    private function onLaunchPlotWin(e:Event):void {
      var appRef:IManagedRef = AppHandlers.instance();
      var plotView:AcmeChartGroup = new AcmeChartGroup();
      var dataProvider:IProvideData =
        new DataProviderForDocState(_docSC);
      var metaGetter:IGetMetaData =
        new MetaDataGetterForDocState(_docSC);
      plotView.init(_docSC, dataProvider, metaGetter);
      var apSt:AppState = AppHandlers.readAppState(appRef);
      var w:BaseWindow = apSt.activeWindow as BaseWindow;
      appRef.handleCommand(new AppNewCustomDoc(
        plotView,
        _docSC,
        false,
        (new WindowOptionsBuilder())
        .withWidth(600)
        .withMinWidth(300)
        .withHeight(500)
        .withMinHeight(250)
        .withTitlePrefix("Chart (EXPERIMENTAL-BETA) for ")
        .build(),
        w));
    }
    private function update(topic:String):void {
      if (topic == DocEvents.EVENT_REQUEST_VIEW_CHART) {
        onLaunchPlotWin(new Event(''));
      }
    }
}
}