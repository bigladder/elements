/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
public class AppEvents {
  public static const EVENT_NEW_DOCUMENT:String =
    'event_newDocument';
  public static const EVENT_ACTIVE_WINDOW_CHANGED:String =
    'event_activeWindowChanged';
  public static const EVENT_OPEN_WINDOWS_CHANGED:String =
    'event_openWindowsChanged';
  public static const EVENT_MENU_BAR_ADDED:String =
    'event_menuBarAdded';
  public static const EVENT_PREFERENCES_CHANGED:String =
    'event_preferencesChanged';
  public static const EVENT_STARTUP_MODE_CHANGED:String =
    'event_startupModeChanged';
  public static const EVENT_MODALITY_CHANGED:String =
    'event_modalityChanged';
  public static const EVENT_WINDOW_RESIZED:String =
    'event_windowResized';
  public static const EVENT_WINDOW_MOVED:String =
    'event_windowMoved';
}
}