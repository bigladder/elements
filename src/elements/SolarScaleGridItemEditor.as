/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import spark.components.TextInput;
import spark.components.gridClasses.GridItemEditor;

import elements.commands.DocScaleSolarByMonthlyAvgDailyInsolation;

import gui.IManagedRef;

import utils.MapUtils;

public class SolarScaleGridItemEditor extends GridItemEditor {
    public static const Hgh_MJ__m2:String = 'Hgh_MJ__m2';
    private var _docSC:IManagedRef;
    private var _textInput:TextInput;
    private var _data:Object;
    private var _value:Number;


    public function SolarScaleGridItemEditor(docSC:IManagedRef) {
        super();
        _docSC = docSC;
        _textInput = new TextInput();
        _data = null;
        _value = NaN;
        addElement(_textInput);
    }

    override public function get data():Object {
        return _data;
    }

    override public function set data(dataPoint:Object):void {
        _data = dataPoint;
        this.value = _data[Hgh_MJ__m2];
    }

    override public function set value(val:Object):void {
        var v:Number = Number(val);
        if (_value == v) {
            _textInput.text = val.toString();
            return;
        }
        _value = v;
        if (isNaN(_value)) {
            _textInput.text = "";
        } else {
            _textInput.text = _value.toString();
        }
    }

    override public function get value():Object {
        if (_textInput.text == "") {
            return NaN;
        }
        return Number(_textInput.text);
    }

    override public function save():Boolean {
        //trace('solarscale save');
        if (!validate()) {
            return false;
        }
        //trace('calling scale solar by monthly average daily insolation');
        _docSC.handleCommand(new DocScaleSolarByMonthlyAvgDailyInsolation(
          this.value, _data);
        return true;
    }

    override protected function validate():Boolean {
        var val:Number = Number(_value);
        if (isNaN(val)) {
            return false;
        }
        if (val < 0.0) {
            return false;
        }
        return true;
    }
}
}