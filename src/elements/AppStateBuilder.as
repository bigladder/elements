/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
  import elements.views.MenuBar;

  import pdt.IPMap;
  import pdt.IPVec;
  import pdt.PersistentArrayMap;
  import pdt.PersistentVector;

public class AppStateBuilder {
  internal var openWindows:IPVec;
  internal var applicationName:String;
  internal var menuBar:MenuBar;
  internal var preferences:IPMap;
  internal var newDocMaker:Function;
  internal var newDocFromViewMaker:Function;
  internal var loadFileIntoDocFun:Function;
  internal var activeWindowIdx:int;
  internal var startupMode:Boolean;
  internal var modal:Boolean;
  public function AppStateBuilder() {
    this.openWindows = PersistentVector.EMPTY;
    this.applicationName = "Elements";
    this.menuBar = null;
    this.preferences = PersistentArrayMap.EMPTY;
    // DocumentMaker.newDocument;
    this.newDocMaker = DocumentMaker.newPrePopulatedDoc;
    this.newDocFromViewMaker = DocumentMaker.newDocumentFromView;
    this.loadFileIntoDocFun = DocumentMaker.loadDocFromFileIntoWin;
    this.activeWindowIdx = -1;
    this.startupMode = true;
    this.modal = false;
  }
  public function withOpenWindows(openWins:IPVec):AppStateBuilder {
    this.openWindows = openWins;
    return this;
  }
  public function withApplicationName(name:String):AppStateBuilder {
    this.applicationName = name;
    return this;
  }
  public function withMenuBar(mb:MenuBar):AppStateBuilder {
    this.menuBar = mb;
    return this;
  }
  public function withPreferences(prefs:IPMap):AppStateBuilder {
    this.preferences = prefs;
    return this;
  }
  public function withNewDocMaker(f:Function):AppStateBuilder {
    this.newDocMaker = f;
    return this;
  }
  public function withNewDocFromViewMaker(f:Function):AppStateBuilder {
    this.newDocFromViewMaker = f;
    return this;
  }
  public function withLoadFileIntoDocFun(f:Function):AppStateBuilder {
    this.loadFileIntoDocFun = f;
    return this;
  }
  public function withActiveWindowIndex(idx:int):AppStateBuilder {
    this.activeWindowIdx = idx;
    return this;
  }
  public function withStartupMode(flag:Boolean):AppStateBuilder {
    this.startupMode = flag;
    return this;
  }
  public function withModal(flag:Boolean):AppStateBuilder {
    this.modal = flag;
    return this;
  }
  public function build():AppState {
    return new AppState(this);
  }
  public static function fromAppState(appSt:AppState):AppStateBuilder {
    return new AppStateBuilder()
    .withOpenWindows(appSt._openWindows)
    .withApplicationName(appSt._applicationName)
    .withMenuBar(appSt._menuBar)
    .withPreferences(appSt._preferences)
    .withNewDocMaker(appSt._newDocMaker)
    .withNewDocFromViewMaker(appSt._newDocFromViewMaker)
    .withLoadFileIntoDocFun(appSt._loadFileIntoDocFun)
    .withActiveWindowIndex(appSt._activeWindowIdx)
    .withStartupMode(appSt._startupMode)
    .withModal(appSt._modal);
  }
}
}