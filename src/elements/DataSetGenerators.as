/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
  import dse.DataSet;
  import dse.DataSetBuilder;
  import dse.DateTime;

  import pdt.PersistentVector;

  import plugins.Clouds;
  import plugins.DateAndTime;
  import plugins.Location;
  import plugins.PlugInfo;
  import plugins.Psych;
  import plugins.Solar;
  import plugins.Wind;

  import units.UnitConversions;

  import utils.MapUtils;

public class DataSetGenerators {
  public static function gen8760(hdr:Object=null):DataSet {
    var header:Object = {};
    // default header values
    header[DateAndTime.REFERENCE_YEAR] = 2013;
    header[DateAndTime.REFERENCE_MONTH] = 1; // January
    header[DateAndTime.REFERENCE_DAY] = 1; // 1st day
    header[DateAndTime.REFERENCE_HOUR] = 0; // midnight
    header[DateAndTime.REFERENCE_MINUTE] = 0; // midnight
    header[DateAndTime.REFERENCE_SECOND] = 0.0; // midnight
    header[Location.ELEVATION_m] = 0.0; // 0 meters above sea level
    header[Location.LATITUDE_deg] = 0.0;
    header[Location.LONGITUDE_deg] = 0.0;
    header[Location.CITY] = "City Name";
    header[Location.COUNTRY] = "Country Name";
    header[Location.DATA_SOURCE] = "Data Source";
    header[Location.STATE_PROVINCE_OR_REGION] = "State Name";
    header[Location.WMO_STATION_NUMBER] = "NA";
    header[DateAndTime.TIME_ZONE] = 0;
    if (hdr != null) {
      header = MapUtils.merge(header, hdr);
    }
    // default series metrics
    var refYear:int = header[DateAndTime.YEAR] as int;
    var Z_m:Number = header[Location.ELEVATION_m] as Number;
    var p_kPa:Number = Psych.standardAtmPressure_kPa(Z_m);
    var Tdb_C:Number = Psych.standardTemperature_C(Z_m);
    var Twb_C:Number = Psych.Twb_from_p_Tdb_and_X(
      Psych.RELATIVE_HUMIDITY, Tdb_C, p_kPa, 0.5);
    var pt:Object;
    var long_deg:Number = header[Location.LONGITUDE_deg] as Number;
    var lat_deg:Number = header[Location.LATITUDE_deg] as Number;
    var tz:int = header[DateAndTime.TIME_ZONE] as int;
    var Ibn_ext:Number; // extraterrestrial beam normal solar irradiation
    var pts:Array = [];
    var Tdew_C:Number = Psych.dewPointTemp_C(Tdb_C, Twb_C, p_kPa);
    var Tdew_K:Number = UnitConversions.C_to_K(Tdew_C);
    var Tdb_K:Number = UnitConversions.C_to_K(Tdb_C);
    var emis:Number = (0.787 + 0.764 * Math.log(Tdew_K/273.15));
    var ir:Number = emis * 5.670373e-8 * Tdb_K * Tdb_K * Tdb_K * Tdb_K;
    for (var n:int=0; n<8760; n++) {
      var doy:int = int(Math.floor(1.0 + n / 24.0));
      var dt:DateTime = DateTime.hourOfYearToDateTime(n, refYear);
      var sha:Number = Solar.solarHourAngle_deg(dt, tz, long_deg);
      var decl:Number = Solar.declination_deg(dt, tz);
      var sunset_ha:Number = Solar.sunsetHourAngle_deg(decl, lat_deg);
      if ((sha < (-sunset_ha)) || (sha > sunset_ha)) {
        Ibn_ext = 0.0;
      } else {
        Ibn_ext = Solar.irradianceExtNormal_W__m2(doy);
      }
      pt = {};
      /*
      Note: this list below populates the default values list when a new
      document is selected. ALL variables that are editable should be listed
      here below.
      */
      // Clouds
      pt[Clouds.CLOUD_COVER] = 0;
      // Conditions
      
      // Date and Time
      pt[DateAndTime.ELAPSED_TIME_hrs] = n;
      // DOE2 Specific -- not included
      // EPW Specific -- not included
      // Ground Temperatures -- no series meta data
      // Location -- no series meta data
      // Psych -- note: just the state points
      pt[Psych.PRESSURE_kPa] = p_kPa;
      pt[Psych.DRY_BULB_TEMP_C] = Tdb_C;
      pt[Psych.WET_BULB_TEMP_C] = Twb_C;
      // Solar -- note: just the statepoints
      pt[Solar.BEAM_NORMAL_Wh__m2] = Ibn_ext * 0.5 * 0.6;
      pt[Solar.DIFFUSE_HORIZONTAL_Wh__m2] = Ibn_ext * 0.5 * 0.4;
      pt[Solar.IR_FROM_SKY_Wh__m2] = ir;
      // Wind
      pt[Wind.SPEED_m__s] = 0.0;
      pt[Wind.DIRECTION_deg] = 0.0;

      pts.push(pt);
    }
    return (new DataSetBuilder())
      .withHeader(header)
      .withPoints(PersistentVector.create(pts))
      .withSeriesMetaData(PlugInfo.knownSeriesMetaData())
      .withHeaderMetaData(PlugInfo.knownHeaderMetaData())
      .build();
  }
}
}