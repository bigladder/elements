/*
Copyright (C) 2014-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import elements.commands.DocCopySelected;
import elements.commands.DocPaste;

import gui.IManagedRef;

public class DataGridCopyPaste implements ICopyPaste
{
  public var _ref:IManagedRef;
  public function DataGridCopyPaste(ref:IManagedRef)
  {
    _ref = ref;
  }
  public function doCopy():void {
    // trace('DataGridCopyPaste :: doCopy()');
    if (_ref == null) return;
    _ref.handleCommand(new DocCopySelected());
  }
  public function doPaste():void {
    // trace('DataGridCopyPaste :: doPaste()');
    if (_ref == null) return;
    _ref.handleCommand(new DocPaste());
  }
}
}