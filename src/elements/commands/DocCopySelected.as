/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;

import dse.MetaData;
import dse.SeriesSelection;

import elements.DataDisplayOptions;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPMap;
import pdt.IPVec;

import units.UnitConversions;

import utils.FormatUtils;

public class DocCopySelected implements IUndoableCommand
{
  public function DocCopySelected()
  {
  }
  public function run(state:*):HandlerReturn
  {
    // trace('DocCopySelected.run');
    var ds:DocState = state as DocState;
    var selections:IPVec = ds.selections;
    if (selections.length == 0) {
      // trace('Selection length is zero');
      return new HandlerReturn(ds, []);
    }
    // trace('Selection length is ' + selections.length);
    var data:Array = new Array;
    var rowData:Array = new Array;
    var unitsSelected:String;
    var md:MetaData;
    var val:*;
    var dp:Object;
    var hdr:Object = ds.dataSet.header;
    var displayUnits:String;
    var displayUnitsMap:IPMap = ds.displayUnits;
    var name:String;
    var ucf:Function;
    var seriesMetaData:Object = ds.dataSet.seriesMetaData;
    var points:IPVec = ds.dataSet.points;
    for (var rowIdx:int = 0; rowIdx < points.length; rowIdx++)
    {
      dp = points.getItemAt(rowIdx);
      for each (var seriesName:String in ds.shownDataAsArray)
      {
        for (var selIdx:int=0; selIdx < selections.length; selIdx++)
        {
          var selection:SeriesSelection =
            selections.getItemAt(selIdx) as SeriesSelection;
          if ((selection.isSelectedIndex(rowIdx)) &&
              (seriesName==selection.selectedSeries))
          {
            name = selection.selectedSeries;
            md = seriesMetaData[name];
            val = md.getter(hdr, dp);
            if (md.dataType == MetaData.DATA_TYPE__NUMBER) {
              var dispOpts:DataDisplayOptions = ds.displayOptionsFor(name);
              if (displayUnitsMap.containsKey(name))
              {
                displayUnits =
                  displayUnitsMap.valAt(name) as String;
              }
              else
              {
                displayUnits = md.baseUnits;
              }
              ucf = UnitConversions.get_(md.baseUnits, displayUnits);
              rowData.push(FormatUtils.numberToFormattedString(
                (ucf(val) * dispOpts.scale), dispOpts.precision));
            }
            else
            {
              rowData.push(val.toString());
            }
          }
        }
      }
      if (rowData.length > 0)
      {
        data.push(rowData.join('\t'));
      }
      rowData = new Array();
    }
    var cb:Clipboard = Clipboard.generalClipboard;
    cb.setData(ClipboardFormats.TEXT_FORMAT, data.join("\n"));
    return new HandlerReturn(ds, []);
  }
  public function get undoable():Boolean
  {
    return false;
  }
}
}