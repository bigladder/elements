/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import mx.collections.IList;

import spark.components.gridClasses.CellPosition;
import spark.components.gridClasses.GridColumn;

import dse.SeriesSelection;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;
import pdt.PersistentVector;

import utils.MapUtils;

public class DocSetGridSelection implements IUndoableCommand {
  public var _selectedCells:Vector.<CellPosition>;
  public var _columns:IList;

  public function DocSetGridSelection(selectedCells:Vector.<CellPosition>,
                                      columns:IList) {
    _selectedCells = selectedCells;
    _columns = columns;
  }
  public function run(state:*):HandlerReturn {
    //trace('Command: DocSetGridSelection');
    //trace('_selectedCells.length', _selectedCells.length);
    var docState:DocState = state as DocState;
    var cells:Array = toArray(_selectedCells);
    var indicesToName:Object = {};
    for (var i:int=0; i<cells.length; i++) {
      var cell:CellPosition = cells[i] as CellPosition;
      var field:String = (_columns[cell.columnIndex] as GridColumn).dataField;
      var rowIdx:int = cell.rowIndex;
      if (indicesToName.hasOwnProperty(field)) {
        (indicesToName[field] as Array).push(rowIdx);
      } else {
        indicesToName[field] = [rowIdx];
      }
    }
    //MapUtils.traceObject(indicesToName, 'indicesToName');
    var selections:Array = [];
    var ss:SeriesSelection;
    var rows:Array;
    var startIdx:int;
    var numVals:int;
    var lastRow:int;
    var points:IPVec = docState.dataSet.points;
    for each (var name:String in MapUtils.keys(indicesToName)) {
      rows = (indicesToName[name] as Array).sort();
      numVals = 0;
      lastRow = rows[0];
      startIdx = rows[0];
      for each (var row:int in rows) {
        if (row == lastRow) {
          numVals += 1;
        } else {
          ss = new SeriesSelection(points, name, startIdx, numVals);
          selections.push(ss);
          startIdx = row;
          lastRow = row;
          numVals = 1;
        }
        lastRow += 1;
      }
      ss = new SeriesSelection(points, name, startIdx, numVals);
      selections.push(ss);
    }
    var events:Array = [DocEvents.EVENT_SELECTION_CHANGED];
    var simplifiedSelections:IPVec = SeriesSelection.simplifySelections(
      PersistentVector.create(selections));
    return (new HandlerReturn(
      docState.settingSelectionsTo(simplifiedSelections),
      events));
  }
  public function get undoable():Boolean {
    return false;
  }
  public static function toArray(xs:Vector.<CellPosition>):Array {
    var ys:Array = [];
    for each (var x:CellPosition in xs) {
      ys.push(x);
    }
    return ys;
  }
}
}