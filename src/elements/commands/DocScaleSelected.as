/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class DocScaleSelected implements IUndoableCommand {
  public var _scaleFactor:Number;
  public function DocScaleSelected(scaleFactor:Number) {
    _scaleFactor = scaleFactor;
  }
  public function run(state:*):HandlerReturn {
    //trace('DocScaleSelected.run');
    var ds:DocState = state as DocState;
    if (!DocHandlers.isProperToolCall(ds, {scaleFactor:_scaleFactor},
      'scaleFactor')) {
      return new HandlerReturn(ds, []);
    }
    var ucfs:Object = ds.unitConversionFunctionsForSelection;
    var f:Function = function(x:Number):Number {
      return ucfs.d2b(ucfs.b2d(x) * _scaleFactor);
    };
    //trace('DocScaleSelected.run -- calling DocHandlers.mapSelected');
    var hr:HandlerReturn = (new DocSetFileModified()).run(
      DocHandlers.mapSelected(ds, f));
    //trace('DocScaleSelected.run -- DocHandlers.mapSelected returned');
    return new HandlerReturn(
      hr.state as DocState,
      [ DocEvents.EVENT_SERIES_DATA_CHANGED,
        DocEvents.EVENT_FILE_STATUS_CHANGED,
        DocEvents.EVENT_SELECTION_CHANGED
      ]);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}