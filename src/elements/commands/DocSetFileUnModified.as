/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.filesystem.File;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class DocSetFileUnModified implements IUndoableCommand {
  public var _file:File;
  public function DocSetFileUnModified(file:File=null) {
    _file = file;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var newDS1:DocState = ds.settingFileModifiedTo(false);
    var newDS2:DocState;
    var newDS3:DocState;
    if (_file != null) {
      newDS2 = newDS1.settingFileTo(_file);
      newDS3 = newDS2.settingFileNameTo(_file.name);
    } else {
      newDS2 = newDS1;
      newDS3 = newDS1;
    }
    var events:Array = [DocEvents.EVENT_FILE_STATUS_CHANGED];
    if (ds.fileModified != newDS3.fileModified) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    return new HandlerReturn(
      newDS3.settingFileTitleTo(newDS3.fileName),
      events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}