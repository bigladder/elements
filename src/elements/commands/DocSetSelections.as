/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;

public class DocSetSelections implements IUndoableCommand {
  // should be an IPVec of SeriesSelection
  public var _selections:IPVec;
  public function DocSetSelections(selections:IPVec) {
    _selections = selections;
  }
  public function run(state:*):HandlerReturn {
    //trace('DocSetSelections.run');
    var ds:DocState = state as DocState;
    var newDS:DocState = ds.settingSelectionsTo(_selections);
    var events:Array = [];
    if (ds.selections != newDS.selections) {
      events.push(DocEvents.EVENT_SELECTION_CHANGED);
    }
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}