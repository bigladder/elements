/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.filesystem.File;

import elements.AppEvents;
import elements.AppState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class AppOpenDocInNewWin implements IUndoableCommand {
  public var _file:File;
  public function AppOpenDocInNewWin(file:File) {
    _file = file;
  }
  public function run(state:*):HandlerReturn {
    var aSt:AppState = state;
    if (_file == null){
      return new HandlerReturn(aSt, []);
    }
    var numWins:int = aSt.numberOfOpenWindows;
    var appState2:AppState;
    var cmd:AppNewDocument = new AppNewDocument();
    var hr:HandlerReturn = cmd.run(aSt);
    appState2 = hr.state as AppState;
    var activeWindow:Object = appState2.activeWindow;
    var loadFileIntoDoc:Function = appState2.loadFileIntoDocFun;
    loadFileIntoDoc(_file, activeWindow);
    return new HandlerReturn(
      appState2, [AppEvents.EVENT_OPEN_WINDOWS_CHANGED]);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}