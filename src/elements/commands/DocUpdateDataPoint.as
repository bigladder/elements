/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.DataPoint;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class DocUpdateDataPoint implements IUndoableCommand {
  public var _dataField:String;
  public var _dataValue:Number;
  public var _index:int;
  public function DocUpdateDataPoint(dataField:String, dataValue:Number,
                                     index:int) {
    _dataField = dataField;
    _dataValue = dataValue;
    _index = index;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var ssIdx:int = ds.settingSetIndexFor(_dataField);
    var newDS:DocState =
      DataPoint.updateDocStateWithSettingsAndDocState(
        _dataField, _dataValue, ds, _index, ssIdx);
    var hr:HandlerReturn = (new DocSetFileModified()).run(newDS);
    var newDS2:DocState = hr.state as DocState;
    var events:Array = [DocEvents.EVENT_SERIES_DATA_CHANGED];
    if (ds.fileModified != newDS2.fileModified) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    return new HandlerReturn(newDS2, events);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}