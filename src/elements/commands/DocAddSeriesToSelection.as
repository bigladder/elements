/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.DataSet;
import dse.SeriesSelection;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;
import pdt.PersistentVector;

public class DocAddSeriesToSelection implements IUndoableCommand {
  public var _name:String;
  public function DocAddSeriesToSelection(name:String) {
    _name = name;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var dataSet:DataSet = ds.dataSet;
    var points:IPVec = dataSet.points;
    var numPoints:int = points.length;
    var entireSeries:SeriesSelection =
      new SeriesSelection(points, _name, 0, numPoints);
    var selections:IPVec = ds.selections;
    var newSelection:Array = new Array();
    for (var sIdx:int=0; sIdx < selections.length; sIdx++) {
      var s:SeriesSelection =
        selections.getItemAt(sIdx) as SeriesSelection;
      if (s.selectedSeries == entireSeries.selectedSeries) {
        continue;
      } else {
        newSelection.push(s);
      }
    }
    newSelection.push(entireSeries);
    var newSelectionVec:IPVec = PersistentVector.create(newSelection);
    var events:Array = [DocEvents.EVENT_SELECTION_CHANGED];
    if (SeriesSelection.proposedSelectionDiffers(selections, newSelectionVec)) {
      return new HandlerReturn(
        ds.settingSelectionsTo(newSelectionVec),
        events);
    }
    return new HandlerReturn(ds, []);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}