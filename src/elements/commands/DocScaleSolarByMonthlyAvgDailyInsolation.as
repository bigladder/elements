/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.DataSet;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.PersistentVector;

import plugins.Solar;

public class DocScaleSolarByMonthlyAvgDailyInsolation
  implements IUndoableCommand {
  public var _desiredHgh_MJ__m2:Number;
  public var _monthlyInsolation:Object;
  public function DocScaleSolarByMonthlyAvgDailyInsolation(
    desiredHgh_MJ__M2:Number, monthlyInsolation:Object) {
    _desiredHgh_MJ__m2 = desiredHgh_MJ__M2;
    _monthlyInsolation = monthlyInsolation;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var dataSet:DataSet = ds.dataSet;
    var points:Array = dataSet.pointsAsArray;
    var hdr:Object = dataSet.header;
    var metaDataMap:Object = dataSet.seriesMetaData;
    var year:int = _monthlyInsolation.year as int;
    var month:int = _monthlyInsolation.month as int;
    var newPoints:Array = Solar.scaleSolarByMonthlyAvgGH(
      points, hdr, metaDataMap, year, month, _desiredHgh_MJ__m2);
    var events:Array = [DocEvents.EVENT_SERIES_DATA_CHANGED];
    var hr:HandlerReturn = (new DocSetFileModified()).run(
      ds.settingDataSetTo(
        ds.dataSet.settingPointsTo(PersistentVector.create(newPoints))));
    return new HandlerReturn(hr.state as DocState, events);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}