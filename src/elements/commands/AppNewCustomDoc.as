/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import mx.core.IVisualElement;

import elements.AppEvents;
import elements.AppHandlers;
import elements.WindowOptions;
import elements.views.BaseWindow;

import gui.HandlerReturn;
import gui.IManagedRef;
import gui.IUndoableCommand;

public class AppNewCustomDoc implements IUndoableCommand {
  public var _view:IVisualElement;
  public var _docStateRef:IManagedRef;
  public var _checkClose:Boolean;
  public var _options:WindowOptions;
  public var _parentWindow:BaseWindow;
  public function AppNewCustomDoc(view:IVisualElement,
                                  docStateRef:IManagedRef,
                                  checkClose:Boolean=false,
                                  options:WindowOptions=null,
                                  parentWindow:BaseWindow=null) {
    this._view = view;
    this._docStateRef = docStateRef;
    this._checkClose = checkClose;
    this._options = options;
    this._parentWindow = parentWindow;
  }
  public function run(state:*):HandlerReturn {
    return new HandlerReturn(
      AppHandlers.addNewCustomDocAndSetActive(state,
        _view, _docStateRef, _checkClose, _options, _parentWindow),
      [AppEvents.EVENT_OPEN_WINDOWS_CHANGED]);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}