/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;

import dse.MetaData;
import dse.SeriesSelection;

import elements.DataDisplayOptions;
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;

import units.NoUnits;
import units.UnitConversions;

public class DocPaste implements IUndoableCommand
{
  public function DocPaste()
  {
  }
  public function run(state:*):HandlerReturn
  {
    // trace('DocPaste.run');
    var ds:DocState = state as DocState;
    var selections:IPVec = ds.selections;
    // trace('selections.length: ', selections.length);
    // trace('only one series selected?',
    //   SeriesSelection.onlyOneSeriesSelected(selections));
    for (var n:int=0; n<selections.length; n++)
    {
      var s:SeriesSelection = selections.getItemAt(n) as SeriesSelection;
      // trace('DocPaste.run :: selection', n, 'is', s.selectedSeries);
    }
    if (selections.length == 0)
    {
      return new HandlerReturn(ds, []);
    }
    if (!SeriesSelection.onlyOneSeriesSelected(selections))
    {
      // trace('DocPaste.run :: more than one series selected -- returning');
      return new HandlerReturn(ds, []);
    }
    // we will build a new selection as we paste to make sure the pasted
    // data and selection are one and the same
    var newSelection:SeriesSelection;
    // do paste
    var cb:Clipboard = Clipboard.generalClipboard;
    var contents:String =
      cb.getData(ClipboardFormats.TEXT_FORMAT) as String;
    // trace('DocPaste.run :: contents', contents);
    var rowSets:Array = contents.split(/[\n\r]+/);
    // trace('DocPaste.run :: rowSets', rowSets);
    var data:Array = [];
    var dataCols:Array;
    var colSets:Array;
    var num:Number;
    function trim(item:String, idx:int, arr:Array):void
    {
      if (item != "")
      {
        colSets = item.split(/\t/);
        dataCols = [];
        for each (var col:String in colSets)
        {
          //trace('pushing', col);
          dataCols.push(Number(col));
        }
        data.push(dataCols);
      }
    }
    rowSets.forEach(trim);
    //trace('DocPaste.run :: rowSets (after trim)', rowSets);
    //trace('DocPaste.run :: data (after trim)', data);
    var i:int;
    var selection:SeriesSelection;
    var maxLength:int;
    // below hard-coded to only use the startIndex of the first selection.
    // This should act as follows:
    // Begin pasting at the startIndex of the first cell in the selection and
    // move downwards until either
    // 1. we run out of data to paste OR
    // 2. we run out of places to paste the data (i.e., go to the end of the
    //    data column
    // data that is not convertable to numeric is skipped when trying to paste
    var hrTmp:HandlerReturn;
    var newDS:DocState = ds;
    var name:String;
    var startIdx:int;
    var md:MetaData;
    var baseUnits:String;
    var displayUnits:String;
    var tag:String;
    var ucf:Function;
    var ddo:DataDisplayOptions;
    // don't allow multiple selections to paste...
    for (var y:int=0; y<1; y++)
    {
      selection = selections.getItemAt(y) as SeriesSelection;
      newSelection = new SeriesSelection(
        selection.dataPoints,
        selection.selectedSeries,
        selection.startIndex,
        0);
      name = selection.selectedSeries;
      md = ds.dataSet.metaDataFor(name);
      ddo = ds.displayOptionsFor(name);
      if (!md.editable)
      {
        continue;
      }
      baseUnits = md.baseUnits;
      displayUnits = ds.displayUnitsFor(name);
      tag = UnitConversions.tag(displayUnits, baseUnits);
      var mult:Number = 1.0 / ddo.scale;
      if (baseUnits != NoUnits.NONE)
      {
        if (UnitConversions.CONVERT.hasOwnProperty(tag))
        {
          ucf = UnitConversions.CONVERT[tag] as Function;
        }
        else
        {
          ucf = function(x:Number):Number { return x; };
        }
      }
      else
      {
        ucf = function(x:Number):Number { return x; };
      }
      var args:Object;
      startIdx = selection.startIndex;
      if (data.length == 1)
      {
        maxLength = Math.min(selection.numberOfValues,
          ds.dataSet.points.length - startIdx);
        for (i=0; i < maxLength; i++)
        {
          newSelection = newSelection.increaseSelection();
          num = mult * ucf(Number(data[0][y]));
          hrTmp = (new DocUpdateDataPoint(name, num, startIdx + i)).run(newDS);
          newDS = hrTmp.state as DocState;
        }
      }
      else
      {
        maxLength = Math.min(data.length, ds.dataSet.points.length - startIdx);
        for (i=0; i < maxLength; i++)
        {
          newSelection = newSelection.increaseSelection();
          num = mult * ucf(Number(data[i][y]));
          //trace('DocPaste.run :: pasting', num, ' at row ', startIdx + i);
          hrTmp = (new DocUpdateDataPoint(name, num, startIdx + i)).run(newDS);
          newDS = hrTmp.state as DocState;
        }
      }
    }
    var hr:HandlerReturn = (new DocSetFileModified()).run(newDS);
    var newDS2:DocState = hr.state as DocState;
    var hr2:HandlerReturn = (new DocSetSelection(newSelection)).run(newDS2);
    var newDS3:DocState = hr2.state as DocState;
    var events:Array = [ DocEvents.EVENT_SERIES_DATA_CHANGED ];
    if (newDS3.fileModified != ds.fileModified ) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    events.push(DocEvents.EVENT_SELECTION_CHANGED);
    //MapUtils.traceObject(newDS2.dataSet.points.getItemAt(0), 'points[0]');
    //MapUtils.traceObject(newDS2.dataSet.points.getItemAt(1), 'points[1]');
    //MapUtils.traceObject(newDS2.dataSet.points.getItemAt(2), 'points[2]');
    //MapUtils.traceObject(newDS2.dataSet.points.getItemAt(3), 'points[3]');
    return new HandlerReturn(newDS3, events);
  }
  public function get undoable():Boolean
  {
    return true;
  }
}
}