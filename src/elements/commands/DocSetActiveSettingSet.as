/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.MetaData;
import dse.SeriesSelection;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;

public class DocSetActiveSettingSet implements IUndoableCommand {
  public var _index:int;
  public function DocSetActiveSettingSet(index:int) {
    _index = index;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var selections:IPVec = ds.selections;
    var newDS:DocState = ds;
    var events:Array = [];
    if (SeriesSelection.onlyOneSeriesSelected(selections)) {
      var md:MetaData = ds.metaDataForSelectedSeries;
      if (md != null) {
        //trace('settingSet for ' + md.name + ' =');
        //trace((md.settingSets[args.index as int] as Array).join(', '));
        newDS = ds.settingActiveSettingSetsTo(
          ds.activeSettingSets.assoc(md.name, _index));
        events.push(DocEvents.EVENT_ACTIVE_SETTING_SET_CHANGED);
      }
    }
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}