/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;
import pdt.PersistentVector;

public class DocRemoveDataColumn implements IUndoableCommand {
  public var _seriesName:String;
  public function DocRemoveDataColumn(seriesName:String) {
    _seriesName = seriesName;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var shownDataColumns:IPVec = ds.shownData;
    var mutableCols:Array = new Array();
    var foundColToRemove:Boolean = false;
    var itemName:String;
    for (var idx:int=0; idx < shownDataColumns.length; idx++) {
      itemName = shownDataColumns.getItemAt(idx) as String;
      if (_seriesName == itemName) {
        foundColToRemove = true;
        continue;
      } else {
        mutableCols.push(itemName);
      }
    }
    if (!foundColToRemove) {
      return new HandlerReturn(ds, []);
    }
    var newDS:DocState = ds.settingShownDataTo(
      PersistentVector.create(mutableCols));
    var events:Array = [ DocEvents.EVENT_DISPLAYED_DATA_COLUMNS_CHANGED ];
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}