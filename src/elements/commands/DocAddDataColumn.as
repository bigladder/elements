/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;

public class DocAddDataColumn implements IUndoableCommand {
  public var _seriesName:String;
  public function DocAddDataColumn(seriesName:String) {
    _seriesName = seriesName;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var shownDataColumns:IPVec = ds.shownData;
    for (var i:int=0; i<shownDataColumns.count; i++) {
      var itemName:String = shownDataColumns.nth(i) as String;
      if (itemName == _seriesName) {
        return new HandlerReturn(ds, []);
      }
    }
    var newDS:DocState = ds.settingShownDataTo(shownDataColumns.cons(
      _seriesName));
    return new HandlerReturn(
      newDS,
      [DocEvents.EVENT_DISPLAYED_DATA_COLUMNS_CHANGED]);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}