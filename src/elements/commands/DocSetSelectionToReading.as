/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.SeriesSelection;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.PersistentVector;

public class DocSetSelectionToReading implements IUndoableCommand {
  public var _name:String;
  public var _index:int;
  public function DocSetSelectionToReading(name:String, index:int) {
    _name = name;
    _index = index;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var newDS:DocState = ds.settingSelectionsTo(PersistentVector.create([
      new SeriesSelection(ds.dataSet.points, _name, _index, 1)]));
    var events:Array = [DocEvents.EVENT_SELECTION_CHANGED];
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}