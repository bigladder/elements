/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements
{
import spark.components.TextInput;
import spark.components.gridClasses.GridItemEditor;

import dse.MetaData;

import elements.commands.DocSetHeaderField;

import gui.IManagedRef;

import units.NoUnits;
import units.UnitConversions;
import units.UnitInfo;

import utils.FormatUtils;
import utils.MapUtils;

public class PropValGridItemEditor extends GridItemEditor
{
  public var _dsRef:IManagedRef;
  public var _textInput:TextInput;
  public var _dataPoint:Object;
  public var _value:Number;
  public var _stringValue:String;

  public function PropValGridItemEditor(docStateRef:IManagedRef)
  {
    super();
    _dsRef = docStateRef;
    _textInput = new TextInput();
    _textInput.visible = false;
    _textInput.percentHeight = 100.0;
    _textInput.percentWidth = 100.0;
    _dataPoint = null;
    _value = NaN;
    _stringValue = "";
    addElement(_textInput);
  }
  override public function get data():Object
  {
    return _dataPoint;
  }
  override public function set data(dataPoint:Object):void
  {
    var ds:DocState = DocHandlers.readDocState(_dsRef);
    var hdrMD:Object = ds.dataSet.headerMetaData;
    _dataPoint = dataPoint;
    if (hdrMD.hasOwnProperty(dataPoint.property))
    {
      var md:MetaData = hdrMD[dataPoint.property] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__NUMBER)
      {
        _value = Number(dataPoint.value);
      }
      else if (md.dataType == MetaData.DATA_TYPE__STRING)
      {
        _value = NaN;
        _stringValue = dataPoint.value as String;
        this.value = _stringValue; // dataPoint.value;
      }
      else
      {
        _value = NaN;
        _stringValue = dataPoint.value.toString();
        this.value = _stringValue; // dataPoint.value;
      }
      if (!isNaN(_value) && (md.baseUnits != NoUnits.NONE))
      {
        var us:String = ds.displayUnitSystem;
        var qom:String = md.quantityOfMeasure;
        var targetUnits:String = UnitInfo.unitsBySystemAndQOM(us, qom);
        var tag:String = UnitConversions.tag(md.baseUnits, targetUnits);
        if (UnitConversions.CONVERT.hasOwnProperty(tag))
        {
          var ucf:Function = UnitConversions.CONVERT[tag] as Function;
          var convertedVal:Number = ucf(Number(dataPoint.value));
          var precision:int = DataDisplayOptions.DEFAULT_PRECISION;
          _stringValue = FormatUtils.numberToFormattedString(
            convertedVal, precision
          );
        }
        else
        {
          _stringValue = dataPoint.value.toString();
        }
        this.value = _value;
      }
      else if (!isNaN(_value))
      {
        _stringValue = FormatUtils.numberToFormattedString(_value, precision);
        this.value = _value;
      }
    }
    else
    {
      this.value = dataPoint.value;
      _stringValue = dataPoint.value.toString();
    }
  }
  override public function set value(val:Object):void
  {
    var ds:DocState = DocHandlers.readDocState(_dsRef);
    var hdrMD:Object = ds.dataSet.headerMetaData;
    if (hdrMD.hasOwnProperty(_dataPoint.property))
    {
      var md:MetaData = hdrMD[_dataPoint.property] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__STRING)
      {
        _textInput.text = val.toString();
      }
      else if (md.dataType == MetaData.DATA_TYPE__NUMBER)
      {
        if (md.baseUnits == NoUnits.NONE)
        {
          _textInput.text = Number(val).toString();
        }
        else
        {
          var us:String = ds.displayUnitSystem;
          var qom:String = md.quantityOfMeasure;
          var targetUnits:String = UnitInfo.unitsBySystemAndQOM(us, qom);
          var baseUnits:String = md.baseUnits;
          var tag:String = UnitConversions.tag(baseUnits, targetUnits);
          if (UnitConversions.CONVERT.hasOwnProperty(tag))
          {
            var ucf:Function = UnitConversions.CONVERT[tag] as Function;
            var convertedVal:Number = ucf(Number(val));
            var precision:int = DataDisplayOptions.DEFAULT_PRECISION;
            _textInput.text = FormatUtils.numberToFormattedString(
              convertedVal, precision);
          }
          else
          {
            _textInput.text = val.toString();
          }
        }
      }
      else
      {
        trace('...PropValGridItemEditor::type is unknown!', md.dataType);
      }
    }
    else
    {
      _textInput.text = val.toString();
    }
  }
  override public function get value():Object
  {
    var ds:DocState = DocHandlers.readDocState(_dsRef);
    var hdrMD:Object = ds.dataSet.headerMetaData;
    if (hdrMD.hasOwnProperty(_dataPoint.property))
    {
      var md:MetaData = hdrMD[_dataPoint.property] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__STRING)
      {
        return _textInput.text;
      }
      else if (md.dataType == MetaData.DATA_TYPE__NUMBER)
      {
        return Number(_textInput.text);
      }
      else
      {
        trace('PropValGridItemEditor:get value -- unhandled dataType!');
        return _textInput.text;
      }
    }
    else
    {
      trace('PropValGridItemEditor:get value -- no metaData specified!');
      return _textInput.text;
    }
  }
  override public function save():Boolean {
    if (!validate()) return false;
    var ds:DocState = DocHandlers.readDocState(_dsRef);
    var hdrMD:Object = ds.dataSet.headerMetaData;
    if (hdrMD.hasOwnProperty(_dataPoint.property))
    {
      var md:MetaData = hdrMD[_dataPoint.property] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__STRING)
      {
        _dsRef.handleCommand(
          new DocSetHeaderField(_dataPoint.property, this.value)
        );
        _dataPoint.value = this.value as String;
        return true;
      }
      if ((md.dataType == MetaData.DATA_TYPE__NUMBER) &&
          isNaN(Number(this.value)))
      {
        return false;
      }
      var isNumeric:Boolean = md.dataType == MetaData.DATA_TYPE__NUMBER;
      if (isNumeric && !isNaN(Number(this.value)))
      {
        if (md.baseUnits != NoUnits.NONE)
        {
          var us:String = ds.displayUnitSystem;
          var qom:String = md.quantityOfMeasure;
          var targetUnits:String = UnitInfo.unitsBySystemAndQOM(us, qom);
          var baseUnits:String = md.baseUnits;
          var tag:String = UnitConversions.tag(targetUnits, baseUnits);
          if (UnitConversions.CONVERT.hasOwnProperty(tag))
          {
            var ucf:Function = UnitConversions.CONVERT[tag] as Function;
            var convertedVal:Number = ucf(Number(this.value));
            var precision:int = DataDisplayOptions.DEFAULT_PRECISION;
            var valToSaveRepr:String = FormatUtils.numberToFormattedString(
              convertedVal, precision);
            if (valToSaveRepr != _stringValue)
            {
              _dsRef.handleCommand(new DocSetHeaderField(
                _dataPoint.property, convertedVal));
              _dataPoint.value = convertedVal;
              return true;
            }
            else
            {
              return true;
            }
          }
          else
          {
            _dsRef.handleCommand(
              new DocSetHeaderField(_dataPoint.property, this.value)
            );
            _dataPoint.value = this.value.toString();
            return true;
          }
        }
        else
        {
          _dsRef.handleCommand(
            new DocSetHeaderField(_dataPoint.property, this.value)
          );
          _dataPoint.value = this.value.toString();
          return true;
        }
      }
    }
    else
    {
      trace('PropValGridItemEditor::save -- no metaData available!');
    }
    // treat as string
    _dsRef.handleCommand(
      new DocSetHeaderField(_dataPoint.property, this.value)
    );
    _dataPoint.value = this.value.toString();
    return true;
  }
  override protected function validate():Boolean {
    //trace('PropValGridItemEditor.validate');
    var ok:Boolean = true;
    var ds:DocState = DocHandlers.readDocState(_dsRef);
    var hdrMD:Object = ds.dataSet.headerMetaData;
    var md:MetaData = hdrMD[_dataPoint.property] as MetaData;
    var err:Number = 0.0;
    var hdr:Object = MapUtils.update(ds.dataSet.header,
      [_dataPoint.property, this.value]);
    MapUtils.traceObject(hdr, 'hdr');
    if (md.constraintFunc != null) {
      //trace('checking constraint:', name);
      err = md.constraintFunc(hdr, {});
      if (err != 0.0) {
        //trace('constraint violated for', name);
        ok = false;
      }
    }
    return ok;
  }
  override public function setFocus():void {
    _textInput.visible = true;
    _textInput.setFocus();
    _textInput.validateNow();
  }
}
}