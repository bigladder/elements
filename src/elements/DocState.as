/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import flash.filesystem.File;
import flash.system.Capabilities;

import dse.DataSet;
import dse.MetaData;
import dse.SeriesSelection;

import pdt.IPMap;
import pdt.IPVec;

import units.UnitConversions;
import units.UnitSystems;

public class DocState {
  public var _dataSet:DataSet;
  public var _displayUnits:IPMap;
  public var _selections:IPVec;
  public var _displayOptionsMap:IPMap;
  public var _shownData:IPVec;
  public var _fileName:String;
  public var _title:String;
  public var _alwaysPlainTitle:Boolean;
  public var _file:File;
  public var _fileModified:Boolean;
  public var _activeSettingSets:IPMap;
  public function DocState(builder:DocStateBuilder) {
    this._activeSettingSets = builder.activeSettingSets;
    this._dataSet = builder.dataSet;
    this._displayOptionsMap = builder.displayOptionsMap;
    this._displayUnits = builder.displayUnits;
    this._file = builder.file;
    this._fileName = builder.fileName;
    this._title = builder.title;
    this._alwaysPlainTitle = builder.alwaysPlainTitle;
    this._fileModified = builder.fileModified;
    this._selections = builder.selections;
    this._shownData = builder.shownData;
  }
  public function get dataSet():DataSet {
    return this._dataSet;
  }
  public function settingDataSetTo(ds:DataSet):DocState {
    return DocStateBuilder.fromDocState(this).withDataSet(ds).build();
  }
  public function get displayUnits():IPMap {
    return this._displayUnits;
  }
  public function displayUnitsFor(name:String):String {
    var du:IPMap = this.displayUnits;
    if (du.containsKey(name)){
      return du.valAt(name) as String;
    }
    var md:MetaData = this.dataSet.metaDataFor(name);
    return md.baseUnits;
  }
  public function get displayUnitSystem():String {
    var du:IPMap = this.displayUnits;
    if (du == null) {
      return UnitSystems.SI;
    }
    if (!du.containsKey(UnitSystems.UNIT_SYSTEM)) {
      return UnitSystems.SI;
    }
    return du.valAt(UnitSystems.UNIT_SYSTEM) as String;
  }
  public function settingDisplayUnitsTo(du:IPMap):DocState {
    return DocStateBuilder.fromDocState(this).withDisplayUnits(du).build();
  }
  public function get selections():IPVec {
    return this._selections;
  }
  public function settingSelectionsTo(ss:IPVec):DocState {
    return DocStateBuilder.fromDocState(this).withSelections(ss).build();
  }
  public function selectionN(n:int):SeriesSelection {
    return this.selections.nth(n) as SeriesSelection;
  }
  public function get displayOptionsMap():IPMap {
    return this._displayOptionsMap;
  }
  public function displayOptionsFor(name:String):DataDisplayOptions {
    return this._displayOptionsMap.valAt(name) as DataDisplayOptions;
  }
  public function settingDataOptionsTo(dopts:IPMap):DocState {
    return DocStateBuilder.fromDocState(this)
      .withDisplayOptionsMap(dopts)
      .build();
  }
  public function get shownData():IPVec {
    return this._shownData;
  }
  public function get shownDataAsArray():Array {
    return this.shownData.toArray();
  }
  public function settingShownDataTo(sd:IPVec):DocState {
    return DocStateBuilder.fromDocState(this).withShownData(sd).build();
  }
  public function get fileName():String {
    return this._fileName;
  }
  public function settingFileNameTo(fn:String):DocState {
    return DocStateBuilder.fromDocState(this)
      .withFileName(fn)
      .withTitle(fn)
      .build();
  }
  public function get title():String {
    if (this._alwaysPlainTitle) return this._title;
    var postfix:String = "";
    if (Capabilities.os.indexOf("Windows") >= 0) {
      postfix = " - Elements";
    }
    var modified:String = "";
    if (this.fileModified) {
      modified = "*";
    }
    return this._title + modified + postfix;
  }
  public function get plainTitle():String {
    return this._title;
  }
  public function settingFileTitleTo(t:String):DocState {
    return DocStateBuilder.fromDocState(this).withTitle(t).build();
  }
  public function get file():File {
    return this._file;
  }
  public function settingFileTo(f:File):DocState {
    return DocStateBuilder.fromDocState(this).withFile(f).build();
  }
  public function get fileModified():Boolean {
    return this._fileModified;
  }
  public function settingFileModifiedTo(mod:Boolean):DocState {
    return DocStateBuilder.fromDocState(this).withFileModified(mod).build();
  }
  public function get activeSettingSets():IPMap {
    return this._activeSettingSets;
  }
  public function settingActiveSettingSetsTo(ass:IPMap):DocState {
    return DocStateBuilder.fromDocState(this)
      .withActiveSettingSets(ass)
      .build();
  }
  /* metaDataForSelectedSeries
  Returns the meta data for selected series selections starting with the
  first selected series. If nothing is selected, return null. */
  public function get metaDataForSelectedSeries():MetaData {
    var selections:IPVec = this.selections;
    if (selections.length == 0) {
      return null;
    }
    if (!SeriesSelection.onlyOneSeriesSelected(selections)) return null;
    var name:String =
      (selections.getItemAt(0) as SeriesSelection).selectedSeries;
    return this.dataSet.metaDataFor(name);
  }
  public function settingSetIndexFor(attr:String):int {
    var idx:int;
    var activeSettingSet:IPMap = this.activeSettingSets;
    if (activeSettingSet.containsKey(attr)) {
      idx = activeSettingSet.valAt(attr) as int;
    } else {
      idx = 0;
    }
    return idx;
  }
  public function get selectedSettingSet():Array {
    var ss:Array;
    var md:MetaData = this.metaDataForSelectedSeries;
    var idx:int = this.settingSetIndexFor(md.name);
    if (md.settingSets.length == 0) {
      ss = [];
    } else {
      ss = md.settingSets[idx] as Array;
    }
    return ss;
  }
  // assumes we have a proper selection: something selected but only from a
  // single data series.
  public function get unitConversionFunctionsForSelection():Object {
    var displayUnitsMap:IPMap = this.displayUnits;
    var name:String = this.selectionN(0).selectedSeries;
    var md:MetaData = this.metaDataForSelectedSeries;
    var baseUnits:String = md.baseUnits;
    var displayUnits:String =
      displayUnitsMap.valAt(name, baseUnits) as String;
    var d2b:Function = UnitConversions.get_(displayUnits, baseUnits);
    var b2d:Function = UnitConversions.get_(baseUnits, displayUnits);
    return {d2b:d2b, b2d:b2d};
  }
}
}