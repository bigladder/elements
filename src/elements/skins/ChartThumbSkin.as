////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2003-2006 Adobe Macromedia Software LLC and its licensors.
//  All Rights Reserved. The following is Source Code and is subject to all
//  restrictions on such code as contained in the End User License Agreement
//  accompanying this product. If you have received this file from a source
//  other than Adobe, then your use, modification, or distribution of this file
//  requires the prior written permission of Adobe.
//
////////////////////////////////////////////////////////////////////////////////
package elements.skins {
import mx.core.UIComponent;

public class ChartThumbSkin extends UIComponent {
    private static const HEIGHT:uint = 38;
    private static const COLOR:int = 0x666666;

    public function ChartThumbSkin():void {
        super();
    }

    override public function get width():Number {
        return(4);
    }

    override public function get measuredWidth():Number {
        return(4);
    }

    override public function get height():Number {
        return(HEIGHT);
    }

    override public function get measuredHeight():Number {
        return(HEIGHT);
    }

    override protected function
    updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
        super.updateDisplayList(unscaledWidth, unscaledHeight);
        graphics.clear();
        graphics.lineStyle(4, COLOR, 0.5);
        graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
    }
  }
}