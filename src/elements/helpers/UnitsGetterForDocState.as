/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers {
import dse.MetaData;

import elements.DocState;
import elements.DocHandlers;

import gui.IManagedRef;

import units.UnitConversions;

public class UnitsGetterForDocState implements IGetUnits {
    public var _sc:IManagedRef;
    public function UnitsGetterForDocState(sc:IManagedRef) {
        _sc = sc;
    }

    private function getDocState():DocState {
        return DocHandlers.readDocState(_sc);
    }

    public function getDisplayUnitsFor(name:String):String {
        return getDocState().displayUnitsFor(name);
    }

    private function getUnitPairFor(name:String):Object {
        var ds:DocState = getDocState();
        var md:MetaData = ds.dataSet.metaDataFor(name);
        var baseUnits:String = md.baseUnits;
        var dispUnits:String = ds.displayUnitsFor(name);
        if (dispUnits == null) dispUnits = baseUnits;
        return {base:baseUnits, disp:dispUnits};
    }

    public function
    getToDisplayUnitConversionFunctionFor(name:String):Function {
        var pr:Object = getUnitPairFor(name);
        return UnitConversions.get_(pr.base, pr.disp);
    }

    public function
    getToBaseUnitConversionFunctionFor(name:String):Function {
        var pr:Object = getUnitPairFor(name);
        return UnitConversions.get_(pr.disp, pr.base);
    }
}
}