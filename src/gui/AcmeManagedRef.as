/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui {
import utils.MapUtils;

public class AcmeManagedRef implements IManagedRef {
    public static const EVENT_STATE_CHANGED:String = 'event_state_changed';

    private var _stateRef:IRef;
    private var _eventDispatch:Object;
    private var _undoStack:Array;
    private var _redoStack:Array;

    public function AcmeManagedRef(state:*) {
        _stateRef = new Ref(state);
        _eventDispatch = new Object();
        _undoStack = new Array();
        _redoStack = new Array();
    }

    public function resetUndoRedo():void {
        _undoStack = new Array();
        _redoStack = new Array();
    }

    public function onRequest(request:String, f:Function, events:Array):void {
        _eventDispatch[request] = {func:f, events:events};
    }

    public function handleRequest(request:String, args:Object):void {
        if (_eventDispatch.hasOwnProperty(request)) {
            var payload:Object = _eventDispatch[request];
            var f:Function = payload.func as Function;
            var events:Array = payload.events as Array;
            var name:String;
            if (events.length > 0) {
              name = events[0];
            } else {
              name = "UNDO";
            }
        } else {
            trace('GeneralStateController.handleRequest :: Error!');
            MapUtils.traceObject(_eventDispatch,
                'GeneralStateController._eventDispatch');
            trace('request:', request);
            throw new Error("GeneralStateController.handleRequest ::" +
                " attempt to dispatch on unknown request: " + request);
        }
        var currentState:* = _stateRef.deref();
        try {
            var newState:* = f(currentState, args);
        } catch (e:Error) {
            trace('GeneralStateController.handleRequest :: Error!');
            trace('--------------------');
            MapUtils.traceObject(currentState, 'currentState');
            MapUtils.traceObject(args, 'args');
            throw e;
        }
        _undoStack.push({state:currentState, name:name});
        _stateRef.setRef(newState);
        for each (var event:String in events){
          _stateRef.notifySubscribers(event);
        }
    }
    public function handleCommand(c:IUndoableCommand):void {
      var currentState:* = _stateRef.deref();
      var hr:HandlerReturn = c.run(currentState);
      var newState:* = hr.state;
      if (c.undoable) {
        _undoStack.push({state:currentState, name:"UNDO"});
      }
      _stateRef.setRef(newState);
      for each (var event:String in hr.events) {
        _stateRef.notifySubscribers(event);
      }
    }
    public function undo():void {
        if (_undoStack.length > 0) {
            _redoStack.push({state:_stateRef.deref(), name:"UNDO"});
            _stateRef.setRef(_undoStack.pop().state);
            _stateRef.notifySubscribers(EVENT_STATE_CHANGED);
        }
    }

    public function redo():void {
        if (_redoStack.length > 0) {
            _undoStack.push({state:_stateRef.deref(), name:"REDO"});
            _stateRef.setRef(_redoStack.pop().state);
            _stateRef.notifySubscribers(EVENT_STATE_CHANGED);
        }
    }

    public function read():* {
        return _stateRef.read();
    }

    public function registerSubscriberFunction(func:Function):void {
        _stateRef.registerSubscriberFunction(func);
    }

    public function removeSubscriberFunction(func:Function):void {
        _stateRef.removeSubscriberFunction(func);
    }

    public function notifySubscribers(topic:String):void {
      _stateRef.notifySubscribers(topic);
    }
}
}