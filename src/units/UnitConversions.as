/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {

public class UnitConversions {
    public static function tag(fromUnit:String, toUnit:String):String {
        return fromUnit + ' -> ' + toUnit;
    }

    // EXTERNAL API
    public static function get_(fromUnit:String, toUnit:String):Function {
        if (fromUnit == toUnit) {
            return identity;
        }
        var tag_:String = tag(fromUnit, toUnit);
        if (CONVERT.hasOwnProperty(tag_)) {
            return CONVERT[tag_];
        }
        throw new Error('UnitConversions :: Attempted to lookup ' +
            'non-existing tag, ' + tag_ + '.');
    }

    public static function
    displayToBase(displayValue:Number, displayUnits:String,
                  displayScaleFactor:Number, baseUnits:String):Number {
        var ucf:Function = get_(displayUnits, baseUnits);
        return ucf(displayValue / displayScaleFactor);
    }

    public static function
    baseToDisplay(baseValue:Number, baseUnits:String,
                  displayScaleFactor:Number, displayUnits:String):Number {
        var ucf:Function = get_(baseUnits, displayUnits);
        return ucf(baseValue) * displayScaleFactor;
    }

    private static function
    _set(fromUnit:String, toUnit:String, f:Function):void {
        CONVERT[tag(fromUnit, toUnit)] = f;
    }

    private static const
    identity:Function = function(x:Number):Number { return x; };

    public static const CONVERT:Object = new Object();


    // ** NONE ***************
    _set(NoUnits.NONE, NoUnits.NONE, identity);


    // ** SCALE UNITS ********
    _set(ScaleUnits.TENTHS, ScaleUnits.TENTHS, identity);


    // ** TEMPERATURE ********
    public static function C_to_K(C:Number):Number {
        return C + 273.15;
    }
    _set(TemperatureUnits.C, TemperatureUnits.K, C_to_K);

    public static function C_to_F(C:Number):Number {
        return 32.0 + 9.0 * C / 5.0;
    }
    _set(TemperatureUnits.C, TemperatureUnits.F, C_to_F);

    public static function C_to_R(C:Number):Number {
        return C * 9.0 / 5.0 + 32.0 + 459.67;
    }
    _set(TemperatureUnits.C, TemperatureUnits.R, C_to_R);

    _set(TemperatureUnits.K, TemperatureUnits.K, identity);

    public static function K_to_C(K:Number):Number {
        return K - 273.15;
    }
    _set(TemperatureUnits.K, TemperatureUnits.C, K_to_C);

    public static function K_to_F(K:Number):Number {
        var K_to_C:Function = get_(TemperatureUnits.K, TemperatureUnits.C);
        var C_to_F:Function = get_(TemperatureUnits.C, TemperatureUnits.F);
        return C_to_F(K_to_C(K));
    }
    _set(TemperatureUnits.K, TemperatureUnits.F, K_to_F);

    _set(TemperatureUnits.F, TemperatureUnits.F, identity);

    public static function F_to_C(F:Number):Number {
        return (F - 32.0) * 5.0 / 9.0;
    }
    _set(TemperatureUnits.F, TemperatureUnits.C, F_to_C);

    public static function R_to_C(R:Number):Number {
            return (R - 459.67 - 32.0) * 5.0 / 9.0;
    }
    _set(TemperatureUnits.R, TemperatureUnits.C, R_to_C);


    // ** ANGLE **************
    _set(AngleUnits.DEGREES, AngleUnits.DEGREES, identity);

    public static function deg_to_rads(deg:Number):Number {
        return deg * Math.PI / 180.0;
    }
    _set(AngleUnits.DEGREES, AngleUnits.RADIANS, deg_to_rads);

    public static function deg_to_compDir(deg:Number):Number {
        while (deg < 0.0)
        {
            deg = deg + 360.0;
        }
        var deg_:Number = deg%360.0;
        return deg_ / 22.5;
    }
    _set(AngleUnits.DEGREES, AngleUnits.COMPASS_DIRECTION, deg_to_compDir);

    public static function rad_to_degs(rad:Number):Number {
        return 180.0 * rad / Math.PI;
    }
    _set(AngleUnits.RADIANS, AngleUnits.DEGREES, rad_to_degs);

    public static function compDir_to_degs(cd:Number):Number {
        return cd * 22.5; // cd == 0 is north
    }
    _set(AngleUnits.COMPASS_DIRECTION, AngleUnits.DEGREES, compDir_to_degs);


    // ** PRESSURE **********
    public static function Pa_to_kPa(Pa:Number):Number {
        return Pa / 1000.0;
    }
    _set(PressureUnits.PA, PressureUnits.KPA, Pa_to_kPa);

    public static function bar_to_kPa(bar:Number):Number {
        return bar * Math.pow(10, 2);
    }
    _set(PressureUnits.BAR, PressureUnits.KPA, bar_to_kPa);

    public static function atm_to_kPa(atm:Number):Number {
        return atm * 1.01325 * Math.pow(10, 2);
    }
    _set(PressureUnits.ATM, PressureUnits.KPA, atm_to_kPa);

    public static function torr_to_kPa(torr:Number):Number {
        return torr * 133.322 / 1000.0;
    }
    _set(PressureUnits.TORR, PressureUnits.KPA, torr_to_kPa);

    public static function psi_to_kPa(psi:Number):Number {
        return psi * 6.895;
    }
    _set(PressureUnits.PSI, PressureUnits.KPA, psi_to_kPa);

    _set(PressureUnits.KPA, PressureUnits.KPA, identity);

    public static function inHg_to_kPa(inHg:Number):Number {
        return 3.38639 * inHg;
    }
    _set(PressureUnits.IN_HG, PressureUnits.KPA, inHg_to_kPa);

    public static function inHgx100_to_kPa(inHgx100:Number):Number {
        return 3.38639 * (inHgx100 / 100.0);
    }
    _set(PressureUnits.IN_HG_x_100, PressureUnits.KPA, inHgx100_to_kPa);

    public static function kPa_to_Pa(kPa:Number):Number {
        return kPa * 1000.0;
    }
    _set(PressureUnits.KPA, PressureUnits.PA, kPa_to_Pa);

    public static function kPa_to_bar(kPa:Number):Number {
        return kPa * Math.pow(10, -2);
    }
    _set(PressureUnits.KPA, PressureUnits.BAR, kPa_to_bar);

    public static function kPa_to_atm(kPa:Number):Number {
        return kPa * 9.8692 * Math.pow(10, -3);
    }
    _set(PressureUnits.KPA, PressureUnits.ATM, kPa_to_atm);

    public static function kPa_to_torr(kPa:Number):Number {
        return kPa * 7.5006;
    }
    _set(PressureUnits.KPA, PressureUnits.TORR, kPa_to_torr);

    public static function kPa_to_psi(kPa:Number):Number {
        return kPa * 145.04 * Math.pow(10, 9);
    }
    _set(PressureUnits.KPA, PressureUnits.PSI, kPa_to_psi);

    _set(PressureUnits.KPA, PressureUnits.KPA, identity);

    public static function kPa_to_inHg(kPa:Number):Number {
        return kPa / 3.38639;
    }
    _set(PressureUnits.KPA, PressureUnits.IN_HG, kPa_to_inHg);

    public static function kPa_to_inHgx100(kPa:Number):Number {
        return kPa * 100.0 / 3.38639;
    }
    _set(PressureUnits.KPA, PressureUnits.IN_HG_x_100, kPa_to_inHgx100);


    // ** DENSITY ***********
    private static const TO_LBM__FT3:Number = 0.062428;

    public static function kg__m3_to_lbm__ft3(kg__m3:Number):Number {
        var lbm__ft3:Number = kg__m3 * TO_LBM__FT3;
        return lbm__ft3;
    }
    _set(DensityUnits.KG__M3, DensityUnits.LBM__FT3, kg__m3_to_lbm__ft3);

    public static function lbm__ft3_to_kg__m3(lbm__ft3:Number):Number {
        var kg__m3:Number = lbm__ft3 / TO_LBM__FT3;
        return kg__m3;
    }
    _set(DensityUnits.LBM__FT3, DensityUnits.KG__M3, lbm__ft3_to_kg__m3);


    // ** SPECIFIC VOLUME ***
    public static function m3__kg_to_ft3__lbm(m3__kg:Number):Number {
        return m3__kg * 16.018463373960138 ;
    }
    _set(SpecificVolumeUnits.M3__KG, SpecificVolumeUnits.FT3__LBM,
        m3__kg_to_ft3__lbm);

    public static function ft3__lbm_to_m3__kg(ft3__lbm:Number):Number {
        return ft3__lbm * 0.06242796057614461;
    }
    _set(SpecificVolumeUnits.FT3__LBM, SpecificVolumeUnits.M3__KG,
        lbm__ft3_to_kg__m3);


    // ** ENERGY DENSITY ****
    private static const TO_BTU__lbm:Number = 0.0004299226;

    public static function kJ__kg_to_BTU__lbm(kJ__kg:Number):Number {
        var J__kg:Number = kJ__kg * 1000.0;
        var BTU__lbm:Number = J__kg * TO_BTU__lbm;
        return BTU__lbm;
    }
    _set(SpecificEnergyUnits.KJ__KG,
        SpecificEnergyUnits.BTU__LBM, kJ__kg_to_BTU__lbm);

    public static function BTU__lbm_to_kJ__kg(BTU__lbm:Number):Number {
        var J__kg:Number = BTU__lbm / TO_BTU__lbm;
        var kJ__kg:Number = J__kg / 1000.0;
        return kJ__kg;
    }
    _set(SpecificEnergyUnits.BTU__LBM,
        SpecificEnergyUnits.KJ__KG, BTU__lbm_to_kJ__kg);


    // ** IRRADIANCE ********
    _set(IrradianceUnits.W__M2, IrradianceUnits.W__M2, identity);
    _set(IrradianceUnits.BTU__HR_FT2, IrradianceUnits.BTU__HR_FT2, identity);
    private static const TO_W__M2:Number = 3.154590745063049;

    public static function BTU__hr_ft2_to_W__m2(BTU__hr_ft2:Number):Number {
        var W__m2:Number = BTU__hr_ft2 * TO_W__M2;
        return W__m2;
    }
    _set(IrradianceUnits.BTU__HR_FT2,
        IrradianceUnits.W__M2, BTU__hr_ft2_to_W__m2);

    public static function W__m2_to_BTU__hr_ft2(W__m2:Number):Number {
        var BTU__hr_ft2:Number = W__m2 / TO_W__M2;
        return BTU__hr_ft2;
    }
    _set(IrradianceUnits.W__M2,
        IrradianceUnits.BTU__HR_FT2, W__m2_to_BTU__hr_ft2);


    // ** IRRADIATION ********
    _set(IrradiationUnits.WHR__M2, IrradiationUnits.WHR__M2, identity);
    _set(IrradiationUnits.BTU__FT2, IrradiationUnits.BTU__FT2, identity);
    private static const TO_Wh__M2:Number = 3.154590745063049;

    public static function BTU__ft2_to_Wh__m2(BTU__ft2:Number):Number {
        var Wh__m2:Number = BTU__ft2 * TO_Wh__M2;
        return Wh__m2;
    }
    _set(IrradiationUnits.BTU__FT2,
        IrradiationUnits.WHR__M2, BTU__ft2_to_Wh__m2);

    public static function Wh__m2_to_BTU__ft2(Wh__m2:Number):Number {
        var BTU__ft2:Number = Wh__m2 / TO_Wh__M2;
        return BTU__ft2;
    }
    _set(IrradiationUnits.WHR__M2,
      IrradiationUnits.BTU__FT2, Wh__m2_to_BTU__ft2);


    // ** SPEED *************
    private static const KNOTS_TO_M__S:Number = 0.514445;

    public static function knots_to_m__s(knots:Number):Number {
        var m__s:Number = knots * KNOTS_TO_M__S;
        return m__s;
    }
    _set(SpeedUnits.KNOTS, SpeedUnits.M__S, knots_to_m__s);

    public static function m__s_to_knots(m__s:Number):Number {
        var knots:Number = m__s / KNOTS_TO_M__S;
        return knots;
    }
    _set(SpeedUnits.M__S, SpeedUnits.KNOTS, m__s_to_knots);

    public static function m__s_to_mph(m__s:Number):Number {
        return m__s * 2.2369362920544025;
    }
    _set(SpeedUnits.M__S, SpeedUnits.MPH, m__s_to_mph);

    public static function mph_to_m__s(mph:Number):Number {
        return mph * 0.44704;
    }
    _set(SpeedUnits.MPH, SpeedUnits.M__S, mph_to_m__s);


    // ** LENGTH **
    private static const METERS_TO_FEET:Number = 3.28084;

    public static function m_to_ft(m:Number):Number {
      return m * METERS_TO_FEET;
    }
    _set(LengthUnits.METER, LengthUnits.FOOT, m_to_ft);

    public static function ft_to_m(ft:Number):Number {
      return ft / METERS_TO_FEET;
    }
    _set(LengthUnits.FOOT, LengthUnits.METER, ft_to_m);


    // ** ELAPSED TIME UNITS *
    public static function hrs_to_seconds(hrs:Number):Number {
        return hrs * 3600.0;
    }
    _set(ElapsedTimeUnits.HOURS, ElapsedTimeUnits.SECONDS, hrs_to_seconds);

    public static function hrs_to_minutes(hrs:Number):Number {
        return hrs * 60.0;
    }
    _set(ElapsedTimeUnits.HOURS, ElapsedTimeUnits.MINUTES, hrs_to_minutes);

    public static function hrs_to_days(hrs:Number):Number {
        return hrs / 24.0;
    }
    _set(ElapsedTimeUnits.HOURS, ElapsedTimeUnits.DAYS, hrs_to_days);

    public static function hrs_to_weeks(hrs:Number):Number {
        return hrs / (24.0 * 7.0);
    }
    _set(ElapsedTimeUnits.HOURS, ElapsedTimeUnits.WEEKS, hrs_to_weeks);

    public static function seconds_to_hrs(secs:Number):Number {
        return secs / 3600.0;
    }
    _set(ElapsedTimeUnits.SECONDS, ElapsedTimeUnits.HOURS, seconds_to_hrs);

    public static function minutes_to_hrs(mins:Number):Number {
        return mins / 60.0;
    }
    _set(ElapsedTimeUnits.MINUTES, ElapsedTimeUnits.HOURS, minutes_to_hrs);

    public static function days_to_hrs(days:Number):Number {
        return days * 24.0;
    }
    _set(ElapsedTimeUnits.DAYS, ElapsedTimeUnits.HOURS, days_to_hrs);

    public static function weeks_to_hrs(weeks:Number):Number {
        return weeks * 24.0 * 7.0;
    }
    _set(ElapsedTimeUnits.WEEKS, ElapsedTimeUnits.HOURS, weeks_to_hrs);

    // ** IRRADIATION UNITS *
    public static function Wh__m2_to_MJ__m2(Wh__m2:Number):Number {
        return Wh__m2 * 0.0036;
    }
    _set(IrradiationUnits.WHR__M2, IrradiationUnits.MJ__M2, Wh__m2_to_MJ__m2);

    public static function MJ__m2_to_Wh__m2(MJ__m2:Number):Number {
        return MJ__m2 * 2500.0 / 9.0;
    }
    _set(IrradiationUnits.MJ__M2, IrradiationUnits.WHR__M2, MJ__m2_to_Wh__m2);

    public static function MJ__m2_to_J__m2(MJ__m2:Number):Number {
        return MJ__m2 * 1e6;
    }
    _set(IrradiationUnits.MJ__M2, IrradiationUnits.J__M2, MJ__m2_to_J__m2);

    public static function J__m2_to_MJ__m2(J__m2:Number):Number {
        return J__m2 / 1e6;
    }
    _set(IrradiationUnits.J__M2, IrradiationUnits.MJ__M2, J__m2_to_MJ__m2);

    public static function BTU__ft2_to_MJ__m2(BTU__ft2:Number):Number {
        return BTU__ft2 * 0.011356526682226975;
    }
    _set(IrradiationUnits.BTU__FT2, IrradiationUnits.MJ__M2,
        BTU__ft2_to_MJ__m2);

    public static function MJ__m2_to_BTU__ft2(MJ__m2:Number):Number {
        return MJ__m2 * 88.05509184115292;
    }
    _set(IrradiationUnits.MJ__M2, IrradiationUnits.BTU__FT2,
        MJ__m2_to_BTU__ft2);

    public static function kBTU__ft2_to_MJ__m2(kBTU__ft2:Number):Number {
        return kBTU__ft2 * 11.356526682226976;
    }
    _set(IrradiationUnits.kBTU__FT2, IrradiationUnits.MJ__M2,
        kBTU__ft2_to_MJ__m2);

    public static function MJ__m2_to_kBTU__ft2(MJ__m2:Number):Number {
        return MJ__m2 * 0.08805509184115293;
    }
    _set(IrradiationUnits.MJ__M2, IrradiationUnits.kBTU__FT2,
        MJ__m2_to_kBTU__ft2);
}
}