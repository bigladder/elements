/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class IrradianceUnits {
    public static const WATTS_PER_METER2:String = 'W/m2';
    public static const W__M2:String = WATTS_PER_METER2;
    public static const BTU_PER_HOUR_FOOT2:String = 'btu/hr-ft2';
    public static const BTU__HR_FT2:String = BTU_PER_HOUR_FOOT2;

    public static const QUANTITY_OF_MEASURE:String =
        'irradiance (radiant energy per unit area)';
    public static const SI:String = WATTS_PER_METER2;
    public static const IP:String = BTU_PER_HOUR_FOOT2;

    public static const AVAILABLE_UNITS:Array = [
        W__M2, BTU__HR_FT2];
}
}