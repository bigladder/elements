/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class DensityUnits {
    public static const KILOGRAMS_PER_METER3:String = "kg/m3";
    public static const KG__M3:String = KILOGRAMS_PER_METER3;
    public static const POUNDS_PER_FOOT3:String = "lbs/ft3";
    public static const LBM__FT3:String = POUNDS_PER_FOOT3;

    public static const QUANTITY_OF_MEASURE:String = 'density';
    public static const SI:String = KILOGRAMS_PER_METER3;
    public static const IP:String = POUNDS_PER_FOOT3;

    public static const AVAILABLE_UNITS:Array = [
        KILOGRAMS_PER_METER3, POUNDS_PER_FOOT3];
}
}