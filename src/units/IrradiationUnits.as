/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class IrradiationUnits {
    public static const WATT_HOURS_PER_METER2:String = 'Wh/m2';
    public static const WHR__M2:String = WATT_HOURS_PER_METER2;
    public static const MEGA_JOULES_PER_METER2:String = 'MJ/m2';
    public static const MJ__M2:String = MEGA_JOULES_PER_METER2;
    public static const JOULES_PER_METER2:String = 'J/m2';
    public static const J__M2:String = JOULES_PER_METER2;
    public static const BTU_PER_FOOT2:String = 'BTU/ft2';
    public static const BTU__FT2:String = BTU_PER_FOOT2;
    public static const KBTU_PER_FOOT2:String = 'kBTU/ft2';
    public static const kBTU__FT2:String = KBTU_PER_FOOT2;

    public static const QUANTITY_OF_MEASURE:String =
        'irradiation (incident energy per unit area on a surface)';
    public static const SI:String = WHR__M2;
    public static const IP:String = BTU__FT2;

    public static const AVAILABLE_UNITS:Array = [
        WHR__M2, BTU__FT2
    ];
}
}