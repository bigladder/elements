/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class AssociativeArray {
    private var _keys:Array;
    private var _vals:Array;

    public function AssociativeArray(keys:Array, vals:Array) {
        _keys = new Array;
        _vals = new Array;
        var keyIdx:int;
        var keyLength:int = 0;
        for (var i:int = 0; i < keys.length; i++) {
            keyIdx = _keys.indexOf(keys[i]);
            if(keyIdx == -1) {
                _keys[keyLength] = keys[i];
                _vals[keyLength] = vals[i];
                keyLength += 1;
            } else {
                _vals[keyIdx] = vals[i];
            }
        }
        if(_keys.length != _vals.length) {
            throw new Error(
                "Problem with initialize:\nkeys: " + _keys.toString() +
                "\nvals: " + _vals.toString());
        }
    }

    public static function
    fromItems(items:Array, itemToKey:Function,
              itemToVal:Function):AssociativeArray {
        var keys:Array = new Array;
        var vals:Array = new Array;
        var keyIdx:int;
        var keyName:*;
        for each (var item:* in items) {
            keyName = itemToKey(item);
            keyIdx = keys.indexOf(keyName);
            if (keyIdx == -1) {
                keys.push(keyName);
                vals.push(new Array);
                keyIdx = keys.length - 1;
            }
            vals[keyIdx].push(itemToVal(item));
        }
        return new AssociativeArray(keys, vals);
    }

    public function keys():Array {
        return _keys;
    }

    public function vals():Array {
        return _vals;
    }

    public function hasKey(key:*):Boolean {
        var keyIdx:int = indexOf(key);
        var returnVal:Boolean;
        if (keyIdx == -1) {
            returnVal = false;
        } else {
            returnVal = true;
        }
        return returnVal;
    }

    public function getValByKey(key:*):* {
        var returnVal:*;
        var keyIdx:int = indexOf(key);
        if (hasKey(key)) {
            returnVal = _vals[keyIdx];
        } else {
            returnVal = null;
        }
        return returnVal;
    }

    public function setKeyVal(key:*, val:*):void {
        var keyIdx:int = indexOf(key);
        if (keyIdx == -1) {
            _keys.push(key);
            _vals.push(val);
        } else {
            _vals[keyIdx] = val;
        }
    }

    public function toString():String {
        var a:Array = [];
        for (var i:int = 0; i < _keys.length; i++) {
            a.push(_keys[i].toString() + ":" + _vals[i].toString());
        }
        return "{" + a.join(", ") + "}";
    }

    public function numItems():int {
        return _keys.length;
    }

    private function indexOf(key:*):int {
        var idx:int = -1;
        for (var i:int = 0; i < _keys.length; i++) {
            if(key == _keys[i]) {
                idx = i;
                break;
            }
        }
        return idx;
    }
}
}