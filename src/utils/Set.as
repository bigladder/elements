/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class Set {
    // isMember -- tests if item is a member of the given set
    public static function
    isMember(item:String, set_:Object):Boolean {
        return set_.hasOwnProperty(item);
    }

    // make -- creates a set from the given items
    public static function
    make(items:Array):Object {
        var set_:Object = new Object();
        for each (var item:String in items) {
            set_[item] = true;
        }
        return set_;
    }

    // count -- the number of elements belonging to the given set
    public static function
    count(set_:Object):int {
        return MapUtils.keys(set_).length;
    }

    // the number of items in array that are members of the given set
    public static function
    numberOfMembers(items:Array, set_:Object):int {
        var num:int = 0;
        for each (var item:String in items) {
            if (isMember(item, set_)) {
                num += 1;
            }
        }
        return num;
    }

    // Two sets are equal if they contain exactly the same elements
    public static function
    equal(set1:Object, set2:Object):Boolean {
        /*
        var set1Keys:Array = MapUtils.attributes(set1);
        var set2Keys:Array = MapUtils.attributes(set2);
        if (set1Keys.length != set2Keys) {
            return false;
        }
        for each (var key:String in set1Keys) {
            if (!set2.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
        */
        var attrs1:Array = MapUtils.keys(set1).sort();
        var attrs2:Array = MapUtils.keys(set2).sort();
        if (attrs1.length == attrs2.length) {
            for (var idx:int=0; idx<attrs1.length; idx++) {
                if (attrs1[idx] != attrs2[idx]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /* Union of the sets A and B, denoted A U B, is the set of all elements
    that are a member of A, or B, or both. The union of {1, 2, 3} and
    {2, 3, 4} is the set {1, 2, 3, 4} */
    public static function
    union(set1:Object, set2:Object):Object {
        var attrs1:Array = MapUtils.keys(set1);
        var attrs2:Array = MapUtils.keys(set2);
        var attrs:Array = attrs1.concat(attrs2);
        return make(attrs);
    }

    /* Set intersection: Given sets A and B, the set intersection, A ∩ B, is
    the set of all elements that are members of both A and B. For example, the
    intersection of {1, 2, 3} and {2, 3, 4} is the set {2, 3}. */
    public static function
    intersection(set1:Object, set2:Object):Object {
        var intersect:Array = new Array();
        for each (var item:String in MapUtils.keys(union(set1, set2))) {
            if (set1.hasOwnProperty(item) && set2.hasOwnProperty(item)) {
                intersect.push(item);
            }
        }
        return make(intersect);
    }

    /* Set difference: given sets U and A, the set difference, "U \ A", is
    the set of all members of U that are not members of A. The set difference
    {1,2,3} \ {2,3,4} is {1} while {2,3,4} \ {1,2,3} is {4}. */
    public static function
    difference(set1:Object, set2:Object):Object {
        var diff:Array = new Array();
        for each (var item:String in MapUtils.keys(set1)) {
            if (!set2.hasOwnProperty(item)) {
                diff.push(item);
            }
        }
        return make(diff);
    }

    /* Symmetric difference is the set of all elements that are members of
    only one of A and B (members of one set, but not both). For instance, for
    the sets {1,2,3} and {2,3,4}, the symmetric difference set is {1,4}. It is
    the set difference of the union and the intersection, (A ∪ B) \ (A ∩ B). */
    public static function
    symmetricDifference(set1:Object, set2:Object):Object {
        var unionSet:Object = union(set1, set2);
        var intersectSet:Object = intersection(set1, set2);
        return difference(unionSet, intersectSet);
    }
}
}