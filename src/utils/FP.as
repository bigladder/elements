/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class FP {
    public static function
    range(start:Number, stop:Number, step:Number):Array {
        var xs:Array = new Array();
        for (var x:Number = start; x < stop; x += step) {
            xs.push(x);
        }
        return xs;
    }

    // reduce :: (a -> b -> a) -> a -> [b] -> a
    public static function
    reduce(f:Function, init:*, xs:Array):* {
        var result:* = init;
        for each (var x:* in xs) {
            result = f(result, x);
        }
        return result;
    }

    public static function
    map(f:Function, xs:Array):Array {
        var result:Array = new Array();
        for each (var x:* in xs) {
            result.push(f(x));
        }
        return result;
    }
    
    public static function
    filter(f:Function, xs:Array):Array {
        var result:Array = new Array();
        for (var n:int=0; n < xs.length; n++){
            var x:* = xs[n];
            if (f(x, n)) {
                result.push(x);
            }
        }
        return result;
    }

    public static function
    zipWith(f:Function, xs:Array, ys:Array):Array {
        var len:int = Math.min(xs.length, ys.length);
        var zs:Array = new Array();
        for (var idx:int = 0; idx < len; idx++) {
            zs.push(f(xs[idx], ys[idx]));
        }
        return zs;
    }

    /* unfold :: (b -> (a, b)) -> b -> (b -> Bool) -> [a]
       unfold :: step, seed, finished -> Array
       step should take a seed value and produce a two-tuple of value to add
       to list and next seed respectively finished should take the next seed
       and determine whether to finish. This is also called an "Anamorphism"
       -- the opposite of fold; builds up a collection from a seed similar to
       Haskell's unfold but includes a "finished" function since we don't
       want to do infinite lists in ActionScript since ActionScript is eager
       (as opposed to Haskell which is lazy or by-need evaluation). */
    public static function
    unfold(step:Function, seed:*, finished:Function):Array {
        var result:Array = new Array();
        var theSeed:* = seed;
        var stepResult:Array;
        var finishedResult:Boolean = false;
        while (!finishedResult) {
            finishedResult = finished(theSeed);
            if (!finishedResult) {
                stepResult = step(theSeed);
                result.push(stepResult[0]);
                theSeed = stepResult[1];
            }
        }
        return result;
    }

    public static function
    cycleUntilLength(cycle:Array, length:int):Array {
        var results:Array = new Array();
        var cycleLength:int = cycle.length;
        for (var idx:int = 0; idx < length; idx++) {
            results.push(cycle[idx % cycleLength]);
        }
        return results;
    }

    /* intersperse :: a -> [a] -> [a]
      NOTE: This can be replaced by Array.join(x) for string applications.
      The 'intersperse' function takes an element and a list and
      intersperses that element between the elements of the list.
      For example,
      intersperse ',' "abcde" == "a,b,c,d,e"

      intersperse             :: a -> [a] -> [a]
      intersperse _   []      = []
      intersperse sep (x:xs)  = x : prependToAll sep xs */
    public static function
    intersperse(sep:*, xs:Array):Array {
        if ((xs == null) || (xs.length == 0)) {
            return [];
        }
        if (xs.length == 1) {
            return [xs[0]];
        }
        var newXs:Array = new Array();
        newXs.push(xs[0]);
        for each (var x:* in xs.slice(1, xs.length)) {
            newXs.push(sep);
            newXs.push(x);
        }
        return newXs;
    }
}
}