/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.dse {
import dse.MetaData;
import dse.SeriesSelection;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

public class TestSeriesSelection {
    public var s1:SeriesSelection;
    public var pts:IPVec;

    [Before]
    public function setUp():void {
        pts = PersistentVector.create([
            {A: 1, B: 2, C: 3},
            {A: 4, B: 5, C: 6},
            {A: 7, B: 8, C: 9}]);
        s1 = new SeriesSelection(pts, 'A', 0, 2);
    }

    [Test]
    public function testGet_endIndex():void {
        Assert.assertEquals(1, s1.endIndex);
    }

    [Test]
    public function testMapOver():void {
        var xs:Array = s1.mapOver(
            function(x:Number):Number { return 10.0 * x; },
            pts, {}, MetaData.makeValueGetter('A'));
        Assert.assertEquals(2, xs.length);
        Assert.assertEquals(10, xs[0]);
        Assert.assertEquals(40, xs[1]);
    }

    [Test]
    public function testIsSelectedIndex():void {
        Assert.assertEquals(true, s1.isSelectedIndex(0));
        Assert.assertEquals(true, s1.isSelectedIndex(1));
        Assert.assertEquals(false, s1.isSelectedIndex(2));
    }

    [Test]
    public function testOnlyOneSeriesSelected():void {
        var s2:SeriesSelection = new SeriesSelection(pts, 'A', 2, 1);
        var ss:IPVec = PersistentVector.create([s1, s2]);
        Assert.assertTrue(SeriesSelection.onlyOneSeriesSelected(ss));
    }

    [Test]
    public function testOnlyOneSeriesSelected2():void {
        var s2:SeriesSelection = new SeriesSelection(pts, 'B', 2, 1);
        var ss:IPVec = PersistentVector.create([s1, s2]);
        Assert.assertEquals(false,
            SeriesSelection.onlyOneSeriesSelected(ss));
    }

    [Test]
    public function testProposedSelectionDiffers():void {
        var s1s:IPVec = PersistentVector.create([
            new SeriesSelection(pts, 'A', 1, 2),
            new SeriesSelection(pts, 'C', 2, 1)]);
        var s2s:IPVec = PersistentVector.create([
            new SeriesSelection(pts, 'A', 1, 2),
            new SeriesSelection(pts, 'C', 2, 1)]);
        Assert.assertEquals(false, SeriesSelection.proposedSelectionDiffers(
            s1s, s2s));
    }

    [Test]
    public function testProposedSelectionDiffers1():void {
        var s1s:IPVec = PersistentVector.create([
            new SeriesSelection(pts, 'A', 1, 2),
            new SeriesSelection(pts, 'C', 2, 1)]);
        var s2s:IPVec = PersistentVector.create([
            new SeriesSelection(pts, 'A', 1, 2),
            new SeriesSelection(pts, 'C', 0, 3)]);
        Assert.assertEquals(true, SeriesSelection.proposedSelectionDiffers(
            s1s, s2s));
    }

    [Test]
    public function testSelectedData():void {
        var xs:Array = s1.selectedData(pts, {}, MetaData.makeValueGetter('A'));
        Assert.assertEquals(2, xs.length);
        Assert.assertEquals(1, xs[0]);
        Assert.assertEquals(4, xs[1]);
    }

    [Test]
    public function testGet_selectedRows():void {
        var rows:Array = s1.selectedRows;
        Assert.assertEquals(2, rows.length);
        Assert.assertEquals(0, rows[0]);
        Assert.assertEquals(1, rows[1]);
    }

    [Test]
    public function testSeriesSelection():void {
        // pass conducted in the setUp...
    }

    [Test]
    public function testMapOver2():void {
        var f:Function = function(x:Number):Number {return x+1;};
        var xs:Array = s1.mapOver(f, pts, {}, MetaData.makeValueGetter('A'));
        Assert.assertEquals(2, xs.length);
        Assert.assertEquals(2, xs[0]);
        Assert.assertEquals(5, xs[1]);
    }

    [Test]
    public function testReduceOver():void {
        var f:Function = function(acc:Number, x:Number):Number {
            return acc + x;
        };
        var x:Number = s1.reduceOver(f, 0, pts, {},
            MetaData.makeValueGetter('A'));
        Assert.assertEquals(5, x);
    }

    [Test]
    public function testAverage():void {
        var x:Number = s1.average(pts, {}, MetaData.makeValueGetter('A'));
        MyAssert.almostEquals(2.5, x);
    }

    [Test]
    public function testSum():void {
      var x:Number = s1.sum(pts, {}, MetaData.makeValueGetter('A'));
      MyAssert.almostEquals(5.0, x);
    }

    [Test]
    public function testMinimum():void {
        var x:Number = s1.minimum(pts, {}, MetaData.makeValueGetter('A'));
        Assert.assertEquals(1, x);
    }

    [Test]
    public function testMaximum():void {
        var x:Number = s1.maximum(pts, {}, MetaData.makeValueGetter('A'));
        Assert.assertEquals(4, x);
    }
    [Test]
    public function testConsolidateSelectionsOnSameSeries():void {
      var pts:IPVec = PersistentVector.create([1,2,3,4,5]);
      var ss:Array = [
        new SeriesSelection(pts, "A", 0, 2),
        new SeriesSelection(pts, "A", 2, 1),
        new SeriesSelection(pts, "A", 3, 2)];
      var out:Array = SeriesSelection.consolidateSelectionsOnSameSeries(ss);
      var outSS:SeriesSelection = out[0] as SeriesSelection;
      Assert.assertEquals("A", outSS.selectedSeries);
      Assert.assertEquals(0, outSS.startIndex);
      Assert.assertEquals(4, outSS.endIndex);
      Assert.assertEquals(5, outSS.numberOfValues);
      Assert.assertEquals(1, out.length);
    }
    [Test]
    public function testConsolidateSelectionsOnSameSeries2():void {
      var pts:IPVec = PersistentVector.create([1,2,3,4,5]);
      var ss:Array = [
        new SeriesSelection(pts, "A", 2, 1),
        new SeriesSelection(pts, "A", 3, 2),
        new SeriesSelection(pts, "A", 0, 2)];
      var out:Array = SeriesSelection.consolidateSelectionsOnSameSeries(ss);
      var outSS:SeriesSelection = out[0] as SeriesSelection;
      Assert.assertEquals("A", outSS.selectedSeries);
      Assert.assertEquals(0, outSS.startIndex);
      Assert.assertEquals(4, outSS.endIndex);
      Assert.assertEquals(5, outSS.numberOfValues);
      Assert.assertEquals(1, out.length);
    }
    [Test]
    public function testConsolidateSelectionsOnSameSeries3():void {
      var pts:IPVec = PersistentVector.create([1,2,3,4,5]);
      var ss:Array = [
        new SeriesSelection(pts, "A", 3, 2),
        new SeriesSelection(pts, "A", 0, 2)];
      var out:Array = SeriesSelection.consolidateSelectionsOnSameSeries(ss);
      var outSS:SeriesSelection = out[0] as SeriesSelection;
      Assert.assertEquals(2, out.length);
    }
    [Test]
    public function testSimplifySelections():void {
      var pts:IPVec = PersistentVector.create([1,2,3,4,5]);
      var ss:IPVec = PersistentVector.create([
        new SeriesSelection(pts, "A", 3, 2),
        new SeriesSelection(pts, "B", 2, 1),
        new SeriesSelection(pts, "A", 0, 2)]);
      var out:IPVec = SeriesSelection.simplifySelections(ss);
      Assert.assertEquals(3, out.length);
    }
    [Test]
    public function testSimplifySelections2():void {
      var pts:IPVec = PersistentVector.create([1,2,3,4,5]);
      var ss:IPVec = PersistentVector.create([
        new SeriesSelection(pts, "A", 3, 2),
        new SeriesSelection(pts, "A", 2, 1),
        new SeriesSelection(pts, "A", 0, 2),
        new SeriesSelection(pts, "B", 0, 5),
        new SeriesSelection(pts, "C", 0, 2),
        new SeriesSelection(pts, "C", 2, 3)]);
      var out:IPVec = SeriesSelection.simplifySelections(ss);
      Assert.assertEquals(3, out.length);
    }
}
}