/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements.commands {
import elements.AppState;
import elements.AppStateBuilder;
import elements.commands.AppSetStartupMode;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import org.flexunit.Assert;

public class TestAppSetStartupMode {
  private var appRef:IManagedRef;
  [Before]
  public function setUp():void {
    var appSt:AppState = new AppStateBuilder().build();
    appRef = new AcmeManagedRef(appSt);
  }
  [Test]
  public function testAppSetStartupMode():void {
    var appSt1:AppState = appRef.read() as AppState;
    Assert.assertEquals(true, appSt1.startupMode);
    appRef.handleCommand(new AppSetStartupMode(false));
    var appSt2:AppState = appRef.read() as AppState;
    Assert.assertEquals(false, appSt2.startupMode);
  }
}
}