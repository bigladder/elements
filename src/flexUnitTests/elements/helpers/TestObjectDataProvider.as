/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements.helpers {
import mx.collections.ArrayCollection;

import elements.helpers.IProvideData;
import elements.helpers.ObjectDataProvider;

import flexunit.framework.Assert;

public class TestObjectDataProvider {
  public var obj:Object;
  public var dataProvider:IProvideData;

  [Before]
  public function setUp():void {
    obj = {a:10, b:20, c:30};
    dataProvider = new ObjectDataProvider(obj);
  }

  [Test]
  public function testProvideData():void {
    var xs:ArrayCollection = dataProvider.provideData() as ArrayCollection;
    Assert.assertEquals(3, xs.length);
  }

  [Test]
  public function testProvideGivenData():void {
    var xs:ArrayCollection =
      dataProvider.provideGivenData(["a", "b"]) as ArrayCollection;
    Assert.assertEquals(2, xs.length);
  }
}
}