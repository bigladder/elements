/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.plugins {
import dse.DataPoint;
import dse.MetaData;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import plugins.Psych;

import utils.MapUtils;

public class TestPsych {
    private var Tdb_C:Number;
    private var Twb_C:Number;
    private var p_kPa:Number;
    private var rs:Object;

    [Before]
    public function setUp():void {
        Tdb_C = 25.0;
        Twb_C = 20.0;
        p_kPa = 101.325;
        rs = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, Tdb_C,
            Psych.WET_BULB_TEMP_C, Twb_C,
            Psych.PRESSURE_kPa, p_kPa]);
    }

    [Test]
    public function testDerivation():void {
        var _get:Function = DataPoint.makeGetter(Psych.SERIES_META_DATA,
            rs, {});
        // reading set views
        Assert.assertEquals(Tdb_C, _get(Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(Twb_C, _get(Psych.WET_BULB_TEMP_C));
        Assert.assertEquals(p_kPa, _get(Psych.PRESSURE_kPa));
        Assert.assertEquals(
            Psych.relativeHumidity(Tdb_C, Twb_C, p_kPa),
            _get(Psych.RELATIVE_HUMIDITY));
        Assert.assertEquals(
            Psych.specVol_m3__kg(Tdb_C, Twb_C, p_kPa),
            _get(Psych.SPECIFIC_VOLUME_m3__kg));
        Assert.assertEquals(
            Psych.density_kg__m3(Tdb_C, Twb_C, p_kPa),
            _get(Psych.DENSITY_kg__m3));
        Assert.assertEquals(
            Psych.dewPointTemp_C(Tdb_C, Twb_C, p_kPa),
            _get(Psych.DEW_POINT_TEMP_C));
        Assert.assertEquals(
            Psych.enthalpy_kJ__kg(Tdb_C, Twb_C, p_kPa),
            _get(Psych.ENTHALPY_kJ__kg));
        Assert.assertEquals(
            Psych.humidityRatio(Tdb_C, Twb_C, p_kPa),
            _get(Psych.HUMIDITY_RATIO));
//        Assert.assertEquals(
//            Psych.degreeOfSaturation(Tdb_C, Twb_C, p_kPa),
//            _get(Psych.DEGREE_OF_SATURATION));
    }

    [Test]
    public function
    testDeriveWetBulb():void {
        var Twb_C:Number = Psych.Twb_from_p_Tdb_and_X(Psych.DEW_POINT_TEMP_C,
            25.0, 101.325, Psych.dewPointTemp_C(25.0, 20.0, 101.325));
        //trace("Twb_C", Twb_C);
        Assert.assertTrue(Math.abs(Twb_C - 20.0) < 1e-6);
    }

    [Test]
    public function
    testTwb_from_p_Tdb_and_X():void {
        var Twb_C:Number = Psych.Twb_from_p_Tdb_and_X(Psych.DEW_POINT_TEMP_C,
            -21.7, 101.8, -26.7);
        var startPoint:Object = {};
        startPoint[Psych.DRY_BULB_TEMP_C] = -21.7;
        // we don't know what wet bulb is but setting it to dry bulb is valid
        startPoint[Psych.WET_BULB_TEMP_C] = -21.7;
        startPoint[Psych.PRESSURE_kPa] = 101.8;
        var settings:Object = new Object();
        settings[Psych.DRY_BULB_TEMP_C] = startPoint[Psych.DRY_BULB_TEMP_C];
        settings[Psych.PRESSURE_kPa] = startPoint[Psych.PRESSURE_kPa];
        settings[Psych.DEW_POINT_TEMP_C] = startPoint[Psych.DEW_POINT_TEMP_C];
        var newDP:Object = DataPoint.update({}, startPoint,
            Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS, settings,
            Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        var newNewDP:Object = DataPoint._updateValuesWithinConstraints({},
            startPoint, Psych.CONSTRAINT_FUNC, newDP);
        trace('Twb_C =', Twb_C);
        trace('Twb_C from newDP =', newDP[Psych.WET_BULB_TEMP_C]);
        trace('Twb_C from newNewDP =', newNewDP[Psych.WET_BULB_TEMP_C]);
        var Tdew_md:MetaData = Psych.SERIES_META_DATA[Psych.DEW_POINT_TEMP_C];
        var RH_md:MetaData = Psych.SERIES_META_DATA[Psych.RELATIVE_HUMIDITY];
        trace('Tdew_C =', Tdew_md.getter({}, newNewDP));
        trace('RH =', RH_md.getter({}, newNewDP));
        MapUtils.traceObject(newDP, 'newDP');
        MapUtils.traceObject(newNewDP, 'newNewDP');
        Assert.assertEquals(0.0, Psych.CONSTRAINT_FUNC({}, newNewDP));
        Assert.assertTrue(!isNaN(newNewDP[Psych.WET_BULB_TEMP_C]));
    }

    [Test]
    public function testMatchEPWCalcs():void {
        trace('testMatchEPWCalcs');
        var Tdb_C:Number = -20.0;
        var RH:Number = 0.50;
        var f:Function = function (Tdb_C:Number, RH:Number, Ws:Number,
                                   pws_kPa:Number):void {
            var p_kPa:Number = 101.325;
            var Twb_C:Number = Psych.Twb_from_p_Tdb_and_X(
                Psych.RELATIVE_HUMIDITY, Tdb_C, p_kPa, RH);
            var pw_kPa:Number = pws_kPa * RH;
            var W:Number = 0.621945 * pw_kPa / (p_kPa - pw_kPa);
            var a:Number = Math.log(pw_kPa);
            var Tdew_C_01:Number = 6.54 + 14.526 * a + 0.7389 * a * a +
                0.09486 * a * a * a + 0.4569 * Math.pow(a, 0.1984);
            var Tdew_C_02:Number = 6.09 + 12.608 * a + 0.4959 * a * a;
            var Tdew_C:Number;
            if ((Tdew_C_01 >= 0.0) && (Tdew_C_01 <= 93.0)) {
                Tdew_C = Tdew_C_01;
            } else {
                Tdew_C = Tdew_C_02;
            }
            var Tdew_C_calced:Number = Psych.dewPointTemp_C(
                Tdb_C, Twb_C, p_kPa);
            if (false) {
                trace('Tdb_C', Tdb_C);
                trace('RH', RH);
                trace('p_kPa', p_kPa);
                trace('Twb_C', Twb_C);
                trace('Tdew_C_calced', Tdew_C_calced);
                trace('Tdew_C', Tdew_C);
            }
            MyAssert.almostEquals(Tdew_C, Tdew_C_calced, 1);
        };
        f(-20.0,  0.50, 0.0006373, 0.10324);
        f(-10.0,  0.50, 0.0016062, 0.25987);
        f( 10.0,  0.50, 0.007663,  1.2282);
        f( 20.0,  0.50, 0.014761,  2.3392);
        f( 20.0,  0.99, 0.014761,  2.3392);
        f( 20.0,  0.01, 0.014761,  2.3392);
        f( 30.0,  0.33, 0.027333,  4.2467);
        f( 45.0,  0.50, 0.065416,  9.5944);
        trace('----------------------');
    }

    [Test]
    public function testGeopotentialHeightToAltitude_m():void {
      Assert.assertEquals(0.0, Psych.geopotentialHeightToAltitude_m(0.0));
      MyAssert.almostEquals(
        1609.344,
        Psych.geopotentialHeightToAltitude_m(1608.937),
        1e-2);
    }

    [Test]
    public function testAltitudeToGeopotentialHeight_m():void {
      MyAssert.almostEquals(0.0, Psych.altitudeToGeopotentialHeight_m(0.0));
      MyAssert.almostEquals(
        1608.937,
        Psych.altitudeToGeopotentialHeight_m(1609.344),
        1e-2);
    }

    [Test]
    public function testStandardAtmPressure_kPa():void {
      MyAssert.almostEquals(
        101.325,
        Psych.standardAtmPressure_kPa(0.0),
        1e-2);
      MyAssert.almostEquals(
        89.875,
        Psych.standardAtmPressure_kPa(1000.0),
        1e-2);
    }

    [Test]
    public function testStandardTemperature_C():void {
      MyAssert.almostEquals(
        15.0,
        Psych.standardTemperature_C(0.0),
        1e-2);
      MyAssert.almostEquals(
        8.5,
        Psych.standardTemperature_C(1000.0),
        1e-2);
    }
}
}