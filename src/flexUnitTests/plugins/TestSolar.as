/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.plugins {
import mx.collections.ArrayCollection;

import dse.DataPoint;
import dse.DataSet;
import dse.DataSetBuilder;
import dse.DateTime;
import dse.MetaData;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import plugins.DateAndTime;
import plugins.Location;
import plugins.Solar;

import utils.MapUtils;

public class TestSolar {
    [Before]
    public function setUp():void {
    }

    [Test]
    public function
    testDerivedReading():void {
        var hdr:Object = MapUtils.mergeAll([
            Location.EXAMPLES['Greenwich, London UK'],
            DateAndTime.makeHeaderFromDate(0, 2012, 5, 29, 12, 0, 0)]);
        var dp:Object = Solar.makeDataPoint(100.0, 50.0);
        dp[DateAndTime.ELAPSED_TIME_hrs] = 0.0;
        var _get:Function = DataPoint.makeGetter(Solar.SERIES_META_DATA,
            dp, hdr);
        var sha:Number = _get(Solar.HOUR_ANGLE_deg);
        var azmNrth:Number = _get(Solar.AZIMUTH_ANGLE_FROM_NORTH_deg);
        var azmSth:Number = _get(Solar.AZIMUTH_ANGLE_FROM_SOUTH_deg);
        Assert.assertTrue(Math.abs(sha) < 4.0);
        Assert.assertTrue(Math.abs(azmNrth - 180.0) < 4.0);
        Assert.assertTrue(Math.abs(azmSth) < 4.0);
    }

    [Test]
    public function
    testUpdate01():void {
        var hdr:Object = MapUtils.mergeAll([
            plugins.Location.EXAMPLES['Greenwich, London UK'],
            DateAndTime.makeHeaderFromDate(0, 2012, 5, 29, 12, 0, 0)]);
        var dp:Object = Solar.makeDataPoint(300.0, 0.0);
        dp[DateAndTime.ELAPSED_TIME_hrs] = 0.0;
        var settings:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.GLOBAL_HORIZ_Wh__m2, 120.0,
            Solar.DIFFUSE_HORIZONTAL_Wh__m2, 120.0]);
        var newDP:Object = DataPoint.update(hdr, dp, Solar.CONSTRAINT_FUNC,
            Solar.DERIVATION_FUNCS, settings, Solar.MINS, Solar.MAXS,
            Solar.STATE_NAMES);
        var _get:Function = DataPoint.makeGetter(
            Solar.SERIES_META_DATA, newDP, hdr);
        var Igh:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
        var Ibn:Number = _get(Solar.BEAM_NORMAL_Wh__m2);
        var Idh:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2);
        //trace('------- testUpdate01 --------');
        //ObjectUtils.traceObject(newDP, 'newDP');
        //for each (var sm:SeriesMeta in Solar.SERIES_META_DATA) {
            //trace(sm.seriesName, sm.getter(hdr, newDP));
        //}
        //trace('Igh', Igh);
        //trace('Ibn', Ibn);
        //trace('Idh', Idh);
        //trace('-----------------------------');
        Assert.assertTrue(Math.abs(Igh - 120.0) < 2);
        Assert.assertTrue(Math.abs(Idh - 120.0) < 2);
    }

    [Test]
    public function
    testUpdate02():void {
        var hdr:Object = MapUtils.mergeAll([
            Location.EXAMPLES['Greenwich, London UK'],
            DateAndTime.makeHeaderFromDate(0,2012,1,1,12,0,0)]);
        var dp:Object = plugins.Solar.makeDataPoint(300.0, 0.0);
        dp[DateAndTime.ELAPSED_TIME_hrs] = 0.0;
        var settings:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.GLOBAL_HORIZ_Wh__m2, 400.0,
            Solar.BEAM_NORMAL_Wh__m2, 0.0]);
        var newDP:Object = DataPoint.update(hdr, dp, Solar.CONSTRAINT_FUNC,
            Solar.DERIVATION_FUNCS, settings, Solar.MINS,
            Solar.MAXS, Solar.STATE_NAMES);
        var _get:Function = DataPoint.makeGetter(Solar.SERIES_META_DATA,
            newDP, hdr);
        var Igh:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
        var Ibn:Number = _get(Solar.BEAM_NORMAL_Wh__m2);
        var Idh:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2);
        //trace('Igh', Igh);
        //trace('Ibn', Ibn);
        Assert.assertTrue(Math.abs(Igh - 400.0) < 1e-3);
        Assert.assertTrue(Math.abs(Ibn) < 1e-6);
        Assert.assertTrue(Math.abs(Idh - 400.0) < 1e-3);
    }

    [Test]
    public function
    testDeriveGlobalHorizSolarIrradiance():void {
        var hdr:Object = MapUtils.mergeAll([
            Location.EXAMPLES['Greenwich, London UK'],
            DateAndTime.makeHeaderFromDate(0,2012,1,1,0,0,0)]);
        var dp:Object = Solar.makeDataPoint(1.0, 140.0);
        dp[DateAndTime.ELAPSED_TIME_hrs] = 12.0;
        var _get:Function = DataPoint.makeGetter(Solar.SERIES_META_DATA,
            dp, hdr);
        var zenAng:Number = _get(Solar.ZENITH_ANGLE_deg);
        var Igh:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
        var Idh:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2);
        Assert.assertEquals(1.0, Idh);
        //trace('Igh=', Igh);
        //trace('ZenAng (deg)', zenAng);
    }

    [Test]
    public function
    testAvgCosZ():void {
        /* TODO: If Longitude is set to negative 110.95, this test fails! We
        need to go back through Solar.as and DateAndTime and make sure that
        there is a consistent interpretation of longitude, latitude, solar time,
        vs local time, etc. */
        var hdr:Object = MapUtils.makeObject([
            DateAndTime.REFERENCE_YEAR, 2012,
            DateAndTime.REFERENCE_MONTH, 1,
            DateAndTime.REFERENCE_DAY, 1,
            DateAndTime.REFERENCE_HOUR, 0,
            DateAndTime.REFERENCE_MINUTE, 0,
            DateAndTime.REFERENCE_SECOND, 0,
            Location.LATITUDE_deg, 32.13,
            Location.LONGITUDE_deg, -110.95,
            DateAndTime.TIME_ZONE, -7]);
        var pts:Array = [
            MapUtils.makeObject([
                DateAndTime.ELAPSED_TIME_hrs, 7.0,
                Solar.BEAM_NORMAL_Wh__m2, 140.0,
                Solar.DIFFUSE_HORIZONTAL_Wh__m2, 1.0]),
            MapUtils.makeObject([
                DateAndTime.ELAPSED_TIME_hrs, 8.0,
                Solar.BEAM_NORMAL_Wh__m2, 671,
                Solar.DIFFUSE_HORIZONTAL_Wh__m2, 28.0]),
            MapUtils.makeObject([
                DateAndTime.ELAPSED_TIME_hrs, 9.0,
                Solar.BEAM_NORMAL_Wh__m2, 856.0,
                Solar.DIFFUSE_HORIZONTAL_Wh__m2, 41.0]),
            MapUtils.makeObject([
                DateAndTime.ELAPSED_TIME_hrs, 10.0,
                Solar.BEAM_NORMAL_Wh__m2, 917.0,
                Solar.DIFFUSE_HORIZONTAL_Wh__m2, 53.0])];
        var serHdrs:Object = Solar.SERIES_META_DATA;
        var ptsv:IPVec = PersistentVector.create(pts);
        var ds:DataSet = (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(ptsv)
          .withSeriesMetaData(serHdrs)
          .withHeaderMetaData({})
          .build();
        var beta7:Number = (serHdrs[Solar.HOURLY_AVERAGED_COSZ]
            as MetaData).getter(hdr, pts[0]);
        var beta8:Number = (serHdrs[Solar.HOURLY_AVERAGED_COSZ]
            as MetaData).getter(hdr, pts[1]);
        var beta9:Number = (serHdrs[Solar.HOURLY_AVERAGED_COSZ]
            as MetaData).getter(hdr, pts[2]);
        var beta10:Number = (serHdrs[Solar.HOURLY_AVERAGED_COSZ]
            as MetaData).getter(hdr, pts[3]);
        var gs7:Number = (serHdrs[Solar.GLOBAL_HORIZ_Wh__m2]
            as MetaData).getter(hdr, pts[0]);
//        trace('avg cosZ @ 7 :', beta7);
//        trace('global solar :', gs7);
//        trace('should be    :', 2.0);
        Assert.assertTrue(Math.abs(gs7 - 2.0) < 10.0);
        var gs8:Number = (serHdrs[Solar.GLOBAL_HORIZ_Wh__m2]
            as MetaData).getter(hdr, pts[1]);
//        trace('avg cosZ @ 8 :', beta8);
//        trace('global solar :', gs8);
//        trace('should be    :', 157.0);
        Assert.assertTrue(Math.abs(gs8 - 157.0) < 10.0);
        var gs9:Number = (serHdrs[Solar.GLOBAL_HORIZ_Wh__m2]
            as MetaData).getter(hdr, pts[2]);
//        trace('avg cosZ @ 9 :', beta9);
//        trace('global solar :', gs9);
//        trace('should be    :', 341.0);
        Assert.assertTrue(Math.abs(gs9 - 341.0) < 10.0);
        var gs10:Number = (serHdrs[Solar.GLOBAL_HORIZ_Wh__m2]
            as MetaData).getter(hdr, pts[3]);
//        trace('avg cosZ @ 10 :', beta10);
//        trace('global solar :', gs10);
//        trace('should be    :', 486.0);
        Assert.assertTrue(Math.abs(gs10 - 486.0) < 10.0);
    }

    [Test]
    public function
    testAvgCosZHourCentered():void {
      // 11 PM
      var eleventhHour:DateTime = new DateTime(2013, 1, 1, 23);
      Assert.assertTrue(
        Solar.avgCosZHourCentered(eleventhHour, -7, 39.7, 105.0) <= 0.0);
    }

    [Test]
    public function
    testMakePointFromGlobalAndBeam():void
    {
      //trace('testMakePointFromGlobalAndBeam');
      var Igh_W__m2:Number = 2.0;
      var Ibn_W__m2:Number = 140.0;
      var Idh_W__m2:Number = 1.0;
      var eTime_hrs:Number = 7.0;
      var hdr:Object = MapUtils.makeObject([
        DateAndTime.REFERENCE_YEAR, 2012,
        DateAndTime.REFERENCE_MONTH, 1,
        DateAndTime.REFERENCE_DAY, 1,
        DateAndTime.REFERENCE_HOUR, 0,
        DateAndTime.REFERENCE_MINUTE, 0,
        DateAndTime.REFERENCE_SECOND, 0,
        DateAndTime.TIME_ZONE, -7,
        Location.LATITUDE_deg, 32.13,
        Location.LONGITUDE_deg, 110.95]);
      var solarVals:Object = Solar.makePointFromGlobalAndBeam(
        Igh_W__m2, Ibn_W__m2, eTime_hrs, 2012, hdr);
      if (false)
      {
        MapUtils.traceObject(solarVals, 'solarVals');
      }
      var rez:Object = MapUtils.mergeAll([
        MapUtils.makeObject([
          DateAndTime.ELAPSED_TIME_hrs, eTime_hrs]),
          solarVals]);
      var _get:Function = DataPoint.makeGetter(Solar.SERIES_META_DATA,
        rez, hdr);
      var Igh_W__m2_out:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
      if (false)
      {
        trace('avgCosZ:', _get(Solar.HOURLY_AVERAGED_COSZ));
        trace('solarGlobalHoriz_W__m2', _get(Solar.GLOBAL_HORIZ_Wh__m2));
        trace('should be :', Igh_W__m2);
        MapUtils.traceObject(rez, 'rez');
      }
      Assert.assertTrue(Math.abs(Igh_W__m2_out - Igh_W__m2) < 5.0);
    }

    [Test]
    public function testMakePointFromGlobalAndBeam2():void
    {
      //trace('testMakePointFromGlobalAndBeam2');
      var Igh_W__m2:Number = 157.0;
      var Ibn_W__m2:Number = 671.0;
      var Idh_W__m2:Number = 28.0;
      var eTime_hrs:Number = 8.0;
      var hdr:Object = MapUtils.makeObject([
        DateAndTime.REFERENCE_YEAR, 2012,
        DateAndTime.REFERENCE_MONTH, 1,
        DateAndTime.REFERENCE_DAY, 1,
        DateAndTime.REFERENCE_HOUR, 0,
        DateAndTime.REFERENCE_MINUTE, 0,
        DateAndTime.REFERENCE_SECOND, 0,
        DateAndTime.TIME_ZONE, -7,
        Location.LATITUDE_deg, 32.13,
        Location.LONGITUDE_deg, 110.95]);
      var rez:Object = MapUtils.mergeAll([
        MapUtils.makeObject([
          DateAndTime.ELAPSED_TIME_hrs, eTime_hrs]),
        Solar.makePointFromGlobalAndBeam(
          Igh_W__m2, Ibn_W__m2, eTime_hrs, 2012, hdr)]);
      var _get:Function = DataPoint.makeGetter(Solar.SERIES_META_DATA,
        rez, hdr);
      var Igh_W__m2_out:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
      if (false)
      {
        trace('avgCosZ:', _get(Solar.HOURLY_AVERAGED_COSZ));
        trace('solarGlobalHoriz_W__m2', _get(Solar.GLOBAL_HORIZ_Wh__m2));
        trace('should be :', Igh_W__m2);
        MapUtils.traceObject(rez, 'rez');
      }
      Assert.assertTrue(Math.abs(Igh_W__m2_out - Igh_W__m2) < 5.0);
    }

    public static function makeSolarDataSet():dse.DataSet {
        var hdr:Object = new Object();
        hdr[DateAndTime.REFERENCE_YEAR] = 2012;
        hdr[DateAndTime.REFERENCE_MONTH] = 1;
        hdr[DateAndTime.REFERENCE_DAY] = 1;
        hdr[DateAndTime.REFERENCE_HOUR] = 0;
        hdr[DateAndTime.REFERENCE_MINUTE] = 0;
        hdr[DateAndTime.REFERENCE_SECOND] = 0;
        hdr[Location.LATITUDE_deg] = 39.7392;
        hdr[Location.LONGITUDE_deg] = 104.9842;
        var points_:Array = new Array();
        for (var i:int=0; i<365; i++) {
            for (var j:int=0; j<24; j++) {
                points_.push(MapUtils.makeObject([
                    DateAndTime.ELAPSED_TIME_hrs, 24 * i + j,
                    Solar.DIFFUSE_HORIZONTAL_Wh__m2, 1,
                    Solar.BEAM_NORMAL_Wh__m2, 0
                ]));
            }
        }
        var points:IPVec = PersistentVector.create(points_);
        var smd:Object = MapUtils.merge(
            DateAndTime.SERIES_META_DATA, Solar.SERIES_META_DATA);
        var hmd:Object = {};
        return (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(points)
          .withSeriesMetaData(smd)
          .withHeaderMetaData(hmd)
          .build();
    }

    [Test]
    public function testDeriveMonthlyAvgGH():void {
        var dataSet:dse.DataSet = makeSolarDataSet();
        var items:ArrayCollection = Solar.deriveMonthlyAvgGH(dataSet);
        var expected_Wh__m2_day:Number = 24.0;
        var Wh__m2_to_MJ__m2:Number = 0.0036;
        var expected_MJ__m2_day:Number = 24.0 * Wh__m2_to_MJ__m2;
        for each (var item:Object in items) {
            Assert.assertTrue(
                Math.abs(expected_MJ__m2_day - item.Hgh_MJ__m2) < 1e-3);
        }
    }

    [Test]
    public function testSetSolarToZero():void {
      var hdr:Object = MapUtils.mergeAll([
        plugins.Location.EXAMPLES['Greenwich, London UK'],
        DateAndTime.makeHeaderFromDate(0, 2012, 5, 29, 12, 0, 0)]);
      var dp:Object = Solar.makeDataPoint(300.0, 100.0);
      dp[DateAndTime.ELAPSED_TIME_hrs] = 0.0;
      var settings:Object = MapUtils.makeObject([
        DateAndTime.ELAPSED_TIME_hrs, 0.0,
        Solar.GLOBAL_HORIZ_Wh__m2, 0.0,
        Solar.DIFFUSE_HORIZONTAL_Wh__m2, 300.0]);
      var newDP:Object = DataPoint.update(hdr, dp, Solar.CONSTRAINT_FUNC,
        Solar.DERIVATION_FUNCS, settings, Solar.MINS, Solar.MAXS,
        Solar.STATE_NAMES);
      var _get:Function = DataPoint.makeGetter(
        Solar.SERIES_META_DATA, newDP, hdr);
      var Igh:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
      var Ibn:Number = _get(Solar.BEAM_NORMAL_Wh__m2);
      var Idh:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2);
      //trace('------- testSetSolarToZero --------');
      //MapUtils.traceObject(newDP, 'newDP');
      //for each (var sm:MetaData in Solar.SERIES_META_DATA) {
      //  trace(sm.name, sm.getter(hdr, newDP));
      //}
      //trace('Igh', Igh);
      //trace('Ibn', Ibn);
      //trace('Idh', Idh);
      //trace('-----------------------------');
      Assert.assertTrue(Math.abs(Igh - 300.0) < 2);
      Assert.assertTrue(Math.abs(Idh - 300.0) < 2);
      Assert.assertTrue(Math.abs(Ibn - 0.0) < 2);
    }

    [Test]
    public function testSetSolarToZero2():void {
      // HERE
      var hdr:Object = MapUtils.mergeAll([
        plugins.Location.EXAMPLES['Greenwich, London UK'],
        DateAndTime.makeHeaderFromDate(0, 2012, 5, 29, 12, 0, 0)]);
      var dp:Object = Solar.makeDataPoint(300.0, 100.0);
      dp[DateAndTime.ELAPSED_TIME_hrs] = 0.0;
      var settings:Object = MapUtils.makeObject([
        DateAndTime.ELAPSED_TIME_hrs, 0.0,
        Solar.GLOBAL_HORIZ_Wh__m2, 0.0,
        Solar.BEAM_NORMAL_Wh__m2, 100.0]);
      var cosZMD:MetaData =
        Solar.SERIES_META_DATA[Solar.HOURLY_AVERAGED_COSZ] as MetaData;
      var cosz:Number = cosZMD.getter(hdr, dp);
      var newDP:Object = DataPoint.update(hdr, dp, Solar.CONSTRAINT_FUNC,
        Solar.DERIVATION_FUNCS, settings, Solar.MINS, Solar.MAXS,
        Solar.STATE_NAMES);
      var _get:Function = DataPoint.makeGetter(
        Solar.SERIES_META_DATA, newDP, hdr);
      var Igh:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
      var Ibn:Number = _get(Solar.BEAM_NORMAL_Wh__m2);
      var Idh:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2);
      //trace('------- testSetSolarToZero2 --------');
      //MapUtils.traceObject(newDP, 'newDP');
      //for each (var sm:MetaData in Solar.SERIES_META_DATA) {
      //  trace(sm.name, sm.getter(hdr, newDP));
      //}
      //trace('Igh', Igh);
      //trace('Ibn', Ibn);
      //trace('Idh', Idh);
      //trace('-----------------------------');
      Assert.assertTrue(Math.abs(Igh - (cosz * 100.0)) < 2);
      Assert.assertTrue(Math.abs(Idh - 0.0) < 2);
      Assert.assertTrue(Math.abs(Ibn - 100.0) < 2);
    }
    [Test]
    public function testScaleSolarByMonthlyAvgGH():void {
        var ds:dse.DataSet = makeSolarDataSet();
        var points:Array = ds.pointsAsArray;
        var hdr:Object = ds.header;
        var metaDataMap:Object = ds.seriesMetaData;
        var year:int = hdr[DateAndTime.REFERENCE_YEAR];
        var month:int = 1;
        var desiredIgh_W__m2:Number = 2.0;
        var desiredHgh_Wh__m2:Number = desiredIgh_W__m2 * 24;
        var Wh__m2_to_MJ__m2:Number = 0.0036;
        var desiredHgh_MJ__m2:Number = desiredHgh_Wh__m2 * Wh__m2_to_MJ__m2;
        var newPoints:Array = Solar.scaleSolarByMonthlyAvgGH(
            points, hdr, metaDataMap, year, month, desiredHgh_MJ__m2);
        var Igh_md:MetaData = ds.metaDataFor(Solar.GLOBAL_HORIZ_Wh__m2);
        var pt0:Object = newPoints[0];
        Assert.assertEquals(8760, newPoints.length);
        //MapUtils.traceObject(pt0, 'pt[0]');
        Assert.assertTrue(
            Math.abs(desiredIgh_W__m2 - Igh_md.getter(hdr, pt0)) < 1e-4);
    }
    // Expected values can be obtained from
    // NOAA: http://www.esrl.noaa.gov/gmd/grad/solcalc/
    public static function testSolar(
      lat_deg:Number,
      long_deg:Number,
      tz:Number,
      city_name:String,
      month:int,
      dayOfMonth:int,
      hourOfDay:int,
      expected_decl_deg:Number,
      expected_z1_deg:Number,
      expected_sha1_deg:Number,
      expected_z2_deg:Number,
      expected_sha2_deg:Number,
      tol:Number=0.1):void
    {
      var obj:Object = new Object();
      obj[DateAndTime.TIME_ZONE] = tz;
      obj[Location.CITY] = city_name;
      obj[Location.ELEVATION_m] = 540;
      obj[Location.LATITUDE_deg] = lat_deg;
      obj[Location.LONGITUDE_deg] = long_deg;
      obj[DateAndTime.REFERENCE_YEAR] = 2005;
      obj[DateAndTime.REFERENCE_MONTH] = month;
      obj[DateAndTime.REFERENCE_DAY] = dayOfMonth;
      obj[DateAndTime.REFERENCE_HOUR] = hourOfDay;
      obj[DateAndTime.REFERENCE_MINUTE] = 0;
      obj[DateAndTime.ELAPSED_TIME_hrs] = 0;
      var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
        obj, obj);
      var dt2:DateTime = DateTime.hourOfYearToDateTime(
        DateAndTime.hourOfYearFromDateObject(dt) + 1,
        obj[DateAndTime.REFERENCE_YEAR]);
      var lat_deg:Number = obj[Location.LATITUDE_deg];
      var long360W_deg:Number = Solar.longToLong360W(obj[Location.LONGITUDE_deg]);
      var TZ_hrs:Number = obj[DateAndTime.TIME_ZONE];
      var result:Number = Solar.deriveHourlyAveragedCosZ(obj);
      var decl_deg:Number = Solar.declination_deg(dt, TZ_hrs);
      var decl2_deg:Number = Solar.declination_deg(dt2, TZ_hrs);
      var UT:Number = Solar.universalTime_hrs(dt, TZ_hrs);
      var t:Number = Solar._t(dt, TZ_hrs);
      var G:Number = Solar._G_deg(t);
      var C:Number = Solar._C(G);
      var L:Number = Solar._L_deg(t, C);
      var alpha:Number = Solar._alpha_deg(L);
      var gha:Number = Solar._greenwichHourAngle_deg(UT, C, L, alpha);
      var _sha1:Number =  Solar._solarHourAngle_deg(gha, long360W_deg);
      var sha1:Number = Solar.solarHourAngle_deg(
        dt,
        obj[DateAndTime.TIME_ZONE],
        long360W_deg);
      var sha2:Number = Solar.solarHourAngle_deg(
        dt2,
        TZ_hrs,
        long360W_deg);
      var z:Number = Solar.zenithAngle_deg(lat_deg, decl_deg, sha1);
      var z2:Number = Solar.zenithAngle_deg(lat_deg, decl2_deg, sha2);
      var acz:Number = Solar.avg_cosz(sha1, sha2, dt, lat_deg, decl_deg);
      var expected_acz:Number = Math.cos(((z + z2) / 2.0) * Math.PI / 180.0);
      Assert.assertTrue(Math.abs(sha1 - expected_sha1_deg) < tol);
      Assert.assertTrue(Math.abs(decl_deg - expected_decl_deg) < tol);
      Assert.assertTrue(Math.abs(expected_z1_deg - z) < tol);
      Assert.assertTrue(Math.abs(sha2 - expected_sha2_deg) < tol);
      //trace("City", city_name);
      //trace("z2: ", z2);
      //trace("expected z2:", expected_z2_deg);
      Assert.assertTrue(Math.abs(expected_z2_deg - z2) < 0.08);
      //trace("acz: ", acz);
      //trace("expected acz:", expected_acz);
      Assert.assertTrue(Math.abs(acz - expected_acz) < 0.05);
    }
    [Test]
    public function testDeriveHourlyAveragedCosZ_Bern():void
    {
      testSolar(
        46.95,
        7.44,
        1,
        "Bern",
        1,
        1,
        12,
        -22.97,
        90.0 - 19.7,
        -8.475,
        90.0 - 19.82,
        6.525);
    }
    [Test]
    public function testDeriveHourlyAveragedCosZ_NewMexico():void
    {
      testSolar(
        34.0,
        -104.0,
        -6,
        "Somewhere, New Mexico",
        6,
        1,
        12,
        22.14,
        90.0 - 73.26,
        -13.4875,
        90.0 - 78.11,
        1.5125);
    }
    [Test]
    public function testDeriveHourlyAveragedCosZ_Auckland():void
    {
      testSolar(
        -36.847,
        174.769999,
        12,
        "Auckland",
        6,
        15,
        12,
        23.31,
        90.0 - 29.66,
        -5.35,
        90.0 - 29.18,
        9.65);
    }
}
}