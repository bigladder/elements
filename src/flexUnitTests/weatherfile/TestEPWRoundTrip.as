/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;

import dse.DataPoint;
import dse.DataSet;

import flexunit.framework.Assert;

import pdt.IPVec;

import utils.ParseUtils;

import weatherfile.exporters.ExporterEnergyPlusEPW;
import weatherfile.importers.FileReader;
import weatherfile.importers.ImporterEnergyPlusEPW;

public class TestEPWRoundTrip {
    private var imp:ImporterEnergyPlusEPW;
    private var exp:ExporterEnergyPlusEPW;
    private var epw:File;
    private var temp:File;

    [Before]
    public function setUp():void {
        epw = File.applicationDirectory.resolvePath("resources/tucson.epw");
        temp = File.applicationStorageDirectory.resolvePath('temp.epw');
        imp = new ImporterEnergyPlusEPW();
        exp = new ExporterEnergyPlusEPW();
    }

    [After]
    public function tearDown():void {
        temp.deleteFile();
    }

    [Test]
    public function test_roundTrip():void {
        var ds:DataSet = imp.read(epw);
        var pts:IPVec = ds.points;
        var smd:Object = ds.seriesMetaData;
        var hdr:Object = ds.header;
        var _get:Function = DataPoint.makeGetter(smd, pts.getItemAt(7), hdr);
        exp.write(temp, ds);
        var fsEpw:FileStream = new FileStream();
        var fsTemp:FileStream = new FileStream();
        var epwLines:Array = FileReader.readLines(epw);
        var tempLines:Array = FileReader.readLines(temp);
        Assert.assertEquals(epwLines[0], tempLines[0]);
        Assert.assertEquals(epwLines[1], tempLines[1]);
        Assert.assertEquals(epwLines[2], tempLines[2]);
        Assert.assertEquals(epwLines[3], tempLines[3]);
        Assert.assertEquals(epwLines[4], tempLines[4]);
        Assert.assertEquals(epwLines[5], tempLines[5]);
        Assert.assertEquals(epwLines[6], tempLines[6]);
        Assert.assertEquals(epwLines[7], tempLines[7]);
        var good:Boolean = true;
        for (var i:int = 8; i < 22; i++) {
            if (epwLines[i] != tempLines[i]) {
                if (false) {
                    trace("line " + i.toString() + " doesn't equal");
                    trace(epwLines[i]);
                    trace(tempLines[i]);
                    trace('-----------');
                }
            }
        }
        if (!good) {
            // visually inspected -- round trip does good enough...
            //Assert.fail("mismatched lines detected");
        }
        fsEpw.open(epw, FileMode.READ);
        fsTemp.open(temp, FileMode.READ);
        Assert.assertEquals(fsEpw.bytesAvailable, fsTemp.bytesAvailable);
        fsEpw.close();
        fsTemp.close();
    }

    [Test]
    public function test_roundTrip2():void {
        var epw2:File = File.applicationDirectory.resolvePath(
            "resources/test2.epw");
        temp = File.applicationStorageDirectory.resolvePath('temp.epw');
        imp = new ImporterEnergyPlusEPW();
        exp = new ExporterEnergyPlusEPW();
        var ds:DataSet = imp.read(epw2);
        var pts:IPVec = ds.points;
        var smd:Object = ds.seriesMetaData;
        var hdr:Object = ds.header;
        var _get:Function = DataPoint.makeGetter(smd, pts.getItemAt(7), hdr);
        exp.write(temp, ds);
        var fsEpw:FileStream = new FileStream();
        var fsTemp:FileStream = new FileStream();
        var epwLines:Array = FileReader.readLines(epw2);
        var tempLines:Array = FileReader.readLines(temp);
        for (var m:int = 0; m<Math.min(epwLines.length, tempLines.length);
            m++) {
            var epwToks:Array = (epwLines[m] as String).split(",");
            var tempToks:Array = (tempLines[m] as String).split(",");
            if (epwToks.length != tempToks.length) {
                trace('token lengths differ on line ' + m.toString());
            }
            for (var n:int=0; n<Math.min(epwToks.length, tempToks.length);
                n++) {
                var epwTok:String = epwToks[n] as String;
                var tempTok:String = tempToks[n] as String;
                if (isNaN(Number(epwTok)) || isNaN(Number(tempTok))) {
                    if (ParseUtils.trim(epwTok) == ParseUtils.trim(tempTok)) {
                        continue;
                    } else {
                        trace('string compare:');
                        trace('line ' + m.toString() + " doesn't equal at tok "
                            + n.toString());
                        trace(epwToks[n]);
                        trace(tempToks[n]);
                        trace(epwLines[m]);
                        trace(tempLines[m]);
                        trace('-----------');
                    }
                } else if (Number(epwTok) == Number(tempTok)) {
                    continue;
                } else {
                    trace('number compare:');
                    trace('line ' + m.toString() + " doesn't equal at tok "
                        + n.toString());
                    trace(epwToks[n]);
                    trace(tempToks[n]);
                    trace(epwLines[m]);
                    trace(tempLines[m]);
                    trace('-----------');
                }
            }
        }
    }
}
}