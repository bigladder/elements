/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
import flash.filesystem.File;

import dse.DataSet;

import flexunit.framework.Assert;

import pdt.IPVec;

import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterNativeFormat;

public class TestImportNative {
    public var f:File;
    [Before]
    public function setUp():void {
        f = File.applicationDirectory.resolvePath("resources/example.elements");
    }

    [After]
    public function tearDown():void {
    }

    [Test]
    public function testZipFileExists():void {
        var f:File = File.applicationDirectory.resolvePath(
            "resources/example.elements");
        if (false) {
            trace(f.parent.nativePath);
            trace(f.nativePath);
        }
        Assert.assertTrue(f.exists);
    }

    [Test]
    public function testImportFile():void {
        var imp:IImporter = new ImporterNativeFormat();
        var ds:DataSet = imp.read(f);
        Assert.assertTrue(ds != null);
        var pts:IPVec = ds.points;
        Assert.assertEquals(3, pts.length);
        var pt0:Object = ds.pointN(0);
        Assert.assertTrue(pt0.hasOwnProperty('elapsedTime_hrs'));
        Assert.assertTrue(pt0.hasOwnProperty('dryBulbTemp_C'));
        Assert.assertTrue(pt0.hasOwnProperty('pressure_kPa'));
        Assert.assertTrue(pt0.hasOwnProperty('wetBulbTemp_C'));
        Assert.assertEquals(0, pt0.elapsedTime_hrs);
        Assert.assertEquals(25, pt0.dryBulbTemp_C);
        Assert.assertEquals(101.325, pt0.pressure_kPa);
        Assert.assertEquals(45, pt0.dryBulbTemp_C + pt0.wetBulbTemp_C);
        var hdr:Object = ds.header;
        Assert.assertTrue(hdr.hasOwnProperty('location'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceYear'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceMonth'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceDay'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceHour'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceMinute'));
        Assert.assertTrue(hdr.hasOwnProperty('referenceSecond'));
        Assert.assertEquals(2013, hdr.referenceYear + hdr.referenceMonth);
        Assert.assertEquals("Denver, CO", hdr.location);
    }
}
}