/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.units {
import flexunit.framework.Assert;

import pdt.IPMap;

import plugins.Psych;

import units.TemperatureUnits;
import units.UnitInfo;
import units.UnitSystems;

import utils.MapUtils;

public class TestUnitInfo {
    [Before]
    public function setUp():void {
    }

    [Test]
    public function
    testSIUnits():void {
        var siUnits:Object = UnitInfo.getUnitSystem(UnitSystems.SI);
        MapUtils.traceObject(siUnits, 'siUnits');
    }

    [Test]
    public function
    testIPUnits():void {
        var ipUnits:Object = UnitInfo.getUnitSystem(UnitSystems.IP);
        MapUtils.traceObject(ipUnits, 'ipUnits');
    }

    [Test]
    public function testMakeDisplayUnitsForUnitSystem():void {
        var du:IPMap = UnitInfo.makeDisplayUnitsForUnitSystem(
            UnitSystems.SI, Psych.SERIES_META_DATA);
        Assert.assertTrue(du.containsKey(Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(
            TemperatureUnits.C, du.valAt(Psych.DRY_BULB_TEMP_C));
    }
}
}