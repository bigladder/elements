/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.units {
import flexunit.framework.Assert;

import units.IrradianceUnits;
import units.UnitConversions;

public class TestUnitConversions {
    [Before]
    public function setUp():void {
    }

    [Test]
    public function testBaseToDisplay():void {
        var baseVal:Number = 10.0;
        var displayScale:Number = 100;
        var baseUnits:String = IrradianceUnits.W__M2;
        var displayUnits:String = IrradianceUnits.W__M2;
        var actual:Number = UnitConversions.baseToDisplay(
            baseVal, baseUnits, displayScale, displayUnits);
        var expected:Number = 1000.0;
        Assert.assertTrue(Math.abs(expected - actual) < 1e-6);
    }

    [Test]
    public function testDisplayToBase():void {
        var displayVal:Number = 1000.0;
        var displayScale:Number = 100;
        var baseUnits:String = IrradianceUnits.W__M2;
        var displayUnits:String = IrradianceUnits.W__M2;
        var actual:Number = UnitConversions.displayToBase(
            displayVal, displayUnits, displayScale, baseUnits);
        var expected:Number = 10.0;
        Assert.assertTrue(Math.abs(expected - actual) < 1e-6);
    }
}
}