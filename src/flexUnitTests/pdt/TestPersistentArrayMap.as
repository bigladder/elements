/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flexunit.framework.Assert;

import pdt.IMapEntry;
import pdt.IPMap;
import pdt.IPVec;
import pdt.ISeq;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;
import pdt.SeqFuncs;

public class TestPersistentArrayMap {
    private var m:PersistentArrayMap;

    [Before]
    public function setUp():void {
        m = new PersistentArrayMap([
            "a", 10,
            "b", new PersistentArrayMap([
                "c", 20,
                "d", PersistentVector.create([1, 2])]),
            "e", 30]);
    }

    [Test]
    public function testValAt():void {
        var n:PersistentArrayMap = new PersistentArrayMap(["a", 10]);
        Assert.assertTrue(10, n.valAt("a"));
    }

    [Test]
    public function testValAt2():void {
        Assert.assertTrue(1,
            ((m.valAt("b") as PersistentArrayMap).valAt("d")
                as IPVec).valAt(0));
    }

    [Test]
    public function testCount():void {
        Assert.assertTrue(3, m.count);
    }

    [Test]
    public function testCreate():void {
        var n:IPMap = PersistentArrayMap.createFromObj({a:1,b:4});
        Assert.assertTrue(2, n.count);
        Assert.assertEquals(4, n.valAt('b'));
    }

    [Test]
    public function testAssoc():void {
        var n:IPMap = m.assoc('b', 20);
        Assert.assertEquals(20, n.valAt('b'));
        Assert.assertEquals(3, m.count);
        Assert.assertEquals(3, n.count);
        Assert.assertEquals(20, (m.valAt('b') as IPMap).valAt('c'));
    }

    [Test]
    public function testAssocEx():void {
        var n:IPMap = m.assocEx('z', 100);
        Assert.assertEquals(100, n.valAt('z'));
    }

    [Test]
    public function testAssocEx2():void {
        try {
            var n:IPMap = m.assocEx('a', 10);
            Assert.fail('failed to throw error message');
        } catch (e:Error) {
            Assert.assertEquals(PersistentArrayMap.ERROR_KEY_ALREADY_PRESENT,
                e.message);
            // do nothing -- test passed
        }
    }

    [Test]
    public function testWithout():void {
        var n:IPMap = m.without('b');
        Assert.assertEquals(3, m.count);
        Assert.assertEquals(2, n.count);
        Assert.assertEquals(null, n.valAt('b'));
    }

    [Test]
    public function testEntryAt():void {
        var me:IMapEntry = m.entryAt('a');
        Assert.assertEquals(10, me.val);
        Assert.assertEquals('a', me.key);
    }

    [Test]
    public function testContainsKey():void {
        Assert.assertTrue(m.containsKey('a'));
        Assert.assertTrue(m.containsKey('b'));
        Assert.assertTrue(m.containsKey('e'));
    }

    [Test]
    public function testCreateFromObj():void {
        var n:PersistentArrayMap = PersistentArrayMap.createFromObj({
            a:100, b:200, z:"Hello"});
        Assert.assertEquals(100, n.valAt('a'));
        Assert.assertEquals(200, n.valAt('b'));
        Assert.assertEquals("Hello", n.valAt('z'));
    }

    private function getObj():Object {
        return {a:"hi", b:10};
    }

    [Test]
    public function testKeys():void {
        var exp:Array = ['a', 'b'];
        var act:Array = PersistentArrayMap.createFromObj(getObj()).keys;
        Assert.assertEquals(exp.length, act.length);
        for (var i:int=0; i<exp.length; i++) {
            Assert.assertEquals(exp[i], act[i]);
        }
    }

    [Test]
    public function testVals():void {
        var exp:Array = ["hi", 10];
        var act:Array = PersistentArrayMap.createFromObj(getObj()).vals;
        Assert.assertEquals(exp.length, act.length);
        for (var i:int=0; i<exp.length; i++) {
            Assert.assertEquals(exp[i], act[i]);
        }
    }

    [Test]
    public function testToObject():void {
        var obj:Object = m.toObject();
    }

    [Test]
    public function testToString():void {
        var exp:String = "PersistentArrayMap.createFromObj({a: 10})";
        var act:String = PersistentArrayMap.createFromObj({a: 10}).toString();
        Assert.assertEquals(exp, act);
    }

    [Test]
    public function testGetIn():void {
        var val:Number = m.getIn(['b', 'c']) as Number;
        Assert.assertEquals(20, val);
    }

    [Test]
    public function testGetIn2():void {
        var val:Number = m.getIn(['b', 'd', 1]) as Number;
        Assert.assertEquals(2, val);
    }

    [Test]
    public function testAssocIn():void {
        var n:IPMap = m.assocIn(['b', 'd'], 40);
        Assert.assertEquals(10, n.valAt('a'));
        Assert.assertEquals(20, n.getIn(['b', 'c']));
        Assert.assertEquals(40, n.getIn(['b', 'd']));
        Assert.assertEquals(30, n.valAt('e'));
    }

    [Test]
    public function testAssocIn2():void {
        var n:IPMap = m.assocIn(['b'], 40);
        Assert.assertEquals(10, n.valAt('a'));
        Assert.assertEquals(40, n.valAt('b'));
        Assert.assertEquals(30, n.valAt('e'));
    }

    [Test]
    public function testAssocIn3():void {
        var n:IPMap = m.assocIn([], 40);
        Assert.assertEquals(m, n);
    }

    [Test]
    public function testAssocIn4():void {
        var n:IPMap = m.assocIn(['b', 'd', 0], "Hello");
        Assert.assertEquals(10, n.valAt('a'));
        Assert.assertEquals(20, n.getIn(['b', 'c']));
        Assert.assertEquals("Hello", n.getIn(['b', 'd', 0]));
        Assert.assertEquals(2, n.getIn(['b', 'd', 1]));
        Assert.assertEquals(30, n.valAt('e'));
    }

    [Test]
    public function testAssoc_():void {
        var n:IPMap = m.assoc_('f', 100) as IPMap;
        Assert.assertEquals(10, n.valAt('a'));
        Assert.assertEquals(20, n.getIn(['b', 'c']));
        Assert.assertEquals(1, n.getIn(['b', 'd', 0]));
        Assert.assertEquals(2, n.getIn(['b', 'd', 1]));
        Assert.assertEquals(30, n.valAt('e'));
        Assert.assertEquals(100, n.valAt('f'));
    }

    [Test]
    public function testEmpty():void {
        var n:IPMap = m.empty() as IPMap;
        Assert.assertEquals(n, PersistentArrayMap.EMPTY);
    }

    [Test]
    public function testSeq():void {
        var s:ISeq = m.seq();
        Assert.assertEquals(3, SeqFuncs.length(s));
    }

    [Test]
    public function testGetKeys():void {
        var ks:Array = m.keys;
        Assert.assertEquals(3, ks.length);
        Assert.assertEquals('a', ks[0]);
        Assert.assertEquals('b', ks[1]);
        Assert.assertEquals('e', ks[2]);
    }
}
}