/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flash.events.Event;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import pdt.EmptyList;
import pdt.IPersistentCollection;
import pdt.IPersistentStack;
import pdt.ISeq;
import pdt.PersistentList;

public class TestEmptyList {
    private var el:EmptyList;

    [Before]
    public function setUp():void {
        el = new EmptyList();
    }

    [Test]
    public function testAddEventListener():void {
        el.addEventListener("", function():void {});
    }

    [Test]
    public function testAddItem():void {
        MyAssert.assertThrows(function():void {
            el.addItem(1);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testAddItemAt():void {
        MyAssert.assertThrows(function():void {
            el.addItemAt("junk", 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testCons():void {
        var lst:ISeq = el.cons(1);
        Assert.assertEquals(1, lst.first);
    }

    [Test]
    public function testGet_count():void {
        Assert.assertEquals(0, el.count);
    }

    [Test]
    public function testDispatchEvent():void {
        var rez:Boolean = el.dispatchEvent(new Event("stuff"));
        Assert.assertTrue(rez == false);
    }

    [Test]
    public function testEmpty():void {
        var e:IPersistentCollection = el.empty();
        Assert.assertEquals(el, e);
    }

    [Test]
    public function testEmptyList():void {
        var newEmptyList:EmptyList = new EmptyList();
    }

    [Test]
    public function testEquiv():void {
        var el1:EmptyList = new EmptyList();
        var el2:EmptyList = new EmptyList();
        Assert.assertTrue(el1.equiv(el2));
    }

    [Test]
    public function testGet_first():void {
        Assert.assertTrue(el.first == null);
    }

    [Test]
    public function testGetItemAt():void {
        Assert.assertTrue(el.getItemAt(0) == null);
    }

    [Test]
    public function testGetItemIndex():void {
        Assert.assertEquals(-1, el.getItemIndex(0));
    }

    [Test]
    public function testHasEventListener():void {
        Assert.assertTrue(el.hasEventListener(Event.CHANGE) == false);
    }

    [Test]
    public function testItemUpdated():void {
        el.itemUpdated({});
    }

    [Test]
    public function testGet_length():void {
        Assert.assertEquals(0, el.length);
    }

    [Test]
    public function testNext():void {
        var next:ISeq = el.next();
        Assert.assertEquals(null, next);
    }

    [Test]
    public function testPeek():void {
        var next:* = el.peek();
        Assert.assertEquals(null, next);
    }

    [Test]
    public function testPop():void {
        var st:IPersistentStack = el.pop();
        Assert.assertEquals(null, st);
    }

    [Test]
    public function testRemoveAll():void {
        MyAssert.assertThrows(function():void {
            el.removeAll();
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testRemoveEventListener():void {
        el.removeEventListener(Event.CHANGE, function (e:Event):void {});
    }

    [Test]
    public function testRemoveItemAt():void {
        MyAssert.assertThrows(function():void {
            el.removeItemAt(0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testGet_rest():void {
        Assert.assertEquals(null, el.rest);
    }

    [Test]
    public function testSeq():void {
        var seq:ISeq = el.seq();
        Assert.assertEquals(null, seq);
    }

    [Test]
    public function testSetItemAt():void {
        MyAssert.assertThrows(function():void {
            el.setItemAt('bob', 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testToArray():void {
        Assert.assertEquals(0, el.toArray().length);
    }

    [Test]
    public function testWillTrigger():void {
        Assert.assertEquals(false, el.willTrigger(Event.CHANGE));
    }
}
}