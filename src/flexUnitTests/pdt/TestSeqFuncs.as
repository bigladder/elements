/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flexunit.framework.Assert;

import pdt.ISeq;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;
import pdt.SeqFuncs;

public class TestSeqFuncs {
    public var xs:ISeq;
    public var ys:ISeq;

    [Before]
    public function setUp():void {
        xs = PersistentVector.create([1,2,3]).seq();
        ys = PersistentArrayMap.createFromObj({a: 1, b: 2, c: 3}).seq();
    }

    [Test]
    public function testLength():void {
        Assert.assertEquals(3, SeqFuncs.length(ys));
        Assert.assertEquals(3, SeqFuncs.length(xs));
    }
}
}