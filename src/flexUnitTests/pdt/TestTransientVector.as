/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flexunit.framework.Assert;

import pdt.IPersistentCollection;
import pdt.ITransientVector;
import pdt.PersistentVector;
import pdt.TransientVector;

public class TestTransientVector {
    public var tv:TransientVector;
    public var tv0:TransientVector;
    public var tv1:TransientVector;
    public var tv2:TransientVector;

    [Before]
    public function setUp():void {
        tv = new TransientVector(
            0, 0, PersistentVector.EMPTY_NODE, new Array(32));
        var empty:TransientVector = new TransientVector(
            0, 0, PersistentVector.EMPTY_NODE, new Array(32));
        tv0 = empty.conj(1) as TransientVector;
        tv1 = empty.conj(2) as TransientVector;
        tv2 = empty.conj(3) as TransientVector;
    }

    [Test]
    public function testAssoc():void {
        var tv3:TransientVector = tv2.assoc(3, 40) as TransientVector;
        Assert.assertEquals(4, tv3.count);
        Assert.assertEquals(40, tv0.valAt(3));
    }

    [Test]
    public function testAssocN():void {
        var tv3:TransientVector = tv2.assocN(3, 40) as TransientVector;
        Assert.assertEquals(40, tv0.nth(3));
    }

    [Test]
    public function testGet_count():void {
        Assert.assertEquals(3, tv2.count);
    }

    [Test]
    public function testNth():void {
        Assert.assertEquals(3, tv1.nth(2));
    }

    [Test]
    public function testPop():void {
        var xs:ITransientVector = tv2.pop();
        Assert.assertEquals(2, xs.count);
        Assert.assertEquals(2, tv0.count);
        Assert.assertEquals(2, xs.nth(1));
    }

    [Test]
    public function testTransientVector():void {
        Assert.assertEquals(0, tv.count);
    }

    [Test]
    public function testPersistent():void {
        var pv:IPersistentCollection = tv.persistent();
        Assert.assertEquals(0, pv.count);
    }

    [Test]
    public function testConj():void {
        var tv2:TransientVector = tv.conj(1) as TransientVector;
        var tv3:TransientVector = tv.conj(2) as TransientVector;
        Assert.assertEquals(2, tv.count);
        Assert.assertEquals(2, tv2.count);
        Assert.assertEquals(2, tv3.count);
        Assert.assertEquals(1, tv.nth(0));
    }
}
}