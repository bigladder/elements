/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import plugins.Psych;

import utils.MapUtils;
import utils.OptUtils;

public class TestOptUtils {
    private var fCon:Function;
    private var fOpt:Function;

    [Before]
    public function setUp():void {
        fCon = function(x:Number):Number {
            if (x > 0.6) {
                return Math.abs(x - 0.6);
            }
            return 0.0;
        };
    }

    [Test]
    public function
    testFindGreatestGoodBound():void {
        var result:Number = OptUtils.findGreatestGoodBound(fCon);
    }

    [Test]
    public function
    testFindGreatestGoodBound02():void {
        var values:Object = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, 25.0,
            Psych.WET_BULB_TEMP_C, 20.0,
            Psych.PRESSURE_kPa, 101.325]);
        var settings:Object = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, 25.0,
            Psych.WET_BULB_TEMP_C,  3.0,
            Psych.PRESSURE_kPa, 101.325]);
        var constraintFunc:Function = Psych.CONSTRAINT_FUNC;
        var globalHeader:Object = new Object();
        var settingNames:Array = MapUtils.keys(settings);
        var fCon0:Function = function(x:Number):Number {
            var keyVals:Array = new Array();
            for each (var settingName:String in settingNames) {
                var Y:Number = settings[settingName];
                var y:Number = values[settingName];
                keyVals.push(settingName);
                keyVals.push(x * (Y - y) + y);
            }
            var guessVals:Object =
                MapUtils.update(values, keyVals);
            return constraintFunc(globalHeader, guessVals);
        };
        //trace('fCon0(0.0)', fCon0(0.0));
        //trace('fCon0(1.0)', fCon0(1.0));
        var xMin:Number = OptUtils.findGreatestGoodBound(fCon0);
        //trace('xMin', xMin);
    }

    [Test]
    public function
    testBinarySearch():void {
        var f:Function = function(x:Number):Number {
            return Math.pow(x - 0.321, 2.0);
        };
        var x:Number = OptUtils.binarySearch(f);
        Assert.assertTrue(Math.abs(0.321 - x) < 1e-3);
    }

    [Test]
    public function
    testMakeErrFunc():void {
        var dp:Object = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, 25.0,
            Psych.WET_BULB_TEMP_C, 20.0,
            Psych.PRESSURE_kPa, 101.325]);
        var settings:Object = MapUtils.makeObject([
            Psych.DEW_POINT_TEMP_C, Psych.dewPointTemp_C(30.0, 22.0, 100.0),
            Psych.HUMIDITY_RATIO, Psych.humidityRatio(30.0, 22.0, 100.0),
            Psych.DENSITY_kg__m3, Psych.density_kg__m3(30.0, 22.0, 100.0)]);
        var dvs:Array = MapUtils.keys(dp.values);
        var sets:Array = MapUtils.keys(settings);
        var ef:Function = OptUtils.makeErrFunc(
            dp.values, settings, Psych.MINS, Psych.MAXS, new Object(),
            dp.constraintFunc, dp.derivationFuncs,
            dvs, sets);
        var nTdb:Number = OptUtils.normalize0to1(
            30.0, Psych.MIN_DRY_BULB_TEMP_C, Psych.MAX_DRY_BULB_TEMP_C);
        var nTwb:Number = OptUtils.normalize0to1(
            22.0, Psych.MIN_WET_BULB_TEMP_C, Psych.MAX_WET_BULB_TEMP_C);
        var nP:Number = OptUtils.normalize0to1(
            100.0, Psych.MIN_PRESSURE_kPa, Psych.MAX_PRESSURE_kPa);
        /*
        trace('dvs', dvs);
        trace('n(30.0)', nTdb);
        trace('n(22.0)', nTwb);
        trace('n(100.0)', nP);
        trace('ef() =>', ef([nTdb, nP, nTwb]));
        trace('denormalize(0.5, min[p], max[p]) =>',
            OptUtils.denormalize(
                0.5, Psych.MIN_PRESSURE_kPa, Psych.MAX_PRESSURE_kPa));
        */
    }
}
}