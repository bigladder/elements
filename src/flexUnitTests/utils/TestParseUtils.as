/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import utils.ParseUtils;

public class TestParseUtils {
    [Before]
    public function setUp():void {
    }

    [After]
    public function tearDown():void {
    }

    [Test]
    public function testParseToks():void {
        var xs:Array = ParseUtils.parseToks("1,2,  3,  \"John\", 4, 5", ",",
            /\d+/, function(x:String):Number { return Number(x); });
        Assert.assertEquals(5, xs.length);
        for (var i:int=0; i<xs.length; i++) {
            Assert.assertEquals(i+1, xs[i]);
        }
    }

    [Test]
    public function testTrim():void {
        var x:String;
        x = ParseUtils.trim('   happy');
        Assert.assertEquals('happy', x);
        x = ParseUtils.trim('happy         ');
        Assert.assertEquals('happy', x);
        x = ParseUtils.trim('   happy   ');
        Assert.assertEquals('happy', x);
        x = ParseUtils.trim('happy');
        Assert.assertEquals('happy', x);
    }

    [Test]
    public function testToStrOrNum():void {
        var x:Number = ParseUtils.toStrOrNum("  45  ");
        Assert.assertEquals(45, x);
        var y:String = ParseUtils.toStrOrNum('   "John"    ');
        Assert.assertEquals('John', y);
    }

    [Test]
    public function testSplit():void {
        var test:String = '"hello, how are you?","I am fine, thanks",' +
            '"That\'s good"';
        var xs:Array = ParseUtils.split(test);
        Assert.assertEquals(3, xs.length);
        Assert.assertEquals('hello, how are you?', xs[0]);
        var t2:String = 'this,is,"some, sample","csv, text, for",you,to,' +
            '"look",at,"on such a ""lovely day"" as this",ne';
        var ys:Array = ParseUtils.split(t2);
        Assert.assertEquals(10, ys.length);
        Assert.assertEquals('on such a "lovely day" as this', ys[8]);
    }

    [Test]
    public function testTryToNum():void {
        var x:Number = ParseUtils.tryToNum("3");
        Assert.assertEquals(3, x);
        var y:String = ParseUtils.tryToNum("John");
        Assert.assertEquals('John', y);
    }
    
    [Test]
    public function testBound():void
    {
      var n:Number = ParseUtils.bound(25, 0, 20);
      Assert.assertEquals(20, n);
      n = ParseUtils.bound(15, 0, 20);
      Assert.assertEquals(15, n);
      n = ParseUtils.bound(-5, 0, 20);
      Assert.assertEquals(0, n);
    }
    
    [Test]
    public function testMinBound():void
    {
      var n:Number = ParseUtils.minBound(24, 30);
      Assert.assertEquals(30, n);
      n = ParseUtils.minBound(24, 20);
      Assert.assertEquals(24, n);
    }
    
    [Test]
    public function testMaxBound():void
    {
      var n:Number = ParseUtils.maxBound(24, 30);
      Assert.assertEquals(24, n);
      n = ParseUtils.maxBound(24, 20);
      Assert.assertEquals(20, n);
    }
}
}