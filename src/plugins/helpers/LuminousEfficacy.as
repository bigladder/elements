/*
Copyright (C) 2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
Reference:
R. Perez, P. Ineichen, R. Seals, J. Michalsky and R. Stewart,
"Modeling daylight availability and irradiance components from direct
global irradiance components from direct and global irradiance,"
Solar Energy 44 (1990) 271-289.
Also:
https://github.com/NREL/EnergyPlus/blob/
4a864a34975a83eb9e638db6822c4881bfc50243/src/EnergyPlus/WeatherManager.cc#L7838
*/
package plugins.helpers
{
public class LuminousEfficacy
{
  // direct luminous efficacy in Lumins/Watt
  private var _direct:Number;
  // diffuse luminous efficacy in Lumins/Watt
  private var _diffuse:Number;

  private static const a_dif:Array = [97.24, 107.22, 104.97, 102.39, 100.71,
    106.42, 141.88, 152.23];
  private static const b_dif:Array = [-0.46, 1.15, 2.96, 5.59, 5.94, 3.83,
    1.90, 0.35];
  private static const c_dif:Array = [12.00, 0.59, -5.53, -13.95, -22.75,
    -36.15, -53.24, -45.27];
  private static const d_dif:Array = [-8.91, -3.95, -8.77, -13.90, -23.74,
    -28.83, -14.03, -7.98];
  private static const a_dir:Array = [57.20, 98.99, 109.83, 110.34, 106.36,
    107.19, 105.75, 101.18];
  private static const b_dir:Array = [-4.55, -3.46, -4.90, -5.84, -3.97,
    -1.25, 0.77, 1.58];
  private static const c_dir:Array = [-2.98, -1.21, -1.71, -1.99, -1.75, -1.51,
    -1.26, -1.10];
  private static const d_dir:Array = [117.12, 12.38, -8.81, -4.56, -6.16,
    -26.73, -34.44, -8.29];
  private static const x_dir_norm_ill:Array = [
    131153.0, 130613.0, 128992.0, 126816.0, 124731.0, 123240.0,
    122652.0, 123120.0, 124576.0, 126658.0, 128814.0, 130471.0];
  private static const deg_to_rad:Number = Math.PI / 180.0;

  public function LuminousEfficacy(direct:Number, diffuse:Number)
  {
    this._direct = direct;
    this._diffuse = diffuse;
  }

  public function get direct():Number
  {
    return this._direct;
  }

  public function get diffuse():Number
  {
    return this._diffuse;
  }

  // Num x Num x Num x Num x Num x Int -> LuminousEfficacy
  //- cos_zen_ang -- cosine of sun's zenith angle
  //- dif_sol_rad -- diffuse horizontal solar irradiance
  //- dir_sol_rad -- direct normal (i.e., beam) solar irradiance
  //- elev_m -- elevation (meters)
  //- Tdp_C -- dew-point temperature (Celsius)
  //- month -- integer, 1 = January, 12 = December
  //Returns: LuminousEfficacy object
  public static function
  calc(
    cos_zen_ang:Number,
    dif_sol_rad:Number,
    dir_sol_rad:Number,
    elev_m:Number,
    Tdp_C:Number,
    month:int):LuminousEfficacy
  {
    // Luminous Efficacy Coefficients for Diffuse (dif) and Beam/Direct (dir)
    var zen_ang:Number = Math.acos(cos_zen_ang);
    if (zen_ang >= (Math.PI / 2.0)) {
      return new LuminousEfficacy(0.0, 0.0);
    }
    var alt_ang:Number = (Math.PI / 2.0) - zen_ang;
    var sin_alt_ang:Number = Math.sin(alt_ang);
    var zeta:Number = 1.041 * Math.pow(zen_ang, 3.0);
    // sky clearness
    var sky_cl:Number = (
      (dif_sol_rad + dir_sol_rad) / (dif_sol_rad + 0.0001) + zeta)
      / (1.0 + zeta);
    var air_mass:Number = (1.0 - (0.1 * elev_m) / 1000.0) / (sin_alt_ang +
      (0.15 / Math.pow(alt_ang / deg_to_rad + 3.885, 1.253)));
    var x_lum_eff:Number = 93.73; // extraterrestrial luminous efficacy
    var m:int = month - 1; // change from 1 base to 0 base month
    // sb = sky brightness
    var sb:Number = ((dif_sol_rad * x_lum_eff) * air_mass)
      / x_dir_norm_ill[m];
    // scb = sky clearness bin
    var scb:int;
    if ( sky_cl <= 1.065) {
      scb = 0;
    } else if (sky_cl > 1.065 && sky_cl <= 1.23) {
      scb = 1;
    } else if (sky_cl > 1.230 && sky_cl <= 1.50) {
      scb = 2;
    } else if (sky_cl > 1.500 && sky_cl <= 1.95) {
      scb = 3;
    } else if (sky_cl > 1.950 && sky_cl <= 2.80) {
      scb = 4;
    } else if (sky_cl > 2.800 && sky_cl <= 4.50) {
      scb = 5;
    } else if (sky_cl > 4.500 && sky_cl <= 6.20) {
      scb = 6;
    } else {
      scb = 7;
    }
    // am = atomspheric moisture
    var am:Number = Math.exp((0.07 * Tdp_C) - 0.075);
    // Diffuse Luminous Efficacy
    var dif_le:Number;
    if (sb <= 0.0) {
      dif_le = 0.0;
    } else {
      dif_le = (a_dif[scb] + b_dif[scb] * am
        + c_dif[scb] * cos_zen_ang + d_dif[scb] * Math.log(sb));
    }
    // Direct Luminous Efficacy
    var dir_le:Number;
    if (sb <= 0.0) {
      dir_le = 0.0;
    } else {
      dir_le = Math.max(0.0, a_dir[scb] + b_dir[scb] * am
        + c_dir[scb] * Math.exp((5.73 * zen_ang) - 5.0) + d_dir[scb] * sb);
    }
    if (isNaN(dir_le)) {
      dir_le = 0.0;
    }
    if (isNaN(dif_le)) {
      dif_le = 0.0;
    }
    return new LuminousEfficacy(dir_le, dif_le);
  }
}
}
