/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.MetaData;

import units.NoUnits;

public class DOE2Specific {
  public static const PLUGIN_NAME:String = 'doe2Specific';
  public static const SOLAR_FLAG:String = 'solarFlag';
  public static const DERIVATION_FUNCS:Object = {};
  public static const SERIES_META_DATA:Object = {};
  public static var HEADER_META_DATA:Object = {};
  HEADER_META_DATA[SOLAR_FLAG] = new MetaData(
    SOLAR_FLAG,
    "Solar Flag",
    NoUnits.NONE,
    NoUnits.QUANTITY_OF_MEASURE,
    function(h:Object, pt:Object):String { return h[SOLAR_FLAG]; },
    MetaData.DATA_TYPE__STRING,
    [],
    null,
    null,
    true,
    NoUnits.AVAILABLE_UNITS);

  public static const MINS:Object = {};
  public static const MAXS:Object = {};
}
}