/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.MetaData;

import units.AngleUnits;
import units.LengthUnits;
import units.NoUnits;

import utils.MapUtils;

public class Location {
    public static const PLUGIN_NAME:String = 'location';
    public static const CITY:String = 'city';
    public static const
    STATE_PROVINCE_OR_REGION:String = 'stateProvinceOrRegion';
    public static const COUNTRY:String = 'country';
    public static const DATA_SOURCE:String = 'source'; // ambiguous -- clarify
    // WMO = World Meteorological Organization
    public static const WMO_STATION_NUMBER:String = 'wmo';
    public static const LATITUDE_deg:String = 'latitude_deg';
    // Convention: negative for West, positive for East
    // note: this is the EPW convention and also seen on Wikipedia
    public static const LONGITUDE_deg:String = 'longitude_deg';
    public static const ELEVATION_m:String = 'elevation_m';

    public static function
    makeLocation(
      city:String,
      country:String,
      state:String,
      dataSource:String,
      wmoNum:String,
      lat_deg:Number,
      long_deg:Number,
      elev_m:Number):Object
    {
      return MapUtils.makeObject(
        [
          CITY, city,
          COUNTRY, country,
          STATE_PROVINCE_OR_REGION, state,
          DATA_SOURCE, dataSource,
          WMO_STATION_NUMBER, wmoNum,
          LATITUDE_deg, lat_deg,
          LONGITUDE_deg, long_deg,
          ELEVATION_m, elev_m
        ]);
    }

    public static const EXAMPLES:Object = new Object();
    EXAMPLES['Denver, Colorado USA'] = makeLocation(
        "Denver", "Colorado", "USA", "", "", 39.7392, -104.9842, 1564.0);
    EXAMPLES['Greenwich, London UK'] = makeLocation(
        "Greenwich", "London", "UK", "", "", 51.4788, 0.0, 24.0);

    public static const DERIVATION_FUNCS:Object = {};
    public static const SERIES_META_DATA:Object = {};
    public static const HEADER_META_DATA:Object = {};
    HEADER_META_DATA[ELEVATION_m] = new MetaData(
      ELEVATION_m,
      "Elevation",
      LengthUnits.METER,
      LengthUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):Number
      {
        return h[ELEVATION_m];
      },
      MetaData.DATA_TYPE__NUMBER,
      [],
      function(h:Object, pt:Object):Number {
        if (h.hasOwnProperty(ELEVATION_m)) {
          var e:Number = h[ELEVATION_m];
          if ((e >= -1000.0) && (e < 9000.0)) {
            return 0.0;
          }
        }
        return 1.0;
      },
      null,
      true,
      LengthUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[CITY] = new MetaData(
      CITY,
      "City (Site Location)",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String
      {
        return h[CITY];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[STATE_PROVINCE_OR_REGION] = new MetaData(
      STATE_PROVINCE_OR_REGION,
      "State/Province/Region",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String
      {
        return h[STATE_PROVINCE_OR_REGION];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[COUNTRY] = new MetaData(
      COUNTRY,
      "Country",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String
      {
        return h[COUNTRY];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[DATA_SOURCE] = new MetaData(
      DATA_SOURCE,
      "Data Source",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String
      {
        return h[DATA_SOURCE];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[WMO_STATION_NUMBER] = new MetaData(
      WMO_STATION_NUMBER,
      "World Meteorological Organization (WMO) Station Number",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String
      {
        return h[WMO_STATION_NUMBER];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[LATITUDE_deg] = new MetaData(
      LATITUDE_deg,
      "Latitude",
      AngleUnits.DEGREES,
      AngleUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):Number
      {
        return h[LATITUDE_deg];
      },
      MetaData.DATA_TYPE__NUMBER,
      [],
      function(h:Object,pt:Object):Number {
        if (h.hasOwnProperty(LATITUDE_deg)) {
          var lat:Number = h[LATITUDE_deg];
          if ((lat >= -90.0) && (lat <= 90.0)) {
            return 0.0;
          }
        }
        return 1.0;
      },
      null,
      true,
      [AngleUnits.DEGREES]);
    HEADER_META_DATA[LONGITUDE_deg] = new MetaData(
      LONGITUDE_deg,
      "Longitude",
      AngleUnits.DEGREES,
      AngleUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):Number
      {
        return h[LONGITUDE_deg];
      },
      MetaData.DATA_TYPE__NUMBER,
      [],
      function(h:Object,pt:Object):Number {
        if (h.hasOwnProperty(LONGITUDE_deg)) {
          var long:Number = h[LONGITUDE_deg];
          if ((long >= -180.0) && (long <= 180.0)) {
            return 0.0;
          }
        }
        return 1.0;
      },
      null,
      true,
      [AngleUnits.DEGREES]);
    public static const MINS:Object = {};
    public static const MAXS:Object = {};
}
}