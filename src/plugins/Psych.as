/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.MetaData;
import dse.Normalization;

import scaling.ScalingFactors;

import units.DensityUnits;
import units.NoUnits;
import units.PressureUnits;
import units.SpecificEnergyUnits;
import units.SpecificVolumeUnits;
import units.TemperatureUnits;
import units.UnitConversions;

import utils.AdvMath;
import utils.MapUtils;
import utils.Set;

/* References
[1] ASHRAE 1997 Fundamentals, Chapter 6, "Psychrometrics"
[2] Chapter 7 of T. Muneer. (2004). "Solar Radiation and Daylight Models".
    Second Edition, Elsevier Ltd. Amsterdam, Boston.
[3] ASHRAE 2009 Fundamentals, Chapter 1, "Psychrometrics" */
public class Psych {
    public static const PLUGIN_NAME:String = "psychrometrics";
    public static const DRY_BULB_TEMP_C:String = 'dryBulbTemp_C';
    public static const WET_BULB_TEMP_C:String = 'wetBulbTemp_C';
    public static const PRESSURE_kPa:String = 'pressure_kPa';
    public static const DEW_POINT_TEMP_C:String = 'dewPointTemp_C';
    public static const HUMIDITY_RATIO:String = 'humidityRatio';
    public static const ENTHALPY_kJ__kg:String = 'enthalpy_kJ__kg';
    public static const SPECIFIC_VOLUME_m3__kg:String = 'specVol_m3__kg';
    public static const DENSITY_kg__m3:String = 'density_kg__m3';
    //public static const DEGREE_OF_SATURATION:String = 'degreeOfSaturation';
    public static const RELATIVE_HUMIDITY:String = 'relativeHumidity';

    public static const STATE_NAMES:Object = Set.make([
        DRY_BULB_TEMP_C, WET_BULB_TEMP_C, PRESSURE_kPa]);

    public static const SCALE_FACTORS:Object =
        Normalization.makeScaleFactors(MINS, MAXS);

    public static function
    CONSTRAINT_FUNC(hdr:Object, vals:Object):Number {
        var err:Number = 0.0;
        var Tdb_C:Number = vals[DRY_BULB_TEMP_C];
        var Twb_C:Number = vals[WET_BULB_TEMP_C];
        var p_kPa:Number = vals[PRESSURE_kPa];
        var RH:Number = relativeHumidity(Tdb_C, Twb_C, p_kPa);
        var dbScale:Number =
            100.0 / (MAX_DRY_BULB_TEMP_C - MIN_DRY_BULB_TEMP_C);
        var pScale:Number =
            100.0 / (MAX_PRESSURE_kPa - MIN_PRESSURE_kPa);
        var rhScale:Number =
            100.0 / (MAX_RELATIVE_HUMIDITY - MIN_RELATIVE_HUMIDITY);
        // error due to dry-bulb and pressure being out of range
        if (Tdb_C > MAX_DRY_BULB_TEMP_C) {
            err += dbScale * Math.abs(Tdb_C - MAX_DRY_BULB_TEMP_C);
        }
        else if (Tdb_C < MIN_DRY_BULB_TEMP_C) {
            err += dbScale * Math.abs(MIN_DRY_BULB_TEMP_C - Tdb_C);
        }
        if (p_kPa > MAX_PRESSURE_kPa) {
            err += pScale * Math.abs(p_kPa - MAX_PRESSURE_kPa);
        }
        else if (p_kPa < MIN_PRESSURE_kPa) {
            err += pScale * Math.abs(MIN_PRESSURE_kPa - p_kPa);
        }
        // error due to relative humidity out of range
        if (RH > MAX_RELATIVE_HUMIDITY) {
            err += rhScale * Math.abs(RH - MAX_RELATIVE_HUMIDITY);
        }
        else if (RH < MIN_RELATIVE_HUMIDITY) {
            err += rhScale * Math.abs(MIN_RELATIVE_HUMIDITY - RH);
        }
        return err;
    };

    public static function
    makeDataPoint(Tdb_C:Number, Twb_C:Number, p_kPa:Number):Object {
        return MapUtils.makeObject([
            DRY_BULB_TEMP_C, Tdb_C,
            WET_BULB_TEMP_C, Twb_C,
            PRESSURE_kPa, p_kPa]);
    }

    public static const MAX_DRY_BULB_TEMP_C:Number = 80.0;
    public static const MAX_WET_BULB_TEMP_C:Number = 80.0;
    public static const MAX_PRESSURE_kPa:Number = 120.0;
    public static const MAX_DEW_POINT_TEMP_C:Number = 80.0;
    public static const MAX_HUMIDITY_RATIO:Number = 30.0e-3;
    public static const MAX_ENTHALPY_kJ__kg:Number = 140.0;
    public static const MAX_SPECIFIC_VOLUME_m3__kg:Number = 0.98;
    public static const MAX_DENSITY_kg__m3:Number = 1.4;
    public static const MAX_DEGREE_OF_SATURATION:Number = 1.0;
    public static const MAX_RELATIVE_HUMIDITY:Number = 1.0;
    public static const MAXS:Object = new Object();
    MAXS[DRY_BULB_TEMP_C] = MAX_DRY_BULB_TEMP_C;
    MAXS[WET_BULB_TEMP_C] = MAX_WET_BULB_TEMP_C;
    MAXS[PRESSURE_kPa] = MAX_PRESSURE_kPa;
    MAXS[DEW_POINT_TEMP_C] = MAX_DEW_POINT_TEMP_C;
    MAXS[HUMIDITY_RATIO] = MAX_HUMIDITY_RATIO;
    MAXS[ENTHALPY_kJ__kg] = MAX_ENTHALPY_kJ__kg;
    MAXS[SPECIFIC_VOLUME_m3__kg] = MAX_SPECIFIC_VOLUME_m3__kg;
    MAXS[DENSITY_kg__m3] = MAX_DENSITY_kg__m3;
    //MAXS[DEGREE_OF_SATURATION] = MAX_DEGREE_OF_SATURATION;
    MAXS[RELATIVE_HUMIDITY] = MAX_RELATIVE_HUMIDITY;

    public static const MIN_DRY_BULB_TEMP_C:Number = -40.0;
    public static const MIN_WET_BULB_TEMP_C:Number = -40.0;
    public static const MIN_PRESSURE_kPa:Number = 80.0;
    public static const MIN_DEW_POINT_TEMP_C:Number = -40.0;
    public static const MIN_HUMIDITY_RATIO:Number = 0.05e-3;
    public static const MIN_ENTHALPY_kJ__kg:Number = -20.0;
    public static const MIN_SPECIFIC_VOLUME_m3__kg:Number = 0.76;
    public static const MIN_DENSITY_kg__m3:Number = 1.04;
    public static const MIN_DEGREE_OF_SATURATION:Number = 0.01;
    public static const MIN_RELATIVE_HUMIDITY:Number = 0.01;
    public static const MINS:Object = new Object();
    MINS[DRY_BULB_TEMP_C] = MIN_DRY_BULB_TEMP_C;
    MINS[WET_BULB_TEMP_C] = MIN_WET_BULB_TEMP_C;
    MINS[PRESSURE_kPa] = MIN_PRESSURE_kPa;
    MINS[DEW_POINT_TEMP_C] = MIN_DEW_POINT_TEMP_C;
    MINS[HUMIDITY_RATIO] = MIN_HUMIDITY_RATIO;
    MINS[ENTHALPY_kJ__kg] = MIN_ENTHALPY_kJ__kg;
    MINS[SPECIFIC_VOLUME_m3__kg] = MIN_SPECIFIC_VOLUME_m3__kg;
    MINS[DENSITY_kg__m3] = MIN_DENSITY_kg__m3;
    //MINS[DEGREE_OF_SATURATION] = MIN_DEGREE_OF_SATURATION;
    MINS[RELATIVE_HUMIDITY] = MIN_RELATIVE_HUMIDITY;

    public static const DEFAULT_DRY_BULB_TEMP_C:Number = 25.0;
    public static const DEFAULT_WET_BULB_TEMP_C:Number = 20.0;
    public static const DEFAULTS_PRESSURE_kPa:Number = 101.325;

    public static const DERIVATION_FUNCS:Object = new Object();
    //DERIVATION_FUNCS[DEGREE_OF_SATURATION] = deriveX(degreeOfSaturation);
    DERIVATION_FUNCS[DENSITY_kg__m3] = deriveX(density_kg__m3);
    DERIVATION_FUNCS[DEW_POINT_TEMP_C] = deriveX(dewPointTemp_C);
    DERIVATION_FUNCS[ENTHALPY_kJ__kg] = deriveX(enthalpy_kJ__kg);
    DERIVATION_FUNCS[HUMIDITY_RATIO] = deriveX(humidityRatio);
    DERIVATION_FUNCS[RELATIVE_HUMIDITY] = deriveX(relativeHumidity);
    DERIVATION_FUNCS[SPECIFIC_VOLUME_m3__kg] = deriveX(specVol_m3__kg);

    // NOTE: These do not do anything yet. Currently, setting sets are
    // specified through the MetaData for a given variable. The advantages
    // of specifying settingSets at each variable are the ability to control
    // the order of presentation/preference for setting sets. The disadvantage
    // is the repetition involved. I believe a better long term solution is
    // to have setting sets specified separately as they are below and let
    // setting set order be a dispaly option preference. However, for now,
    // they continue to be specified through the MetaData declarations.
    public static const SETTING_SETS:Array = [
        Set.make([DRY_BULB_TEMP_C, WET_BULB_TEMP_C, PRESSURE_kPa]),
        //Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, DEGREE_OF_SATURATION]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, DENSITY_kg__m3]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, DEW_POINT_TEMP_C]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, ENTHALPY_kJ__kg]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, HUMIDITY_RATIO]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, RELATIVE_HUMIDITY]),
        Set.make([DRY_BULB_TEMP_C, PRESSURE_kPa, SPECIFIC_VOLUME_m3__kg]),
        Set.make([WET_BULB_TEMP_C, PRESSURE_kPa, RELATIVE_HUMIDITY]),
        Set.make([WET_BULB_TEMP_C, PRESSURE_kPa, DEW_POINT_TEMP_C]),
        Set.make([WET_BULB_TEMP_C, PRESSURE_kPa, HUMIDITY_RATIO]),
        Set.make([WET_BULB_TEMP_C, PRESSURE_kPa, SPECIFIC_VOLUME_m3__kg]),
        Set.make([WET_BULB_TEMP_C, PRESSURE_kPa, DENSITY_kg__m3]),
        Set.make([DEW_POINT_TEMP_C, RELATIVE_HUMIDITY, PRESSURE_kPa]),
        Set.make([DEW_POINT_TEMP_C, RELATIVE_HUMIDITY, PRESSURE_kPa]),
        Set.make([RELATIVE_HUMIDITY, HUMIDITY_RATIO, PRESSURE_kPa]),
        Set.make([RELATIVE_HUMIDITY, ENTHALPY_kJ__kg, PRESSURE_kPa]),
        Set.make([RELATIVE_HUMIDITY, SPECIFIC_VOLUME_m3__kg, PRESSURE_kPa]),
        Set.make([RELATIVE_HUMIDITY, DENSITY_kg__m3, PRESSURE_kPa]),
        Set.make([HUMIDITY_RATIO, ENTHALPY_kJ__kg, PRESSURE_kPa]),
        Set.make([HUMIDITY_RATIO, SPECIFIC_VOLUME_m3__kg, PRESSURE_kPa]),
        Set.make([HUMIDITY_RATIO, DENSITY_kg__m3, PRESSURE_kPa])];

    public static const STATE_FUNCS:Object = new Object();
    //STATE_FUNCS[DEGREE_OF_SATURATION] = degreeOfSaturation;
    STATE_FUNCS[DENSITY_kg__m3] = density_kg__m3;
    STATE_FUNCS[DEW_POINT_TEMP_C] = dewPointTemp_C;
    STATE_FUNCS[ENTHALPY_kJ__kg] = enthalpy_kJ__kg;
    STATE_FUNCS[HUMIDITY_RATIO] = humidityRatio;
    STATE_FUNCS[RELATIVE_HUMIDITY] = relativeHumidity;
    STATE_FUNCS[SPECIFIC_VOLUME_m3__kg] = specVol_m3__kg;

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[WET_BULB_TEMP_C] = new MetaData(
        WET_BULB_TEMP_C,
        "Wet Bulb Temperature",
        TemperatureUnits.CELSIUS,
        TemperatureUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(WET_BULB_TEMP_C),
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa],
         [DEW_POINT_TEMP_C, PRESSURE_kPa],
         [RELATIVE_HUMIDITY, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        TemperatureUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[DRY_BULB_TEMP_C] = new MetaData(
        DRY_BULB_TEMP_C,
        "Dry Bulb Temperature",
        TemperatureUnits.CELSIUS,
        TemperatureUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(DRY_BULB_TEMP_C),
        MetaData.DATA_TYPE__NUMBER,
        [[RELATIVE_HUMIDITY, PRESSURE_kPa],
         [DEW_POINT_TEMP_C, PRESSURE_kPa],
         [WET_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        TemperatureUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[DEW_POINT_TEMP_C] = new MetaData(
        DEW_POINT_TEMP_C,
        "Dew Point Temperature",
        TemperatureUnits.CELSIUS,
        TemperatureUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[DEW_POINT_TEMP_C],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa],
         [WET_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        TemperatureUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[PRESSURE_kPa] = new MetaData(
        PRESSURE_kPa,
        "Atmospheric Pressure",
        PressureUnits.KPA,
        PressureUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(PRESSURE_kPa),
        MetaData.DATA_TYPE__NUMBER,
        [[WET_BULB_TEMP_C, DRY_BULB_TEMP_C]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        PressureUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[HUMIDITY_RATIO] = new MetaData(
        HUMIDITY_RATIO,
        "Humidity Ratio",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[HUMIDITY_RATIO],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        NoUnits.AVAILABLE_UNITS,
        ScalingFactors.x1e6);
    SERIES_META_DATA[DENSITY_kg__m3] = new MetaData(
        DENSITY_kg__m3,
        "Density",
        DensityUnits.KG__M3,
        DensityUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[DENSITY_kg__m3],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        DensityUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[SPECIFIC_VOLUME_m3__kg] = new MetaData(
        SPECIFIC_VOLUME_m3__kg,
        "Specific Volume",
        SpecificVolumeUnits.M3__KG,
        SpecificVolumeUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[SPECIFIC_VOLUME_m3__kg],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        SpecificVolumeUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[ENTHALPY_kJ__kg] = new MetaData(
        ENTHALPY_kJ__kg,
        "Enthalpy",
        SpecificEnergyUnits.KILOJOULES_PER_KILOGRAM,
        SpecificEnergyUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[ENTHALPY_kJ__kg],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        SpecificEnergyUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[RELATIVE_HUMIDITY] = new MetaData(
        RELATIVE_HUMIDITY,
        "Relative Humidity",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[RELATIVE_HUMIDITY],
        MetaData.DATA_TYPE__NUMBER,
        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        NoUnits.AVAILABLE_UNITS,
        ScalingFactors.PERCENTAGE);
//    SERIES_META_DATA[DEGREE_OF_SATURATION] = new MetaData(
//        DEGREE_OF_SATURATION,
//        "Degree of Saturation",
//        NoUnits.NONE,
//        NoUnits.QUANTITY_OF_MEASURE,
//        DERIVATION_FUNCS[DEGREE_OF_SATURATION],
//        MetaData.DATA_TYPE__NUMBER,
//        [[DRY_BULB_TEMP_C, PRESSURE_kPa]],
//        CONSTRAINT_FUNC,
//        STATE_NAMES,
//        true,
//        NoUnits.AVAILABLE_UNITS,
//        ScalingFactors.PERCENTAGE);

    public static function
    deriveX(f:Function):Function {
        return function(hdr:Object, vals:Object):Number {
            var Tdb_C:Number = vals[DRY_BULB_TEMP_C];
            var Twb_C:Number = vals[WET_BULB_TEMP_C];
            var p_kPa:Number = vals[PRESSURE_kPa];
            return f(Tdb_C, Twb_C, p_kPa);
        };
    }

    // myu or u, [3] eq. 13
    public static function
    degreeOfSaturation(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                       pressure_kPa:Number):Number {
        var W:Number =
            humidityRatio(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        var Ws:Number = satHumidityRatio(dryBulbTemp_C, pressure_kPa);
        return W / Ws;
    }

    // h, [3] eq. 32
    public static function
    enthalpy_kJ__kg(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                    pressure_kPa:Number):Number {
        var W:Number =
            humidityRatio(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        return 1.006 * dryBulbTemp_C + W * (2501.0 + 1.86 * dryBulbTemp_C);
    }

    public static function
    density_kg__m3(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                   pressure_kPa:Number):Number {
        return 1.0 /
            specVol_m3__kg(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
    }

    // nu or v, [3] eq. 28a
    public static function
    specVol_m3__kg(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                   pressure_kPa:Number):Number {
        var W:Number =
            humidityRatio(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        return 0.287042
            * (dryBulbTemp_C + 273.15) * (1.0 + 1.607858 * W) / pressure_kPa;
    }

    /* td or tdew, [3] eqs. 39 & 40
    // note: [2] p 319 eq 7.1.14 runs the first two coefficients together
    // and is incorrect */
    public static function
    dewPointTemp_C(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                   pressure_kPa:Number):Number {
        var C14:Number = 6.54;
        var C15:Number = 14.526;
        var C16:Number = 0.7389;
        var C17:Number = 0.09486;
        var C18:Number = 0.4569;
        var pw:Number =
            partPressureH2O_kPa(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        var alpha:Number = Math.log(pw);
        var td_0_to_93C:Number = C14 + C15 * alpha + C16 * alpha * alpha
            + C17 * alpha * alpha * alpha + C18 * Math.pow(pw, 0.1984);
        var td_below_0C:Number = 6.09 + 12.608 * alpha
            + 0.4959 * alpha * alpha;
        if (td_0_to_93C < 0.0) {
            return td_below_0C;
        }
        return td_0_to_93C;
    }

    // pw, [1] eq. 36, [3] eq. 38
    public static function
    partPressureH2O_kPa(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                        pressure_kPa:Number):Number {
        var W:Number
            = humidityRatio(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        return pressure_kPa * W / (0.621945 + W);
    }

    // W, [3], eq. 35
    public static function
    humidityRatio(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                  pressure_kPa:Number):Number {
        var Ws_star:Number = satHumidityRatio(wetBulbTemp_C, pressure_kPa);
        var numer:Number;
        var denom:Number;
        if (dryBulbTemp_C >= 0.0) {
            numer =
                (2501.0 - 2.326 * wetBulbTemp_C) * Ws_star
                - 1.006 * (dryBulbTemp_C - wetBulbTemp_C);
            denom = 2501.0 + 1.86 * dryBulbTemp_C - 4.186 * wetBulbTemp_C;
        } else {
            numer =
                (2830.0 - 0.24 * wetBulbTemp_C) * Ws_star
                - 1.006 * (dryBulbTemp_C - wetBulbTemp_C);
            denom = 2830.0 + 1.86 * dryBulbTemp_C - 2.1 * wetBulbTemp_C;
        }
        return numer / denom;
    }

    /* Ws, [3] eq. 23, using coefficient from [3]
    // saturation humidity ratio as a function of dry bulb temperature
    // and pressure */
    public static function
    satHumidityRatio(dryBulbTemp_C:Number, pressure_kPa:Number):Number {
        var pws_kPa:Number = satPressureH2OVapor_kPa(dryBulbTemp_C);
        return 0.621945 * pws_kPa / (pressure_kPa - pws_kPa);
    }

    /* pws, [3] eq. 5 & 6
    // water vapor saturation pressure (kPa)
    // Notes:
    // * coefficients differ between [1] and [2]. This is likely due to
    //   kPa vs Pa for output units.
    // * taking [3] as authoritative */
    public static function
    satPressureH2OVapor_kPa(temp_C:Number):Number {
        var T:Number = UnitConversions.C_to_K(temp_C);
        var ln_pws:Number;
        if (temp_C < 0.0) {
            var C1:Number = -5.6745359e3;
            var C2:Number =  6.3925247; // -5.1523058e-1;
            var C3:Number = -9.6778430e-3; // -9.6778430e3;
            var C4:Number =  6.2215701e-7;
            var C5:Number =  2.0747825e-9;
            var C6:Number = -9.4840240e-13;
            var C7:Number =  4.1635019;
            ln_pws = C1 / T + C2 + C3 * T + C4 * T * T + C5 * T * T * T
                + C6 * T * T * T * T + C7 * Math.log(T);
        } else {
            var  C8:Number = -5.8002206e3;
            var  C9:Number = 1.3914993; // -5.5162560 f/ [2]
            var C10:Number = -4.8640239e-2;
            var C11:Number =  4.1764768e-5;
            var C12:Number = -1.4452093e-8;
            var C13:Number =  6.5459673;
            ln_pws = C8 / T + C9 + C10 * T + C11 * T * T + C12 * T * T * T
                + C13 * Math.log(T);
        }
        var pws_Pa:Number = Math.exp(ln_pws);
        var kPa__Pa:Number = 1.0 / 1000.0;
        return pws_Pa * kPa__Pa;
    }

    // phi or RH, [3] eq. 24
    public static function
    relativeHumidity(dryBulbTemp_C:Number, wetBulbTemp_C:Number,
                     pressure_kPa:Number):Number {
        var pw:Number =
            partPressureH2O_kPa(dryBulbTemp_C, wetBulbTemp_C, pressure_kPa);
        var pws:Number = satPressureH2OVapor_kPa(dryBulbTemp_C);
        return pw / pws;
    }

    public static function
    Twb_from_p_Tdb_and_X(XName:String, Tdb_C:Number, p_kPa:Number,
                         X:Number):Number {
        var Twb_C:Number;
        var tolerance:Number = 1e-4;
        var fieldValueError:Function = function(Twb_C:Number):Number {
            var sf:Function = STATE_FUNCS[XName];
            var calculatedValue:Number = sf(Tdb_C, Twb_C, p_kPa);
            return X - calculatedValue;
        };
        var df:Function = AdvMath.makeDerivativeFunction(
            fieldValueError, AdvMath.DX);
        var Twb_C_max:Number = MAXS[Psych.WET_BULB_TEMP_C];
        var Twb_C_min:Number = MINS[Psych.WET_BULB_TEMP_C];
        var dxBounds:Number = (Twb_C_max - Twb_C_min) / 200.0;
        var maxCount:int = 400;
        try {
            Twb_C = AdvMath.safeNewtonRaphson(
                fieldValueError, df, Twb_C_min, Twb_C_max, dxBounds,
                AdvMath.DX, maxCount);
        } catch (e:Error) {
            if (false) {
                // OK, newton raphson crashed we'll try a brute force solution:
                var numPts:int = 400;
                var errs:Array = [];
                var range:Number = Twb_C_max - Twb_C_min;
                var dD:Number = range / (numPts + 1.0);
                for (var ptIdx:int = 0; ptIdx < numPts; ptIdx++) {
                    var val:Number =
                        fieldValueError(Twb_C_min + (ptIdx + 1.0) * dD);
                    if (isNaN(val)) {
                        errs.push(1e6);
                    } else {
                        errs.push(Math.abs(val));
                    }
                }
                var idxForMinErr:int = AdvMath.idxOfMin(errs);
                Twb_C = Twb_C_min + (idxForMinErr + 1.0) * dD;
            }
            Twb_C = NaN;
        }
        return Twb_C;
    }

    public static function
    geopotentialHeightToAltitude_m(h_geopotential:Number):Number {
      var Rearth:Number = 6356766.0;
      return h_geopotential * Rearth / (Rearth - h_geopotential);
    }

    public static function
    altitudeToGeopotentialHeight_m(h_altitude:Number):Number {
      var f:Function = function(x:Number):Number {
        return h_altitude - geopotentialHeightToAltitude_m(x);
      };
      var df:Function = AdvMath.makeDerivativeFunctionStencil(f, 1e-6);
      var TOL:Number = 1e-6;
      return AdvMath.safeNewtonRaphson(f, df, -1000.0, 10000.0, TOL, TOL, 100);
    }

    public static function standardAtmPressure_kPa(altitude_m:Number):Number {
      return 101.325 * Math.pow(1.0 - (2.25577e-5 * altitude_m), 5.2559);
    }

    public static function standardTemperature_C(altitude_m:Number):Number {
      return 15.0 - 0.0065 * altitude_m;
    }
}
}