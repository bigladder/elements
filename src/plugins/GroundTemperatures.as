/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
  import dse.MetaData;

  import units.NoUnits;

public class GroundTemperatures {
    public static const PLUGIN_NAME:String = 'groundTemperatures';
    public static const GROUND_TEMPERATURES_C:String = 'groundTemperatures_C';
    public static const DERIVATION_FUNCS:Object = {};
    public static const SERIES_META_DATA:Object = {};
    public static const HEADER_META_DATA:Object = {};
    HEADER_META_DATA[GROUND_TEMPERATURES_C] = new MetaData(
      GROUND_TEMPERATURES_C,
      "Ground Temperatures (C)",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object,pt:Object):String {
        return h[GROUND_TEMPERATURES_C];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      [],
      true,
      NoUnits.AVAILABLE_UNITS);

    public static const MINS:Object = {};
    public static const MAXS:Object = {};
}
}