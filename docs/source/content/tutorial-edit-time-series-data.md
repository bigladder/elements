% Edit Time-Series Data - Elements | User Guide

# Edit Time-Series Data

This tutorial will help you to understand how *Elements* preserves the relationships between psychrometric variables when editing.

1. Start *Elements*.
2. Click the `New Weather File` button.
3. Select IP Units.
4. Select the Dry Bulb Temperature for 2013/01/01 @ 01:00:00 by clicking on the Data Grid on the cell at that location so that it becomes highlighted.
5. Select "Relative Humidity, Atmospheric Pressure" in the `Variables to Hold Constant` drop-down list.
6. Double click on the Data Grid cell indicated in step 4 until the cell is enabled for editing.
7. Type "65" for the new value and hit the `Return` or `Enter` key
8. Note that Atmospheric Pressure and Relative Humidity have remained constant. However, Wet Bulb Temperature and Dew Point Temperature have changed to keep the psychrometric state physically valid.

Let's now look at another relationship involving solar variables.

1. Start *Elements*.
2. Click the `New Weather File` button.
3. Select SI Units.
4. Click `Add` to add the variable Cosine of Zenith to the Data Grid view.
5. Click on the Wind Speed column to highlight that column.
6. Click the `Remove` button to remove Wind Speed from the view.
7. Scroll the Data Grid down until the row with "2013/01/01 @ 12:00:00" is visible.
8. Change Diffuse Solar for this row to 100.0 Wh/m2.
9. Change Normal Solar for this row to 0.0 Wh/m2.
10. Note that Global Solar is now equal to 100.0 Wh/m2.
11. Change Normal Solar to 100.0 Wh/m2.
12. Note that Global Solar is now equal to approximately 191 Wh/m2

In this last set of steps, Normal Solar, Global Solar, Diffuse Solar, and the cosine of the zenith angle are all related. As we change both Diffuse and Normal Solar to 100.0 Wh/m2, we see that *Elements* automatically changes the global solar to always equal the sum of diffuse solar plus the component of beam normal incident on a horizontal surface.
