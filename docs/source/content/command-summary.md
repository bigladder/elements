% Command Summary - User Guide | Elements

# Command Summary

The following menu commands are available.

## `File/New`

Open a new [Document Window](document-window.html) populated with some default data.

## `File/Open...`

Open a [Document Window](document-window.html) populated with an existing weather file from your hard drive. The supported formats are:

- DOE-2 (.bin): Binary format for the *DOE-2* engine
- DOE-2 (.fmt): Text format for the *DOE-2* engine
- EnergyPlus (.epw): Text format for the *EnergyPlus* engine
- Elements (.elements): Native [Elements file format](elements-file-format.html).

## `File/Close`

Close the current window. If the window is a [Document Window](document-window.html) and the weather file has not yet been saved to your hard drive, *Elements* will prompt for you to save.

## `File/Save`

Save the current weather file. The file will be saved in the same format that it was opened from. If the file has never been saved, this command will be disabled and you must first use the `File/Save As...` command to set the format.

## `File/Save As...`

Save the current weather file as a new copy or in a new format. The submenu commands under `File/Save As...` allow you to select the file format. The supported formats are:

- DOE-2 (.bin): Binary format for the *DOE-2* engine
- DOE-2 (.fmt): Text format for the *DOE-2* engine
- EnergyPlus (.epw): Text format for the *EnergyPlus* engine
- Elements (.elements): Native [Elements file format](elements-file-format.html).

## `File/Quit` or `Elements/Quit Elements`

Exit the program.

## `Edit/Undo`

Undo the previous change.

## `Edit/Redo`

Redo the previous change that was undone.

## `Edit/Cut`

Cut the data selection. Note: Not currently available--use `Copy` instead.

## `Edit/Copy`

Copy the data selection to the clipboard.

## `Edit/Paste`

Paste data from the clipboard into the data selection.

## `Edit/Select All`

Select all data in the Data Grid. Note: Not currently available--hold down Shift and click in the top left and bottom right corners of the Data Grid to select all data.

## `Tools/Offset`

Apply the `Offset` tool to edit the data selection in bulk. This command is only enabled if the entire data selection is within the same column.

## `Tools/Scale`

Apply the `Scale` tool to edit the data selection in bulk. This command is only enabled if the entire data selection is within the same column.

## `Tools/Normalize`

Apply the `Normalize` tool to edit the data selection in bulk. This command is only enabled if the entire data selection is within the same column.

## `Tools/Normalize By Month`

Show the [Normalize By Month Dialog](normalize-by-month-dialog.html) and subsequently apply the `Normalize By Month` tool to edit the data selection in bulk. This command is only enabled if an entire column is selected.

## `View/Header`

Show the [Header Dialog](header-dialog.html).

## `View/Chart`

Show the [Chart Window](chart-window.html).

## `View/Units/SI`

Switch the displayed unit system to the international system of units (SI).

## `View/Units/IP`

Switch the displayed unit system to the inch-pound system of units (IP).

## `View/Columns/Add`

Show the [Add Column Dialog](document-window.html#columns) to add a new data column to the current view.

## `View/Columns/Remove`

Remove the selected data column from the current view. This command is only enabled if an entire column is selected.

## `View/Columns/Move Left`

Move the selected data column left by one position. This command is only enabled if an entire column is selected.

## `View/Columns/Move Right`

Move the selected data column right by one position. This command is only enabled if an entire column is selected.

## `Window`

Switch to another open window. This menu lists all open *Elements* windows along with the current active window.

## `Help/User Guide`

Open the *User Guide* documentation for *Elements* (what you are reading now) in your web browser.

## `Help/Online Resources`

Open the online resources for *Elements* in your web browser. You must be connected to the internet.

## `Help/About Elements`

Show the About Dialog which displays the version number and other information about *Elements*.
