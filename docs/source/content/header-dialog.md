% Header Dialog - User Guide | Elements

# Header Dialog

The Header Dialog displays all of the header data for viewing and editing. The Header Dialog is opened by clicking the `Header` button in the [Document Window](document-window.html).

The header data are a simple mapping between properties and values. The values can be edited by double clicking on a cell and typing in a new value.

![Header Dialog](images/header-dialog.png)
