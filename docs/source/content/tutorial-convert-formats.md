% Convert Formats - User Guide | Elements

# Convert Formats

Converting weather file formats is easy.

1. Click on `File/Open` to open an existing weather file.
2. Click on `File/Save As...` and choose the submenu command corresponding to the output format you would like.
