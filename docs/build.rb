# Copyright (c) 2014 Big Ladder Software LLC. All rights reserved.
# See the license file for additional terms and conditions.

require 'find'
require 'fileutils'
require 'rexml/document'

PATH = File.expand_path(File.join("../src"), File.dirname(__FILE__))

def get_elements_version
  path = File.join(PATH, "Elements-app.xml")
  raw = File.open(path)
  doc = REXML::Document.new(raw)
  doc.elements["application/versionNumber"].to_a[0]
end

puts "Starting build..."

source_dir = "source/"
output_dir = "../src/htmldocs/user-guide/"
date_stamp = Time.now.strftime("%Y.%m.%d")  # e.g., "2014.03.10"
version = get_elements_version

# Clean up output directory
FileUtils.rm_rf(Dir["#{output_dir}/*"])




#  pandoc -Ss --include-in-header=base.css --from=markdown --to=html5 -o ./user-guide-www/intro.html intro.md

#shell_command = "pandoc -o #{output_path} -f markdown -t html --smart -c style.css source/header.tx_ #{source_path} source/footer.tx_"


shell_command = "pandoc --from=markdown --to=html5 --standalone --smart --template=#{source_dir}/template.html --variable=date:#{date_stamp} --variable=version:#{version} -o"

content_paths = Dir["#{source_dir}/content/*.md"]
for content_path in content_paths
  content_name = File.basename(content_path, ".*")
  output_path = "#{output_dir}/#{content_name}.html"

  pipe = IO::popen("#{shell_command} #{output_path} #{content_path}", 'r') do |io|
    while (line = io.gets)
      puts(line)
    end
  end
end

# Copy static files
FileUtils.cp_r("#{source_dir}/css", output_dir)
FileUtils.cp_r("#{source_dir}/content/images", output_dir)
FileUtils.cp_r("#{source_dir}/content/media", output_dir)

puts "Completed."
