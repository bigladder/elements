FasdUAS 1.101.10   ��   ��    k             l     ��  ��    P J Run the make_dmg.sh script following instructions "FOR LAYOUT ADJUSTMENT"     � 	 	 �   R u n   t h e   m a k e _ d m g . s h   s c r i p t   f o l l o w i n g   i n s t r u c t i o n s   " F O R   L A Y O U T   A D J U S T M E N T "   
  
 l     ��  ��    " open/mount the temp.dmg file     �   8 o p e n / m o u n t   t h e   t e m p . d m g   f i l e   ��  l    � ����  O     �    O    �    k    �       l   ��  ��    G A the close/open is to defeat a bug in SnowLeopard & Mountain Lion     �   �   t h e   c l o s e / o p e n   i s   t o   d e f e a t   a   b u g   i n   S n o w L e o p a r d   &   M o u n t a i n   L i o n      I   ������
�� .coreclosnull���    obj ��  ��        I   ������
�� .aevtodocnull  �    alis��  ��         r     ! " ! m    ��
�� ecvwicnv " n       # $ # 1    ��
�� 
pvew $ 1    ��
�� 
cwnd    % & % r    & ' ( ' m     ��
�� boovfals ( n       ) * ) 1   # %��
�� 
tbvi * 1     #��
�� 
cwnd &  + , + r   ' . - . - m   ' (��
�� boovfals . n       / 0 / 1   + -��
�� 
stvi 0 1   ( +��
�� 
cwnd ,  1 2 1 r   / ; 3 4 3 J   / 5 5 5  6 7 6 m   / 0����� 7  8 9 8 m   0 1���� d 9  : ; : m   1 2����L ;  <�� < m   2 3����&��   4 l      =���� = n       > ? > 1   8 :��
�� 
pbnd ? 1   5 8��
�� 
cwnd��  ��   2  @ A @ r   < G B C B l  < C D���� D n   < C E F E m   ? C��
�� 
icop F 1   < ?��
�� 
cwnd��  ��   C o      ����  0 theviewoptions theViewOptions A  G H G r   H S I J I m   H K��
�� earrnarr J n       K L K 1   N R��
�� 
iarr L o   K N����  0 theviewoptions theViewOptions H  M N M r   T _ O P O m   T W���� H P n       Q R Q 1   Z ^��
�� 
lvis R o   W Z����  0 theviewoptions theViewOptions N  S T S r   ` p U V U 4   ` h�� W
�� 
file W m   d g X X � Y Y < . b a c k g r o u n d : d m g - b a c k g r o u n d . p n g V n       Z [ Z 1   k o��
�� 
ibkg [ o   h k����  0 theviewoptions theViewOptions T  \ ] \ l  q q�� ^ _��   ^ $  only run the below line once:    _ � ` ` <   o n l y   r u n   t h e   b e l o w   l i n e   o n c e : ]  a b a l  q q�� c d��   c q kmake new alias file at container window to POSIX file "/Applications" with properties {name:"Applications"}    d � e e � m a k e   n e w   a l i a s   f i l e   a t   c o n t a i n e r   w i n d o w   t o   P O S I X   f i l e   " / A p p l i c a t i o n s "   w i t h   p r o p e r t i e s   { n a m e : " A p p l i c a t i o n s " } b  f g f l  q q�� h i��   h = 7get position of item "Elements.app" of container window    i � j j n g e t   p o s i t i o n   o f   i t e m   " E l e m e n t s . a p p "   o f   c o n t a i n e r   w i n d o w g  k l k r   q � m n m J   q y o o  p q p m   q t���� � q  r�� r m   t w���� ���   n n       s t s 1   � ���
�� 
posn t n   y � u v u 4   | ��� w
�� 
cobj w m    � x x � y y  E l e m e n t s . a p p v 1   y |��
�� 
cwnd l  z { z l  � ��� | }��   | = 7get position of item "Applications" of container window    } � ~ ~ n g e t   p o s i t i o n   o f   i t e m   " A p p l i c a t i o n s "   o f   c o n t a i n e r   w i n d o w {   �  r   � � � � � J   � � � �  � � � m   � ������ �  ��� � m   � ����� ���   � n       � � � 1   � ���
�� 
posn � n   � � � � � 4   � ��� �
�� 
cobj � m   � � � � � � �  A p p l i c a t i o n s � 1   � ���
�� 
cwnd �  � � � l  � ��� � ���   � < 6get position of item "license.txt" of container window    � � � � l g e t   p o s i t i o n   o f   i t e m   " l i c e n s e . t x t "   o f   c o n t a i n e r   w i n d o w �  � � � r   � � � � � J   � � � �  � � � m   � ������ �  ��� � m   � �����E��   � n       � � � 1   � ���
�� 
posn � n   � � � � � 4   � ��� �
�� 
cobj � m   � � � � � � �  l i c e n s e . t x t � 1   � ���
�� 
cwnd �  � � � l  � ��� � ���   � ; 5get position of item "README.txt" of container window    � � � � j g e t   p o s i t i o n   o f   i t e m   " R E A D M E . t x t "   o f   c o n t a i n e r   w i n d o w �  � � � r   � � � � � J   � � � �  � � � m   � ������ �  ��� � m   � ����� ���   � n       � � � 1   � ���
�� 
posn � n   � � � � � 4   � ��� �
�� 
cobj � m   � � � � � � �  R E A D M E . t x t � 1   � ���
�� 
cwnd �  ��� � l  � ��� � ���   � - 'update without registering applications    � � � � N u p d a t e   w i t h o u t   r e g i s t e r i n g   a p p l i c a t i o n s��    4    �� �
�� 
cdis � m     � � � � �  E l e m e n t s - 1 . 0 . 0  m      � ��                                                                                  MACS  alis    f  Denmark                    �0��H+     2
Finder.app                                                      �c�y2        ����  	                CoreServices    �1�      �͒       2   ,   +  1Denmark:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p    D e n m a r k  &System/Library/CoreServices/Finder.app  / ��  ��  ��  ��       �� � ���   � ��
�� .aevtoappnull  �   � **** � �� ����� � ���
�� .aevtoappnull  �   � **** � k     � � �  ����  ��  ��   �   � $ ��� ����������������������������������������� X�������� x���� ����� � �
�� 
cdis
�� .coreclosnull���    obj 
�� .aevtodocnull  �    alis
�� ecvwicnv
�� 
cwnd
�� 
pvew
�� 
tbvi
�� 
stvi����� d��L��&�� 
�� 
pbnd
�� 
icop��  0 theviewoptions theViewOptions
�� earrnarr
�� 
iarr�� H
�� 
lvis
�� 
file
�� 
ibkg�� ��� �
�� 
cobj
�� 
posn��������E�� �� �*��/ �*j O*j O�*�,�,FOf*�,�,FOf*�,�,FO�����v*�,�,FO*�,a ,E` Oa _ a ,FOa _ a ,FO*a a /_ a ,FOa a lv*�,a a /a ,FOa a lv*�,a a /a ,FOa  a !lv*�,a a "/a ,FOa  a lv*�,a a #/a ,FOPUUascr  ��ޭ