# Elements 1.0.6 -- Released November 29, 2016

* Fixed bug where EPW files were not saving and restoring changes to Cloud
  Cover. Cloud Cover is now aliased with Total Sky Cover in EPW files.
* Elements now remembers the last directory you opened or saved from and
  defaults to that directory when you next open or save.
* Add sum of current solar insolation to the normalization box for solar
* Add new Global Solar scaling method: Beam Fraction of Total. This method
  holds the relative fractions of beam and diffuse solar constant while
  scaling.
* Fix issue with incredibly poor performance on applying selection tools
  (Scale, Offset, Normalize) to solar (especially Global Solar)
* Fix issue where after a tool was applied to a selection (offset, scale,
  normalize, normalize by month), the selection would remain but the tool-bar
  buttons would grey out. Now, after applying a tool, both the buttons and the
  selection stay active.
* Fix issue where Elements would crash if data parsable as a number was
  entered into a header field
* Fix bug where Elements would freeze if negative solar data was entered when
  cos(Z) is < 0.0. Elements no longer depends on the beam normal component of
  solar under those circumstances.
* Fix issue with the internal time series metadata for solar elevation angle
  getting confused with solar zenith angle. This would cause Elements to
  crash if a user attempted to click on values from this column
* Fix issue with Elements hanging upon edit of Wind Direction column
* Fix issue with diffuse horizontal irradiance calculator at sunrise/sunset
  where the calculated irradiance could be much larger than it is supposed
  to be when a strong beam normal component is present and the average cozine
  of the zenith angle over the hour is negative. This functionality is only
  used on import of DOE2 style weather files.
